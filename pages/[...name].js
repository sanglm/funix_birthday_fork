import React, { useState, useEffect, useRef } from "react";
import Head from "next/head";
import styles from "../styles/Name.module.css";
import { useRouter } from "next/router";
import ConfettiGenerator from "confetti-js";
import messages from "../utils/birthdayWishes.js";
import useTheme from "../hooks/useTheme";
import * as htmlToImage from "html-to-image";
import FileSaver from "file-saver";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { Carousel } from "react-responsive-carousel";

const Example = () => (
  <div>
    {/* <h2>Chúc anh ngày sinh nhật thật vui vẻ, luôn mạnh khoẻ, gặt nhiều thành công hơn nữa anh nhé!</h2> */}
    <div style={{ width: "600px", margin: "auto", height: "600px"}}>
      <Carousel autoPlay={true} infiniteLoop={false} interval={5000}>
        <div>
          <img src="/h2.jpg" alt="image1" />
          {/* <p style={{background:"#7FB77E", fontSize:"26px", opacity:"1"}} className="legend"></p> */}
        </div>
        <div>
          <img src="/h3.jpg" alt="image2" />
          {/* <p style={{background:"#7FB77E", fontSize:"26px", opacity:"1"}} className="legend">Cảm ơn anh thật nhiều vì...</p> */}
        </div>

        <div>
          <img src="/h4.jpg" alt="image3" />
          {/* <p style={{background:"#7FB77E", fontSize:"26px", opacity:"1"}} className="legend">Cảm ơn anh thật nhiều vì...</p> */}
        </div>
        <div>
          <img src="/h5.jpg" alt="image4" />
          <p
            style={{ background: "#7FB77E", fontSize: "26px", opacity: "1" }}
            className="legend"
          >
            Luôn là người anh cả của chúng em
          </p>
        </div>
        <div>
          <img src="/h6.jpg" alt="image5" />
          <p
            style={{ background: "#7FB77E", fontSize: "26px", opacity: "1" }}
            className="legend"
          >
            3 phần yêu thương 7 phần nuông chiều chúng em
          </p>
        </div>
        <div>
          <img src="/h7.jpg" alt="image6" />
          <p
            style={{ background: "#7FB77E", fontSize: "26px", opacity: "1" }}
            className="legend"
          >
            Thi thoảng dí deadline chúng em bất kể ngày đêm
          </p>
        </div>
        <div>
          <img src="/h8.jpg" alt="image7" />
          <p
            style={{ background: "#7FB77E", fontSize: "26px", opacity: "1" }}
            className="legend"
          >
            Và cuối cùng là Đã yêu thương, tạo điều kiện cho chúng em trưởng
            thành hơn{" "}
          </p>
        </div>
        <div>
          <img src="/h9.jpg" alt="image8" />
          {/* <p style={{background:"#7FB77E", fontSize:"26px", opacity:"1"}} className="legend">Và cuối cùng là  Đã yêu thương, tạo điều kiện cho chúng em trưởng thành hơn </p> */}
        </div>
        <div>
          <img src="/h10.jpg" alt="image9" />
          {/* <p style={{background:"#7FB77E", fontSize:"26px", opacity:"1"}} className="legend">Và cuối cùng là  Đã yêu thương, tạo điều kiện cho chúng em trưởng thành hơn </p> */}
        </div>
      </Carousel>
    </div>
  </div>
);



const Wish = ({ history }) => {
  const router = useRouter();
  const { name } = router.query; // gets both name & color id in form of array [name,colorId]
  const color = name ? name[1] : 0; //extracting colorId from name
  const [downloading, setDownloading] = useState(false);
  const [downloadedOnce, setDownloadedOnce] = useState(false);
  const audioRef = useRef();

  const { setTheme } = useTheme();

  useEffect(() => {
    // Theme Change
    setTheme(color);

    if (downloading === false) {
      // Confetti
      const confettiSettings = {
        target: "canvas",
        start_from_edge: true,
      };
      const confetti = new ConfettiGenerator(confettiSettings);
      confetti.render();
    }

    audioRef.current.play();
  }, [color, downloading]);

  useEffect(() => {
    if (downloading === true && downloadedOnce === false) {
      downloadImage();
    }
  }, [downloading, downloadedOnce]);

  // function for randomly picking the message from messages array
  const randomNumber = (min, max) => {
    return Math.floor(Math.random() * (max - min)) + min;
  };

  const downloadImage = () => {
    if (downloadedOnce === true) return;

    const node = document.getElementById("image");

    if (node) {
      setDownloadedOnce(true);

      htmlToImage.toPng(node).then((blob) => {
        FileSaver.saveAs(blob, "birthday-wish.png");
        setDownloading(false);
      });
    }
  };

  const title = (name) => {
    const wish = "Happy Birthday " + name + "!";
    const base_letters = [];
    const name_letters = [];

    for (let i = 0; i < wish.length; i++) {
      if (i < 15) {
        const letter = wish.charAt(i);
        base_letters.push(
          <span key={i} style={{ "--i": i + 1 }}>
            {letter}
          </span>
        );
      } else {
        const letter = wish.charAt(i);
        name_letters.push(
          <span key={i} style={{ "--i": i + 1 }} className={styles.span}>
            {letter}
          </span>
        );
      }
    }

    return (
      <>
        {downloading ? (
          <h1
            className={styles.titleImg}
            style={{ "--wish-length": wish.length }}
          >
            <div>{base_letters.map((letter) => letter)}</div>
            <div>{name_letters.map((letter) => letter)}</div>
          </h1>
        ) : (
          <h1 className={styles.title} style={{ "--wish-length": wish.length }}>
            <div>{base_letters.map((letter) => letter)}</div>
            <div>{name_letters.map((letter) => letter)}</div>
          </h1>
        )}
      </>
    );
  };

  if (downloading) {
    return (
      <div className={styles.containerImg} id="image" onClick={downloadImage}>
        {downloadImage()}
        <main className={styles.image}>
          <div>
            <div className={styles.main}>{title(name && name[0])}</div>

            <div style={{ height: 40 }} />

            <p className={styles.descImg}>
              {messages[randomNumber(0, messages.length)].value}
            </p>
          </div>
        </main>
      </div>
    );
  }

  return (
    <div className={styles.container}>
      <Head>
        <title>Happy Birthday {name && name[0]}</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <canvas className={styles.canvas } id="canvas"></canvas>

      <main className={styles.animate}>
        <div>
          <div className={styles.main +' hidden_it'}>{title(name && name[0])}</div>
          {/* <p className={styles.desc}>
            {messages[randomNumber(0, messages.length)].value}
          </p> */}
        </div>

        <Example />

        {/* <div className={styles.buttonContainer}>
          {history[0] == "/" ? <CopyLinkButton /> : ""}

          {history[0] == "/" ? (
            <Button
              onClick={() => {
                setDownloadedOnce(false);
                setDownloading(true);
              }}
              text="Download as Image"
            />
          ) : (
            ""
          )}

          <Button
            onClick={() => router.push("/")}
            text="&larr; Create a wish"
          />
        </div> */}
      </main>
      <audio ref={audioRef} id="player" autoPlay>
        <source src="media/leemon_tree.mp3" />
      </audio>
    </div>
  );
};

export default Wish;
