(() => {
var exports = {};
exports.id = "pages/[...name]";
exports.ids = ["pages/[...name]"];
exports.modules = {

/***/ "./hooks/useTheme.js":
/*!***************************!*\
  !*** ./hooks/useTheme.js ***!
  \***************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
 //colors list

const useTheme = () => {
  const themes = [{
    id: 0,
    name: "blue",
    color: "#0070f3"
  }, {
    id: 1,
    name: "green",
    color: "#10B981"
  }, {
    id: 2,
    name: "violet",
    color: "#8B5CF6"
  }, {
    id: 3,
    name: "yellow",
    color: "#FBBF24"
  }, {
    id: 4,
    name: "red",
    color: "#E11D48"
  }];
  const {
    0: currentTheme,
    1: setCurrentTheme
  } = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)(themes[0]);

  const setTheme = id => {
    const requiredTheme = themes.find(item => id == item.id); // If the theme with the given id exists then change theme .

    if (requiredTheme) setCurrentTheme(requiredTheme); // If the theme with the given id does not exist then it doesnt change the default theme;
  };

  (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(() => {
    let root = document.documentElement;
    root.style.setProperty("--color", currentTheme.color);
  }, [currentTheme]);
  return {
    themes,
    setTheme,
    currentTheme
  };
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (useTheme);

/***/ }),

/***/ "./pages/[...name].js":
/*!****************************!*\
  !*** ./pages/[...name].js ***!
  \****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/head */ "next/head");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../styles/Name.module.css */ "./styles/Name.module.css");
/* harmony import */ var _styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var confetti_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! confetti-js */ "confetti-js");
/* harmony import */ var confetti_js__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(confetti_js__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _utils_birthdayWishes_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../utils/birthdayWishes.js */ "./utils/birthdayWishes.js");
/* harmony import */ var _hooks_useTheme__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../hooks/useTheme */ "./hooks/useTheme.js");
/* harmony import */ var html_to_image__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! html-to-image */ "html-to-image");
/* harmony import */ var html_to_image__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(html_to_image__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! file-saver */ "file-saver");
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(file_saver__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var react_responsive_carousel_lib_styles_carousel_min_css__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react-responsive-carousel/lib/styles/carousel.min.css */ "./node_modules/react-responsive-carousel/lib/styles/carousel.min.css");
/* harmony import */ var react_responsive_carousel_lib_styles_carousel_min_css__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react_responsive_carousel_lib_styles_carousel_min_css__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var react_responsive_carousel__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-responsive-carousel */ "react-responsive-carousel");
/* harmony import */ var react_responsive_carousel__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(react_responsive_carousel__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react/jsx-dev-runtime */ "react/jsx-dev-runtime");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__);
var _jsxFileName = "D:\\SangLM3\\APP\\fx_birthday_xuanqn\\pages\\[...name].js";














const Example = () => /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
  children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
    style: {
      width: "600px",
      margin: "auto",
      height: "600px"
    },
    children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)(react_responsive_carousel__WEBPACK_IMPORTED_MODULE_9__.Carousel, {
      autoPlay: true,
      infiniteLoop: false,
      interval: 5000,
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("img", {
          src: "/h2.jpg",
          alt: "image1"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 19,
          columnNumber: 11
        }, undefined)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 18,
        columnNumber: 9
      }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("img", {
          src: "/h3.jpg",
          alt: "image2"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 23,
          columnNumber: 11
        }, undefined)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 22,
        columnNumber: 9
      }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("img", {
          src: "/h4.jpg",
          alt: "image3"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 28,
          columnNumber: 11
        }, undefined)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 27,
        columnNumber: 9
      }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("img", {
          src: "/h5.jpg",
          alt: "image4"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 32,
          columnNumber: 11
        }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("p", {
          style: {
            background: "#7FB77E",
            fontSize: "26px",
            opacity: "1"
          },
          className: "legend",
          children: "Lu\xF4n la\u0300 ng\u01B0\u01A1\u0300i anh ca\u0309 cu\u0309a chu\u0301ng em"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 33,
          columnNumber: 11
        }, undefined)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 31,
        columnNumber: 9
      }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("img", {
          src: "/h6.jpg",
          alt: "image5"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 41,
          columnNumber: 11
        }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("p", {
          style: {
            background: "#7FB77E",
            fontSize: "26px",
            opacity: "1"
          },
          className: "legend",
          children: "3 ph\xE2\u0300n y\xEAu th\u01B0\u01A1ng 7 ph\xE2\u0300n nu\xF4ng chi\xEA\u0300u chu\u0301ng em"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 42,
          columnNumber: 11
        }, undefined)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 40,
        columnNumber: 9
      }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("img", {
          src: "/h7.jpg",
          alt: "image6"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 50,
          columnNumber: 11
        }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("p", {
          style: {
            background: "#7FB77E",
            fontSize: "26px",
            opacity: "1"
          },
          className: "legend",
          children: "Thi thoa\u0309ng di\u0301 deadline chu\u0301ng em b\xE2\u0301t k\xEA\u0309 nga\u0300y \u0111\xEAm"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 51,
          columnNumber: 11
        }, undefined)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 49,
        columnNumber: 9
      }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("img", {
          src: "/h8.jpg",
          alt: "image7"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 59,
          columnNumber: 11
        }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("p", {
          style: {
            background: "#7FB77E",
            fontSize: "26px",
            opacity: "1"
          },
          className: "legend",
          children: ["Va\u0300 cu\xF4\u0301i cu\u0300ng la\u0300 \u0110a\u0303 y\xEAu th\u01B0\u01A1ng, ta\u0323o \u0111i\xEA\u0300u ki\xEA\u0323n cho chu\u0301ng em tr\u01B0\u01A1\u0309ng tha\u0300nh h\u01A1n", " "]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 60,
          columnNumber: 11
        }, undefined)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 58,
        columnNumber: 9
      }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("img", {
          src: "/h9.jpg",
          alt: "image8"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 69,
          columnNumber: 11
        }, undefined)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 68,
        columnNumber: 9
      }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("img", {
          src: "/h10.jpg",
          alt: "image9"
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 73,
          columnNumber: 11
        }, undefined)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 72,
        columnNumber: 9
      }, undefined)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 17,
      columnNumber: 7
    }, undefined)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 16,
    columnNumber: 5
  }, undefined)
}, void 0, false, {
  fileName: _jsxFileName,
  lineNumber: 14,
  columnNumber: 3
}, undefined);

const Wish = ({
  history
}) => {
  const router = (0,next_router__WEBPACK_IMPORTED_MODULE_2__.useRouter)();
  const {
    name
  } = router.query; // gets both name & color id in form of array [name,colorId]

  const color = name ? name[1] : 0; //extracting colorId from name

  const {
    0: downloading,
    1: setDownloading
  } = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)(false);
  const {
    0: downloadedOnce,
    1: setDownloadedOnce
  } = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)(false);
  const audioRef = (0,react__WEBPACK_IMPORTED_MODULE_0__.useRef)();
  const {
    setTheme
  } = (0,_hooks_useTheme__WEBPACK_IMPORTED_MODULE_5__.default)();
  (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(() => {
    // Theme Change
    setTheme(color);

    if (downloading === false) {
      // Confetti
      const confettiSettings = {
        target: "canvas",
        start_from_edge: true
      };
      const confetti = new (confetti_js__WEBPACK_IMPORTED_MODULE_3___default())(confettiSettings);
      confetti.render();
    }

    audioRef.current.play();
  }, [color, downloading]);
  (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(() => {
    if (downloading === true && downloadedOnce === false) {
      downloadImage();
    }
  }, [downloading, downloadedOnce]); // function for randomly picking the message from messages array

  const randomNumber = (min, max) => {
    return Math.floor(Math.random() * (max - min)) + min;
  };

  const downloadImage = () => {
    if (downloadedOnce === true) return;
    const node = document.getElementById("image");

    if (node) {
      setDownloadedOnce(true);
      html_to_image__WEBPACK_IMPORTED_MODULE_6__.toPng(node).then(blob => {
        file_saver__WEBPACK_IMPORTED_MODULE_7___default().saveAs(blob, "birthday-wish.png");
        setDownloading(false);
      });
    }
  };

  const title = name => {
    const wish = "Happy Birthday " + name + "!";
    const base_letters = [];
    const name_letters = [];

    for (let i = 0; i < wish.length; i++) {
      if (i < 15) {
        const letter = wish.charAt(i);
        base_letters.push( /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("span", {
          style: {
            "--i": i + 1
          },
          children: letter
        }, i, false, {
          fileName: _jsxFileName,
          lineNumber: 145,
          columnNumber: 11
        }, undefined));
      } else {
        const letter = wish.charAt(i);
        name_letters.push( /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("span", {
          style: {
            "--i": i + 1
          },
          className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().span),
          children: letter
        }, i, false, {
          fileName: _jsxFileName,
          lineNumber: 152,
          columnNumber: 11
        }, undefined));
      }
    }

    return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.Fragment, {
      children: downloading ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("h1", {
        className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().titleImg),
        style: {
          "--wish-length": wish.length
        },
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: base_letters.map(letter => letter)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 166,
          columnNumber: 13
        }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: name_letters.map(letter => letter)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 167,
          columnNumber: 13
        }, undefined)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 162,
        columnNumber: 11
      }, undefined) : /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("h1", {
        className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().title),
        style: {
          "--wish-length": wish.length
        },
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: base_letters.map(letter => letter)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 171,
          columnNumber: 13
        }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: name_letters.map(letter => letter)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 172,
          columnNumber: 13
        }, undefined)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 170,
        columnNumber: 11
      }, undefined)
    }, void 0, false);
  };

  if (downloading) {
    return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
      className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().containerImg),
      id: "image",
      onClick: downloadImage,
      children: [downloadImage(), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("main", {
        className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().image),
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
            className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().main),
            children: title(name && name[0])
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 185,
            columnNumber: 13
          }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
            style: {
              height: 40
            }
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 187,
            columnNumber: 13
          }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("p", {
            className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().descImg),
            children: _utils_birthdayWishes_js__WEBPACK_IMPORTED_MODULE_4__.default[randomNumber(0, _utils_birthdayWishes_js__WEBPACK_IMPORTED_MODULE_4__.default.length)].value
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 189,
            columnNumber: 13
          }, undefined)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 184,
          columnNumber: 11
        }, undefined)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 183,
        columnNumber: 9
      }, undefined)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 181,
      columnNumber: 7
    }, undefined);
  }

  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
    className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().container),
    children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)((next_head__WEBPACK_IMPORTED_MODULE_1___default()), {
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("title", {
        children: ["Happy Birthday ", name && name[0]]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 201,
        columnNumber: 9
      }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("meta", {
        name: "description",
        content: "Generated by create next app"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 202,
        columnNumber: 9
      }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("link", {
        rel: "icon",
        href: "/favicon.ico"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 203,
        columnNumber: 9
      }, undefined)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 200,
      columnNumber: 7
    }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("canvas", {
      className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().canvas),
      id: "canvas"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 206,
      columnNumber: 7
    }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("main", {
      className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().animate),
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().main) + ' hidden_it',
          children: title(name && name[0])
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 210,
          columnNumber: 11
        }, undefined)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 209,
        columnNumber: 9
      }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)(Example, {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 216,
        columnNumber: 9
      }, undefined)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 208,
      columnNumber: 7
    }, undefined), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("audio", {
      ref: audioRef,
      id: "player",
      autoPlay: true,
      children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("source", {
        src: "media/leemon_tree.mp3"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 240,
        columnNumber: 9
      }, undefined)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 239,
      columnNumber: 7
    }, undefined)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 199,
    columnNumber: 5
  }, undefined);
};

/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (Wish);

/***/ }),

/***/ "./utils/birthdayWishes.js":
/*!*********************************!*\
  !*** ./utils/birthdayWishes.js ***!
  \*********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
const messages = [{
  "value": "May you be gifted with life’s biggest joys and never-ending bliss. After all, you yourself are a gift to earth, so you deserve the best!"
}, {
  "value": "Count not the candles…see the lights they give. Count not the years, but the life you live. Wishing you a wonderful time ahead!"
}, {
  "value": "Forget the past; look forward to the future, for the best things are yet to come!"
}, {
  "value": "You are only young once, but you can be immature for a lifetime!"
}, {
  "value": "Cheers to you for another trip around the sun!"
}, {
  "value": "Wishing you a beautiful day with good health and happiness forever."
}, {
  "value": "To quote Shakespeare: ‘Party thine ass off!’"
}, {
  "value": "Don’t forget to smile awkwardly as everyone sings you happy birthday today!"
}, {
  "value": "Birthdays are nature’s way of telling us to eat more cake!"
}, {
  "value": "On your birthday I’m going to share the secret to staying young: lie about your age."
}, {
  "value": "Work hard. Play hard. Eat lots of cake. That’s a good motto for your birthday and for life."
}, {
  "value": "Hope you get birthday presents in the dozens!"
}, {
  "value": "May you have all the love your heart can hold, all the happiness a day can bring, and all the blessings a life can unfold."
}, {
  "value": "Be happy! Today is the day you were brought into this world to be a blessing and inspiration to the people around you! You are a wonderful person! May you be given more birthdays to fulfill all of your dreams!"
}, {
  "value": "Forget about the past, you can’t change it. Forget about the future, you can’t predict it. And forget about the present, I didn’t get you one. Happy birthday!"
}, {
  "value": "As you get older three things happen. The first is your memory goes, and I can’t remember the other two. Happy birthday!"
}, {
  "value": "When the little kids ask how old you are at your party, you should go ahead and tell them. While they’re distracted trying to count that high, you can steal a bite of their cake! Happy birthday!"
}, {
  "value": "Sending you smiles for every moment of your special day…Have a wonderful time and a very happy birthday!"
}, {
  "value": "Sending you a birthday wish wrapped with all my love. Have a very happy birthday!"
}, {
  "value": "A simple celebration, a gathering of friends; here wishing you great happiness and a joy that never ends. Happy birthday!"
}, {
  "value": "Hope your birthday is just like you…totally freaking awesome."
}, {
  "value": "I hope that today, at your party, you dance and others sing as you celebrate with joy your best birthday."
}, {
  "value": "It’s always a treat to wish happy birthday to someone so sweet."
}, {
  "value": "Hey you! It’s your birthday! Cake! Candles! Drinks! Presents! More food! Can you tell I’m excited?"
}, {
  "value": "Happy birthday to my forever young friend!"
}, {
  "value": "Birthdays only come once a year, and your friendship only comes once a lifetime."
}, {
  "value": "You deserve all the cake, happiness, and love today. Happy birthday!"
}, {
  "value": "Thank you for being the brightest person I know. Hope to see you shine for years on! Happy birthday!"
}];
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (messages);

/***/ }),

/***/ "./styles/Name.module.css":
/*!********************************!*\
  !*** ./styles/Name.module.css ***!
  \********************************/
/***/ ((module) => {

// Exports
module.exports = {
	"container": "Name_container__rsYCz",
	"containerImg": "Name_containerImg__2G3hb",
	"animate": "Name_animate__15zJa",
	"zoom": "Name_zoom__jRSdA",
	"canvas": "Name_canvas__15NY-",
	"main": "Name_main__3z7gK",
	"span": "Name_span__1jGq-",
	"desc": "Name_desc__32lu1",
	"descImg": "Name_descImg__3maM0",
	"title": "Name_title__20yBX",
	"wavyText": "Name_wavyText__3TvtA",
	"titleImg": "Name_titleImg__14eZ5",
	"buttonContainer": "Name_buttonContainer__voqA6"
};


/***/ }),

/***/ "./node_modules/react-responsive-carousel/lib/styles/carousel.min.css":
/*!****************************************************************************!*\
  !*** ./node_modules/react-responsive-carousel/lib/styles/carousel.min.css ***!
  \****************************************************************************/
/***/ (() => {



/***/ }),

/***/ "confetti-js":
/*!******************************!*\
  !*** external "confetti-js" ***!
  \******************************/
/***/ ((module) => {

"use strict";
module.exports = require("confetti-js");

/***/ }),

/***/ "file-saver":
/*!*****************************!*\
  !*** external "file-saver" ***!
  \*****************************/
/***/ ((module) => {

"use strict";
module.exports = require("file-saver");

/***/ }),

/***/ "html-to-image":
/*!********************************!*\
  !*** external "html-to-image" ***!
  \********************************/
/***/ ((module) => {

"use strict";
module.exports = require("html-to-image");

/***/ }),

/***/ "next/head":
/*!****************************!*\
  !*** external "next/head" ***!
  \****************************/
/***/ ((module) => {

"use strict";
module.exports = require("next/head");

/***/ }),

/***/ "next/router":
/*!******************************!*\
  !*** external "next/router" ***!
  \******************************/
/***/ ((module) => {

"use strict";
module.exports = require("next/router");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/***/ ((module) => {

"use strict";
module.exports = require("react");

/***/ }),

/***/ "react-responsive-carousel":
/*!********************************************!*\
  !*** external "react-responsive-carousel" ***!
  \********************************************/
/***/ ((module) => {

"use strict";
module.exports = require("react-responsive-carousel");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ ((module) => {

"use strict";
module.exports = require("react/jsx-dev-runtime");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./pages/[...name].js"));
module.exports = __webpack_exports__;

})();
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFnZXMvWy4uLm5hbWVdLmpzIiwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Q0FFQTs7QUFFQSxNQUFNRyxRQUFRLEdBQUcsTUFBTTtBQUVyQixRQUFNQyxNQUFNLEdBQUcsQ0FDYjtBQUFFQyxJQUFBQSxFQUFFLEVBQUUsQ0FBTjtBQUFTQyxJQUFBQSxJQUFJLEVBQUUsTUFBZjtBQUF1QkMsSUFBQUEsS0FBSyxFQUFFO0FBQTlCLEdBRGEsRUFFYjtBQUFFRixJQUFBQSxFQUFFLEVBQUUsQ0FBTjtBQUFTQyxJQUFBQSxJQUFJLEVBQUUsT0FBZjtBQUF3QkMsSUFBQUEsS0FBSyxFQUFFO0FBQS9CLEdBRmEsRUFHYjtBQUFFRixJQUFBQSxFQUFFLEVBQUUsQ0FBTjtBQUFTQyxJQUFBQSxJQUFJLEVBQUUsUUFBZjtBQUF5QkMsSUFBQUEsS0FBSyxFQUFFO0FBQWhDLEdBSGEsRUFJYjtBQUFFRixJQUFBQSxFQUFFLEVBQUUsQ0FBTjtBQUFTQyxJQUFBQSxJQUFJLEVBQUUsUUFBZjtBQUF5QkMsSUFBQUEsS0FBSyxFQUFFO0FBQWhDLEdBSmEsRUFLYjtBQUFFRixJQUFBQSxFQUFFLEVBQUUsQ0FBTjtBQUFTQyxJQUFBQSxJQUFJLEVBQUUsS0FBZjtBQUFzQkMsSUFBQUEsS0FBSyxFQUFFO0FBQTdCLEdBTGEsQ0FBZjtBQVFBLFFBQU07QUFBQSxPQUFDQyxZQUFEO0FBQUEsT0FBZUM7QUFBZixNQUFrQ1IsK0NBQVEsQ0FBQ0csTUFBTSxDQUFDLENBQUQsQ0FBUCxDQUFoRDs7QUFFQSxRQUFNTSxRQUFRLEdBQUlMLEVBQUQsSUFBUTtBQUN2QixVQUFNTSxhQUFhLEdBQUdQLE1BQU0sQ0FBQ1EsSUFBUCxDQUFhQyxJQUFELElBQVVSLEVBQUUsSUFBSVEsSUFBSSxDQUFDUixFQUFqQyxDQUF0QixDQUR1QixDQUV2Qjs7QUFDQSxRQUFJTSxhQUFKLEVBQW1CRixlQUFlLENBQUNFLGFBQUQsQ0FBZixDQUhJLENBSXZCO0FBQ0QsR0FMRDs7QUFPQVQsRUFBQUEsZ0RBQVMsQ0FBQyxNQUFNO0FBQ2QsUUFBSVksSUFBSSxHQUFHQyxRQUFRLENBQUNDLGVBQXBCO0FBQ0FGLElBQUFBLElBQUksQ0FBQ0csS0FBTCxDQUFXQyxXQUFYLENBQXVCLFNBQXZCLEVBQWtDVixZQUFZLENBQUNELEtBQS9DO0FBQ0QsR0FIUSxFQUdOLENBQUNDLFlBQUQsQ0FITSxDQUFUO0FBS0EsU0FBTztBQUFFSixJQUFBQSxNQUFGO0FBQVVNLElBQUFBLFFBQVY7QUFBb0JGLElBQUFBO0FBQXBCLEdBQVA7QUFDRCxDQXpCRDs7QUEyQkEsaUVBQWVMLFFBQWY7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDL0JBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFFQSxNQUFNeUIsT0FBTyxHQUFHLG1CQUNkO0FBQUEseUJBRUU7QUFBSyxTQUFLLEVBQUU7QUFBRUMsTUFBQUEsS0FBSyxFQUFFLE9BQVQ7QUFBa0JDLE1BQUFBLE1BQU0sRUFBRSxNQUExQjtBQUFrQ0MsTUFBQUEsTUFBTSxFQUFFO0FBQTFDLEtBQVo7QUFBQSwyQkFDRSwrREFBQywrREFBRDtBQUFVLGNBQVEsRUFBRSxJQUFwQjtBQUEwQixrQkFBWSxFQUFFLEtBQXhDO0FBQStDLGNBQVEsRUFBRSxJQUF6RDtBQUFBLDhCQUNFO0FBQUEsK0JBQ0U7QUFBSyxhQUFHLEVBQUMsU0FBVDtBQUFtQixhQUFHLEVBQUM7QUFBdkI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBREYsZUFLRTtBQUFBLCtCQUNFO0FBQUssYUFBRyxFQUFDLFNBQVQ7QUFBbUIsYUFBRyxFQUFDO0FBQXZCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQUxGLGVBVUU7QUFBQSwrQkFDRTtBQUFLLGFBQUcsRUFBQyxTQUFUO0FBQW1CLGFBQUcsRUFBQztBQUF2QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFWRixlQWNFO0FBQUEsZ0NBQ0U7QUFBSyxhQUFHLEVBQUMsU0FBVDtBQUFtQixhQUFHLEVBQUM7QUFBdkI7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFERixlQUVFO0FBQ0UsZUFBSyxFQUFFO0FBQUVDLFlBQUFBLFVBQVUsRUFBRSxTQUFkO0FBQXlCQyxZQUFBQSxRQUFRLEVBQUUsTUFBbkM7QUFBMkNDLFlBQUFBLE9BQU8sRUFBRTtBQUFwRCxXQURUO0FBRUUsbUJBQVMsRUFBQyxRQUZaO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFkRixlQXVCRTtBQUFBLGdDQUNFO0FBQUssYUFBRyxFQUFDLFNBQVQ7QUFBbUIsYUFBRyxFQUFDO0FBQXZCO0FBQUE7QUFBQTtBQUFBO0FBQUEscUJBREYsZUFFRTtBQUNFLGVBQUssRUFBRTtBQUFFRixZQUFBQSxVQUFVLEVBQUUsU0FBZDtBQUF5QkMsWUFBQUEsUUFBUSxFQUFFLE1BQW5DO0FBQTJDQyxZQUFBQSxPQUFPLEVBQUU7QUFBcEQsV0FEVDtBQUVFLG1CQUFTLEVBQUMsUUFGWjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBdkJGLGVBZ0NFO0FBQUEsZ0NBQ0U7QUFBSyxhQUFHLEVBQUMsU0FBVDtBQUFtQixhQUFHLEVBQUM7QUFBdkI7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFERixlQUVFO0FBQ0UsZUFBSyxFQUFFO0FBQUVGLFlBQUFBLFVBQVUsRUFBRSxTQUFkO0FBQXlCQyxZQUFBQSxRQUFRLEVBQUUsTUFBbkM7QUFBMkNDLFlBQUFBLE9BQU8sRUFBRTtBQUFwRCxXQURUO0FBRUUsbUJBQVMsRUFBQyxRQUZaO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFoQ0YsZUF5Q0U7QUFBQSxnQ0FDRTtBQUFLLGFBQUcsRUFBQyxTQUFUO0FBQW1CLGFBQUcsRUFBQztBQUF2QjtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQURGLGVBRUU7QUFDRSxlQUFLLEVBQUU7QUFBRUYsWUFBQUEsVUFBVSxFQUFFLFNBQWQ7QUFBeUJDLFlBQUFBLFFBQVEsRUFBRSxNQUFuQztBQUEyQ0MsWUFBQUEsT0FBTyxFQUFFO0FBQXBELFdBRFQ7QUFFRSxtQkFBUyxFQUFDLFFBRlo7QUFBQSxvTkFLYSxHQUxiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBekNGLGVBbURFO0FBQUEsK0JBQ0U7QUFBSyxhQUFHLEVBQUMsU0FBVDtBQUFtQixhQUFHLEVBQUM7QUFBdkI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBbkRGLGVBdURFO0FBQUEsK0JBQ0U7QUFBSyxhQUFHLEVBQUMsVUFBVDtBQUFvQixhQUFHLEVBQUM7QUFBeEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBdkRGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFGRjtBQUFBO0FBQUE7QUFBQTtBQUFBLGFBREY7O0FBc0VBLE1BQU1DLElBQUksR0FBRyxDQUFDO0FBQUVDLEVBQUFBO0FBQUYsQ0FBRCxLQUFpQjtBQUM1QixRQUFNQyxNQUFNLEdBQUdmLHNEQUFTLEVBQXhCO0FBQ0EsUUFBTTtBQUFFaEIsSUFBQUE7QUFBRixNQUFXK0IsTUFBTSxDQUFDQyxLQUF4QixDQUY0QixDQUVHOztBQUMvQixRQUFNL0IsS0FBSyxHQUFHRCxJQUFJLEdBQUdBLElBQUksQ0FBQyxDQUFELENBQVAsR0FBYSxDQUEvQixDQUg0QixDQUdNOztBQUNsQyxRQUFNO0FBQUEsT0FBQ2lDLFdBQUQ7QUFBQSxPQUFjQztBQUFkLE1BQWdDdkMsK0NBQVEsQ0FBQyxLQUFELENBQTlDO0FBQ0EsUUFBTTtBQUFBLE9BQUN3QyxjQUFEO0FBQUEsT0FBaUJDO0FBQWpCLE1BQXNDekMsK0NBQVEsQ0FBQyxLQUFELENBQXBEO0FBQ0EsUUFBTTBDLFFBQVEsR0FBR3hCLDZDQUFNLEVBQXZCO0FBRUEsUUFBTTtBQUFFVCxJQUFBQTtBQUFGLE1BQWVQLHdEQUFRLEVBQTdCO0FBRUFELEVBQUFBLGdEQUFTLENBQUMsTUFBTTtBQUNkO0FBQ0FRLElBQUFBLFFBQVEsQ0FBQ0gsS0FBRCxDQUFSOztBQUVBLFFBQUlnQyxXQUFXLEtBQUssS0FBcEIsRUFBMkI7QUFDekI7QUFDQSxZQUFNSyxnQkFBZ0IsR0FBRztBQUN2QkMsUUFBQUEsTUFBTSxFQUFFLFFBRGU7QUFFdkJDLFFBQUFBLGVBQWUsRUFBRTtBQUZNLE9BQXpCO0FBSUEsWUFBTUMsUUFBUSxHQUFHLElBQUl4QixvREFBSixDQUFzQnFCLGdCQUF0QixDQUFqQjtBQUNBRyxNQUFBQSxRQUFRLENBQUNDLE1BQVQ7QUFDRDs7QUFFREwsSUFBQUEsUUFBUSxDQUFDTSxPQUFULENBQWlCQyxJQUFqQjtBQUNELEdBZlEsRUFlTixDQUFDM0MsS0FBRCxFQUFRZ0MsV0FBUixDQWZNLENBQVQ7QUFpQkFyQyxFQUFBQSxnREFBUyxDQUFDLE1BQU07QUFDZCxRQUFJcUMsV0FBVyxLQUFLLElBQWhCLElBQXdCRSxjQUFjLEtBQUssS0FBL0MsRUFBc0Q7QUFDcERVLE1BQUFBLGFBQWE7QUFDZDtBQUNGLEdBSlEsRUFJTixDQUFDWixXQUFELEVBQWNFLGNBQWQsQ0FKTSxDQUFULENBM0I0QixDQWlDNUI7O0FBQ0EsUUFBTVcsWUFBWSxHQUFHLENBQUNDLEdBQUQsRUFBTUMsR0FBTixLQUFjO0FBQ2pDLFdBQU9DLElBQUksQ0FBQ0MsS0FBTCxDQUFXRCxJQUFJLENBQUNFLE1BQUwsTUFBaUJILEdBQUcsR0FBR0QsR0FBdkIsQ0FBWCxJQUEwQ0EsR0FBakQ7QUFDRCxHQUZEOztBQUlBLFFBQU1GLGFBQWEsR0FBRyxNQUFNO0FBQzFCLFFBQUlWLGNBQWMsS0FBSyxJQUF2QixFQUE2QjtBQUU3QixVQUFNaUIsSUFBSSxHQUFHM0MsUUFBUSxDQUFDNEMsY0FBVCxDQUF3QixPQUF4QixDQUFiOztBQUVBLFFBQUlELElBQUosRUFBVTtBQUNSaEIsTUFBQUEsaUJBQWlCLENBQUMsSUFBRCxDQUFqQjtBQUVBakIsTUFBQUEsZ0RBQUEsQ0FBa0JpQyxJQUFsQixFQUF3QkcsSUFBeEIsQ0FBOEJDLElBQUQsSUFBVTtBQUNyQ3BDLFFBQUFBLHdEQUFBLENBQWlCb0MsSUFBakIsRUFBdUIsbUJBQXZCO0FBQ0F0QixRQUFBQSxjQUFjLENBQUMsS0FBRCxDQUFkO0FBQ0QsT0FIRDtBQUlEO0FBQ0YsR0FiRDs7QUFlQSxRQUFNd0IsS0FBSyxHQUFJMUQsSUFBRCxJQUFVO0FBQ3RCLFVBQU0yRCxJQUFJLEdBQUcsb0JBQW9CM0QsSUFBcEIsR0FBMkIsR0FBeEM7QUFDQSxVQUFNNEQsWUFBWSxHQUFHLEVBQXJCO0FBQ0EsVUFBTUMsWUFBWSxHQUFHLEVBQXJCOztBQUVBLFNBQUssSUFBSUMsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR0gsSUFBSSxDQUFDSSxNQUF6QixFQUFpQ0QsQ0FBQyxFQUFsQyxFQUFzQztBQUNwQyxVQUFJQSxDQUFDLEdBQUcsRUFBUixFQUFZO0FBQ1YsY0FBTUUsTUFBTSxHQUFHTCxJQUFJLENBQUNNLE1BQUwsQ0FBWUgsQ0FBWixDQUFmO0FBQ0FGLFFBQUFBLFlBQVksQ0FBQ00sSUFBYixlQUNFO0FBQWMsZUFBSyxFQUFFO0FBQUUsbUJBQU9KLENBQUMsR0FBRztBQUFiLFdBQXJCO0FBQUEsb0JBQ0dFO0FBREgsV0FBV0YsQ0FBWDtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQURGO0FBS0QsT0FQRCxNQU9PO0FBQ0wsY0FBTUUsTUFBTSxHQUFHTCxJQUFJLENBQUNNLE1BQUwsQ0FBWUgsQ0FBWixDQUFmO0FBQ0FELFFBQUFBLFlBQVksQ0FBQ0ssSUFBYixlQUNFO0FBQWMsZUFBSyxFQUFFO0FBQUUsbUJBQU9KLENBQUMsR0FBRztBQUFiLFdBQXJCO0FBQXVDLG1CQUFTLEVBQUUvQyxzRUFBbEQ7QUFBQSxvQkFDR2lEO0FBREgsV0FBV0YsQ0FBWDtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQURGO0FBS0Q7QUFDRjs7QUFFRCx3QkFDRTtBQUFBLGdCQUNHN0IsV0FBVyxnQkFDVjtBQUNFLGlCQUFTLEVBQUVsQiwwRUFEYjtBQUVFLGFBQUssRUFBRTtBQUFFLDJCQUFpQjRDLElBQUksQ0FBQ0k7QUFBeEIsU0FGVDtBQUFBLGdDQUlFO0FBQUEsb0JBQU1ILFlBQVksQ0FBQ1MsR0FBYixDQUFrQkwsTUFBRCxJQUFZQSxNQUE3QjtBQUFOO0FBQUE7QUFBQTtBQUFBO0FBQUEscUJBSkYsZUFLRTtBQUFBLG9CQUFNSCxZQUFZLENBQUNRLEdBQWIsQ0FBa0JMLE1BQUQsSUFBWUEsTUFBN0I7QUFBTjtBQUFBO0FBQUE7QUFBQTtBQUFBLHFCQUxGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFEVSxnQkFTVjtBQUFJLGlCQUFTLEVBQUVqRCx1RUFBZjtBQUE2QixhQUFLLEVBQUU7QUFBRSwyQkFBaUI0QyxJQUFJLENBQUNJO0FBQXhCLFNBQXBDO0FBQUEsZ0NBQ0U7QUFBQSxvQkFBTUgsWUFBWSxDQUFDUyxHQUFiLENBQWtCTCxNQUFELElBQVlBLE1BQTdCO0FBQU47QUFBQTtBQUFBO0FBQUE7QUFBQSxxQkFERixlQUVFO0FBQUEsb0JBQU1ILFlBQVksQ0FBQ1EsR0FBYixDQUFrQkwsTUFBRCxJQUFZQSxNQUE3QjtBQUFOO0FBQUE7QUFBQTtBQUFBO0FBQUEscUJBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBVkoscUJBREY7QUFrQkQsR0F6Q0Q7O0FBMkNBLE1BQUkvQixXQUFKLEVBQWlCO0FBQ2Ysd0JBQ0U7QUFBSyxlQUFTLEVBQUVsQiw4RUFBaEI7QUFBcUMsUUFBRSxFQUFDLE9BQXhDO0FBQWdELGFBQU8sRUFBRThCLGFBQXpEO0FBQUEsaUJBQ0dBLGFBQWEsRUFEaEIsZUFFRTtBQUFNLGlCQUFTLEVBQUU5Qix1RUFBakI7QUFBQSwrQkFDRTtBQUFBLGtDQUNFO0FBQUsscUJBQVMsRUFBRUEsc0VBQWhCO0FBQUEsc0JBQThCMkMsS0FBSyxDQUFDMUQsSUFBSSxJQUFJQSxJQUFJLENBQUMsQ0FBRCxDQUFiO0FBQW5DO0FBQUE7QUFBQTtBQUFBO0FBQUEsdUJBREYsZUFHRTtBQUFLLGlCQUFLLEVBQUU7QUFBRXlCLGNBQUFBLE1BQU0sRUFBRTtBQUFWO0FBQVo7QUFBQTtBQUFBO0FBQUE7QUFBQSx1QkFIRixlQUtFO0FBQUcscUJBQVMsRUFBRVYseUVBQWQ7QUFBQSxzQkFDR0csNkRBQVEsQ0FBQzRCLFlBQVksQ0FBQyxDQUFELEVBQUk1QixvRUFBSixDQUFiLENBQVIsQ0FBMkN3RDtBQUQ5QztBQUFBO0FBQUE7QUFBQTtBQUFBLHVCQUxGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQURGO0FBZ0JEOztBQUVELHNCQUNFO0FBQUssYUFBUyxFQUFFM0QsMkVBQWhCO0FBQUEsNEJBQ0UsK0RBQUMsa0RBQUQ7QUFBQSw4QkFDRTtBQUFBLHNDQUF1QmYsSUFBSSxJQUFJQSxJQUFJLENBQUMsQ0FBRCxDQUFuQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBREYsZUFFRTtBQUFNLFlBQUksRUFBQyxhQUFYO0FBQXlCLGVBQU8sRUFBQztBQUFqQztBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQUZGLGVBR0U7QUFBTSxXQUFHLEVBQUMsTUFBVjtBQUFpQixZQUFJLEVBQUM7QUFBdEI7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFIRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBREYsZUFPRTtBQUFRLGVBQVMsRUFBRWUsd0VBQW5CO0FBQW1DLFFBQUUsRUFBQztBQUF0QztBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVBGLGVBU0U7QUFBTSxlQUFTLEVBQUVBLHlFQUFqQjtBQUFBLDhCQUNFO0FBQUEsK0JBQ0U7QUFBSyxtQkFBUyxFQUFFQSxzRUFBQSxHQUFhLFlBQTdCO0FBQUEsb0JBQTRDMkMsS0FBSyxDQUFDMUQsSUFBSSxJQUFJQSxJQUFJLENBQUMsQ0FBRCxDQUFiO0FBQWpEO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQURGLGVBUUUsK0RBQUMsT0FBRDtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQVJGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFURixlQXdDRTtBQUFPLFNBQUcsRUFBRXFDLFFBQVo7QUFBc0IsUUFBRSxFQUFDLFFBQXpCO0FBQWtDLGNBQVEsTUFBMUM7QUFBQSw2QkFDRTtBQUFRLFdBQUcsRUFBQztBQUFaO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQXhDRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUFERjtBQThDRCxDQWpLRDs7QUFtS0EsaUVBQWVSLElBQWY7Ozs7Ozs7Ozs7Ozs7OztBQ3JQQyxNQUFNWCxRQUFRLEdBQUcsQ0FDZDtBQUFDLFdBQVE7QUFBVCxDQURjLEVBRWQ7QUFBQyxXQUFRO0FBQVQsQ0FGYyxFQUdkO0FBQUMsV0FBUTtBQUFULENBSGMsRUFJZDtBQUFDLFdBQVE7QUFBVCxDQUpjLEVBS2Q7QUFBQyxXQUFRO0FBQVQsQ0FMYyxFQU1kO0FBQUMsV0FBUTtBQUFULENBTmMsRUFPZDtBQUFDLFdBQVE7QUFBVCxDQVBjLEVBUWQ7QUFBQyxXQUFRO0FBQVQsQ0FSYyxFQVNkO0FBQUMsV0FBUTtBQUFULENBVGMsRUFVZDtBQUFDLFdBQVE7QUFBVCxDQVZjLEVBV2Q7QUFBQyxXQUFRO0FBQVQsQ0FYYyxFQVlkO0FBQUMsV0FBUTtBQUFULENBWmMsRUFhZDtBQUFDLFdBQVE7QUFBVCxDQWJjLEVBY2Q7QUFBQyxXQUFRO0FBQVQsQ0FkYyxFQWVkO0FBQUMsV0FBUTtBQUFULENBZmMsRUFnQmQ7QUFBQyxXQUFRO0FBQVQsQ0FoQmMsRUFpQmQ7QUFBQyxXQUFRO0FBQVQsQ0FqQmMsRUFrQmQ7QUFBQyxXQUFRO0FBQVQsQ0FsQmMsRUFtQmQ7QUFBQyxXQUFRO0FBQVQsQ0FuQmMsRUFvQmQ7QUFBQyxXQUFRO0FBQVQsQ0FwQmMsRUFxQmQ7QUFBQyxXQUFRO0FBQVQsQ0FyQmMsRUFzQmQ7QUFBQyxXQUFRO0FBQVQsQ0F0QmMsRUF1QmQ7QUFBQyxXQUFRO0FBQVQsQ0F2QmMsRUF3QmQ7QUFBQyxXQUFRO0FBQVQsQ0F4QmMsRUF5QmQ7QUFBQyxXQUFRO0FBQVQsQ0F6QmMsRUEwQmQ7QUFBQyxXQUFRO0FBQVQsQ0ExQmMsRUEyQmQ7QUFBQyxXQUFRO0FBQVQsQ0EzQmMsRUE0QmQ7QUFBQyxXQUFRO0FBQVQsQ0E1QmMsQ0FBakI7QUFnQ0QsaUVBQWVBLFFBQWY7Ozs7Ozs7Ozs7QUNoQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUNmQTs7Ozs7Ozs7Ozs7QUNBQTs7Ozs7Ozs7Ozs7QUNBQTs7Ozs7Ozs7Ozs7QUNBQTs7Ozs7Ozs7Ozs7QUNBQTs7Ozs7Ozs7Ozs7QUNBQTs7Ozs7Ozs7Ozs7QUNBQTs7Ozs7Ozs7Ozs7QUNBQSIsInNvdXJjZXMiOlsid2VicGFjazovL2JpcnRoZGF5LXdpc2gvLi9ob29rcy91c2VUaGVtZS5qcyIsIndlYnBhY2s6Ly9iaXJ0aGRheS13aXNoLy4vcGFnZXMvWy4uLm5hbWVdLmpzIiwid2VicGFjazovL2JpcnRoZGF5LXdpc2gvLi91dGlscy9iaXJ0aGRheVdpc2hlcy5qcyIsIndlYnBhY2s6Ly9iaXJ0aGRheS13aXNoLy4vc3R5bGVzL05hbWUubW9kdWxlLmNzcyIsIndlYnBhY2s6Ly9iaXJ0aGRheS13aXNoL2V4dGVybmFsIFwiY29uZmV0dGktanNcIiIsIndlYnBhY2s6Ly9iaXJ0aGRheS13aXNoL2V4dGVybmFsIFwiZmlsZS1zYXZlclwiIiwid2VicGFjazovL2JpcnRoZGF5LXdpc2gvZXh0ZXJuYWwgXCJodG1sLXRvLWltYWdlXCIiLCJ3ZWJwYWNrOi8vYmlydGhkYXktd2lzaC9leHRlcm5hbCBcIm5leHQvaGVhZFwiIiwid2VicGFjazovL2JpcnRoZGF5LXdpc2gvZXh0ZXJuYWwgXCJuZXh0L3JvdXRlclwiIiwid2VicGFjazovL2JpcnRoZGF5LXdpc2gvZXh0ZXJuYWwgXCJyZWFjdFwiIiwid2VicGFjazovL2JpcnRoZGF5LXdpc2gvZXh0ZXJuYWwgXCJyZWFjdC1yZXNwb25zaXZlLWNhcm91c2VsXCIiLCJ3ZWJwYWNrOi8vYmlydGhkYXktd2lzaC9leHRlcm5hbCBcInJlYWN0L2pzeC1kZXYtcnVudGltZVwiIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCwgeyB1c2VTdGF0ZSwgdXNlRWZmZWN0IH0gZnJvbSBcInJlYWN0XCI7XHJcblxyXG4vL2NvbG9ycyBsaXN0XHJcblxyXG5jb25zdCB1c2VUaGVtZSA9ICgpID0+IHtcclxuXHJcbiAgY29uc3QgdGhlbWVzID0gW1xyXG4gICAgeyBpZDogMCwgbmFtZTogXCJibHVlXCIsIGNvbG9yOiBcIiMwMDcwZjNcIiB9LFxyXG4gICAgeyBpZDogMSwgbmFtZTogXCJncmVlblwiLCBjb2xvcjogXCIjMTBCOTgxXCIgfSxcclxuICAgIHsgaWQ6IDIsIG5hbWU6IFwidmlvbGV0XCIsIGNvbG9yOiBcIiM4QjVDRjZcIiB9LFxyXG4gICAgeyBpZDogMywgbmFtZTogXCJ5ZWxsb3dcIiwgY29sb3I6IFwiI0ZCQkYyNFwiIH0sXHJcbiAgICB7IGlkOiA0LCBuYW1lOiBcInJlZFwiLCBjb2xvcjogXCIjRTExRDQ4XCIgfSxcclxuICBdO1xyXG5cclxuICBjb25zdCBbY3VycmVudFRoZW1lLCBzZXRDdXJyZW50VGhlbWVdID0gdXNlU3RhdGUodGhlbWVzWzBdKTtcclxuXHJcbiAgY29uc3Qgc2V0VGhlbWUgPSAoaWQpID0+IHtcclxuICAgIGNvbnN0IHJlcXVpcmVkVGhlbWUgPSB0aGVtZXMuZmluZCgoaXRlbSkgPT4gaWQgPT0gaXRlbS5pZCk7XHJcbiAgICAvLyBJZiB0aGUgdGhlbWUgd2l0aCB0aGUgZ2l2ZW4gaWQgZXhpc3RzIHRoZW4gY2hhbmdlIHRoZW1lIC5cclxuICAgIGlmIChyZXF1aXJlZFRoZW1lKSBzZXRDdXJyZW50VGhlbWUocmVxdWlyZWRUaGVtZSk7XHJcbiAgICAvLyBJZiB0aGUgdGhlbWUgd2l0aCB0aGUgZ2l2ZW4gaWQgZG9lcyBub3QgZXhpc3QgdGhlbiBpdCBkb2VzbnQgY2hhbmdlIHRoZSBkZWZhdWx0IHRoZW1lO1xyXG4gIH07XHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICBsZXQgcm9vdCA9IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudDtcclxuICAgIHJvb3Quc3R5bGUuc2V0UHJvcGVydHkoXCItLWNvbG9yXCIsIGN1cnJlbnRUaGVtZS5jb2xvcik7XHJcbiAgfSwgW2N1cnJlbnRUaGVtZV0pO1xyXG5cclxuICByZXR1cm4geyB0aGVtZXMsIHNldFRoZW1lLCBjdXJyZW50VGhlbWUgfTtcclxufTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IHVzZVRoZW1lO1xyXG4iLCJpbXBvcnQgUmVhY3QsIHsgdXNlU3RhdGUsIHVzZUVmZmVjdCwgdXNlUmVmIH0gZnJvbSBcInJlYWN0XCI7XHJcbmltcG9ydCBIZWFkIGZyb20gXCJuZXh0L2hlYWRcIjtcclxuaW1wb3J0IHN0eWxlcyBmcm9tIFwiLi4vc3R5bGVzL05hbWUubW9kdWxlLmNzc1wiO1xyXG5pbXBvcnQgeyB1c2VSb3V0ZXIgfSBmcm9tIFwibmV4dC9yb3V0ZXJcIjtcclxuaW1wb3J0IENvbmZldHRpR2VuZXJhdG9yIGZyb20gXCJjb25mZXR0aS1qc1wiO1xyXG5pbXBvcnQgbWVzc2FnZXMgZnJvbSBcIi4uL3V0aWxzL2JpcnRoZGF5V2lzaGVzLmpzXCI7XHJcbmltcG9ydCB1c2VUaGVtZSBmcm9tIFwiLi4vaG9va3MvdXNlVGhlbWVcIjtcclxuaW1wb3J0ICogYXMgaHRtbFRvSW1hZ2UgZnJvbSBcImh0bWwtdG8taW1hZ2VcIjtcclxuaW1wb3J0IEZpbGVTYXZlciBmcm9tIFwiZmlsZS1zYXZlclwiO1xyXG5pbXBvcnQgXCJyZWFjdC1yZXNwb25zaXZlLWNhcm91c2VsL2xpYi9zdHlsZXMvY2Fyb3VzZWwubWluLmNzc1wiO1xyXG5pbXBvcnQgeyBDYXJvdXNlbCB9IGZyb20gXCJyZWFjdC1yZXNwb25zaXZlLWNhcm91c2VsXCI7XHJcblxyXG5jb25zdCBFeGFtcGxlID0gKCkgPT4gKFxyXG4gIDxkaXY+XHJcbiAgICB7LyogPGgyPkNow7pjIGFuaCBuZ8OgeSBzaW5oIG5o4bqtdCB0aOG6rXQgdnVpIHbhurssIGx1w7RuIG3huqFuaCBraG/hurssIGfhurd0IG5oaeG7gXUgdGjDoG5oIGPDtG5nIGjGoW4gbuG7r2EgYW5oIG5ow6khPC9oMj4gKi99XHJcbiAgICA8ZGl2IHN0eWxlPXt7IHdpZHRoOiBcIjYwMHB4XCIsIG1hcmdpbjogXCJhdXRvXCIsIGhlaWdodDogXCI2MDBweFwifX0+XHJcbiAgICAgIDxDYXJvdXNlbCBhdXRvUGxheT17dHJ1ZX0gaW5maW5pdGVMb29wPXtmYWxzZX0gaW50ZXJ2YWw9ezUwMDB9PlxyXG4gICAgICAgIDxkaXY+XHJcbiAgICAgICAgICA8aW1nIHNyYz1cIi9oMi5qcGdcIiBhbHQ9XCJpbWFnZTFcIiAvPlxyXG4gICAgICAgICAgey8qIDxwIHN0eWxlPXt7YmFja2dyb3VuZDpcIiM3RkI3N0VcIiwgZm9udFNpemU6XCIyNnB4XCIsIG9wYWNpdHk6XCIxXCJ9fSBjbGFzc05hbWU9XCJsZWdlbmRcIj48L3A+ICovfVxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDxkaXY+XHJcbiAgICAgICAgICA8aW1nIHNyYz1cIi9oMy5qcGdcIiBhbHQ9XCJpbWFnZTJcIiAvPlxyXG4gICAgICAgICAgey8qIDxwIHN0eWxlPXt7YmFja2dyb3VuZDpcIiM3RkI3N0VcIiwgZm9udFNpemU6XCIyNnB4XCIsIG9wYWNpdHk6XCIxXCJ9fSBjbGFzc05hbWU9XCJsZWdlbmRcIj5D4bqjbSDGoW4gYW5oIHRo4bqtdCBuaGnhu4F1IHbDrC4uLjwvcD4gKi99XHJcbiAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgIDxkaXY+XHJcbiAgICAgICAgICA8aW1nIHNyYz1cIi9oNC5qcGdcIiBhbHQ9XCJpbWFnZTNcIiAvPlxyXG4gICAgICAgICAgey8qIDxwIHN0eWxlPXt7YmFja2dyb3VuZDpcIiM3RkI3N0VcIiwgZm9udFNpemU6XCIyNnB4XCIsIG9wYWNpdHk6XCIxXCJ9fSBjbGFzc05hbWU9XCJsZWdlbmRcIj5D4bqjbSDGoW4gYW5oIHRo4bqtdCBuaGnhu4F1IHbDrC4uLjwvcD4gKi99XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPGRpdj5cclxuICAgICAgICAgIDxpbWcgc3JjPVwiL2g1LmpwZ1wiIGFsdD1cImltYWdlNFwiIC8+XHJcbiAgICAgICAgICA8cFxyXG4gICAgICAgICAgICBzdHlsZT17eyBiYWNrZ3JvdW5kOiBcIiM3RkI3N0VcIiwgZm9udFNpemU6IFwiMjZweFwiLCBvcGFjaXR5OiBcIjFcIiB9fVxyXG4gICAgICAgICAgICBjbGFzc05hbWU9XCJsZWdlbmRcIlxyXG4gICAgICAgICAgPlxyXG4gICAgICAgICAgICBMdcO0biBsYcyAIG5nxrDGocyAaSBhbmggY2HMiSBjdcyJYSBjaHXMgW5nIGVtXHJcbiAgICAgICAgICA8L3A+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPGRpdj5cclxuICAgICAgICAgIDxpbWcgc3JjPVwiL2g2LmpwZ1wiIGFsdD1cImltYWdlNVwiIC8+XHJcbiAgICAgICAgICA8cFxyXG4gICAgICAgICAgICBzdHlsZT17eyBiYWNrZ3JvdW5kOiBcIiM3RkI3N0VcIiwgZm9udFNpemU6IFwiMjZweFwiLCBvcGFjaXR5OiBcIjFcIiB9fVxyXG4gICAgICAgICAgICBjbGFzc05hbWU9XCJsZWdlbmRcIlxyXG4gICAgICAgICAgPlxyXG4gICAgICAgICAgICAzIHBow6LMgG4gecOqdSB0aMawxqFuZyA3IHBow6LMgG4gbnXDtG5nIGNoacOqzIB1IGNodcyBbmcgZW1cclxuICAgICAgICAgIDwvcD5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgPGltZyBzcmM9XCIvaDcuanBnXCIgYWx0PVwiaW1hZ2U2XCIgLz5cclxuICAgICAgICAgIDxwXHJcbiAgICAgICAgICAgIHN0eWxlPXt7IGJhY2tncm91bmQ6IFwiIzdGQjc3RVwiLCBmb250U2l6ZTogXCIyNnB4XCIsIG9wYWNpdHk6IFwiMVwiIH19XHJcbiAgICAgICAgICAgIGNsYXNzTmFtZT1cImxlZ2VuZFwiXHJcbiAgICAgICAgICA+XHJcbiAgICAgICAgICAgIFRoaSB0aG9hzIluZyBkacyBIGRlYWRsaW5lIGNodcyBbmcgZW0gYsOizIF0IGvDqsyJIG5nYcyAeSDEkcOqbVxyXG4gICAgICAgICAgPC9wPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDxkaXY+XHJcbiAgICAgICAgICA8aW1nIHNyYz1cIi9oOC5qcGdcIiBhbHQ9XCJpbWFnZTdcIiAvPlxyXG4gICAgICAgICAgPHBcclxuICAgICAgICAgICAgc3R5bGU9e3sgYmFja2dyb3VuZDogXCIjN0ZCNzdFXCIsIGZvbnRTaXplOiBcIjI2cHhcIiwgb3BhY2l0eTogXCIxXCIgfX1cclxuICAgICAgICAgICAgY2xhc3NOYW1lPVwibGVnZW5kXCJcclxuICAgICAgICAgID5cclxuICAgICAgICAgICAgVmHMgCBjdcO0zIFpIGN1zIBuZyBsYcyAIMSQYcyDIHnDqnUgdGjGsMahbmcsIHRhzKNvIMSRacOqzIB1IGtpw6rMo24gY2hvIGNodcyBbmcgZW0gdHLGsMahzIluZ1xyXG4gICAgICAgICAgICB0aGHMgG5oIGjGoW57XCIgXCJ9XHJcbiAgICAgICAgICA8L3A+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPGRpdj5cclxuICAgICAgICAgIDxpbWcgc3JjPVwiL2g5LmpwZ1wiIGFsdD1cImltYWdlOFwiIC8+XHJcbiAgICAgICAgICB7LyogPHAgc3R5bGU9e3tiYWNrZ3JvdW5kOlwiIzdGQjc3RVwiLCBmb250U2l6ZTpcIjI2cHhcIiwgb3BhY2l0eTpcIjFcIn19IGNsYXNzTmFtZT1cImxlZ2VuZFwiPlZhzIAgY3XDtMyBaSBjdcyAbmcgbGHMgCAgxJBhzIMgecOqdSB0aMawxqFuZywgdGHMo28gxJFpw6rMgHUga2nDqsyjbiBjaG8gY2h1zIFuZyBlbSB0csawxqHMiW5nIHRoYcyAbmggaMahbiA8L3A+ICovfVxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDxkaXY+XHJcbiAgICAgICAgICA8aW1nIHNyYz1cIi9oMTAuanBnXCIgYWx0PVwiaW1hZ2U5XCIgLz5cclxuICAgICAgICAgIHsvKiA8cCBzdHlsZT17e2JhY2tncm91bmQ6XCIjN0ZCNzdFXCIsIGZvbnRTaXplOlwiMjZweFwiLCBvcGFjaXR5OlwiMVwifX0gY2xhc3NOYW1lPVwibGVnZW5kXCI+VmHMgCBjdcO0zIFpIGN1zIBuZyBsYcyAICDEkGHMgyB5w6p1IHRoxrDGoW5nLCB0YcyjbyDEkWnDqsyAdSBracOqzKNuIGNobyBjaHXMgW5nIGVtIHRyxrDGocyJbmcgdGhhzIBuaCBoxqFuIDwvcD4gKi99XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgIDwvQ2Fyb3VzZWw+XHJcbiAgICA8L2Rpdj5cclxuICA8L2Rpdj5cclxuKTtcclxuXHJcblxyXG5cclxuY29uc3QgV2lzaCA9ICh7IGhpc3RvcnkgfSkgPT4ge1xyXG4gIGNvbnN0IHJvdXRlciA9IHVzZVJvdXRlcigpO1xyXG4gIGNvbnN0IHsgbmFtZSB9ID0gcm91dGVyLnF1ZXJ5OyAvLyBnZXRzIGJvdGggbmFtZSAmIGNvbG9yIGlkIGluIGZvcm0gb2YgYXJyYXkgW25hbWUsY29sb3JJZF1cclxuICBjb25zdCBjb2xvciA9IG5hbWUgPyBuYW1lWzFdIDogMDsgLy9leHRyYWN0aW5nIGNvbG9ySWQgZnJvbSBuYW1lXHJcbiAgY29uc3QgW2Rvd25sb2FkaW5nLCBzZXREb3dubG9hZGluZ10gPSB1c2VTdGF0ZShmYWxzZSk7XHJcbiAgY29uc3QgW2Rvd25sb2FkZWRPbmNlLCBzZXREb3dubG9hZGVkT25jZV0gPSB1c2VTdGF0ZShmYWxzZSk7XHJcbiAgY29uc3QgYXVkaW9SZWYgPSB1c2VSZWYoKTtcclxuXHJcbiAgY29uc3QgeyBzZXRUaGVtZSB9ID0gdXNlVGhlbWUoKTtcclxuXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIC8vIFRoZW1lIENoYW5nZVxyXG4gICAgc2V0VGhlbWUoY29sb3IpO1xyXG5cclxuICAgIGlmIChkb3dubG9hZGluZyA9PT0gZmFsc2UpIHtcclxuICAgICAgLy8gQ29uZmV0dGlcclxuICAgICAgY29uc3QgY29uZmV0dGlTZXR0aW5ncyA9IHtcclxuICAgICAgICB0YXJnZXQ6IFwiY2FudmFzXCIsXHJcbiAgICAgICAgc3RhcnRfZnJvbV9lZGdlOiB0cnVlLFxyXG4gICAgICB9O1xyXG4gICAgICBjb25zdCBjb25mZXR0aSA9IG5ldyBDb25mZXR0aUdlbmVyYXRvcihjb25mZXR0aVNldHRpbmdzKTtcclxuICAgICAgY29uZmV0dGkucmVuZGVyKCk7XHJcbiAgICB9XHJcblxyXG4gICAgYXVkaW9SZWYuY3VycmVudC5wbGF5KCk7XHJcbiAgfSwgW2NvbG9yLCBkb3dubG9hZGluZ10pO1xyXG5cclxuICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgaWYgKGRvd25sb2FkaW5nID09PSB0cnVlICYmIGRvd25sb2FkZWRPbmNlID09PSBmYWxzZSkge1xyXG4gICAgICBkb3dubG9hZEltYWdlKCk7XHJcbiAgICB9XHJcbiAgfSwgW2Rvd25sb2FkaW5nLCBkb3dubG9hZGVkT25jZV0pO1xyXG5cclxuICAvLyBmdW5jdGlvbiBmb3IgcmFuZG9tbHkgcGlja2luZyB0aGUgbWVzc2FnZSBmcm9tIG1lc3NhZ2VzIGFycmF5XHJcbiAgY29uc3QgcmFuZG9tTnVtYmVyID0gKG1pbiwgbWF4KSA9PiB7XHJcbiAgICByZXR1cm4gTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpICogKG1heCAtIG1pbikpICsgbWluO1xyXG4gIH07XHJcblxyXG4gIGNvbnN0IGRvd25sb2FkSW1hZ2UgPSAoKSA9PiB7XHJcbiAgICBpZiAoZG93bmxvYWRlZE9uY2UgPT09IHRydWUpIHJldHVybjtcclxuXHJcbiAgICBjb25zdCBub2RlID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJpbWFnZVwiKTtcclxuXHJcbiAgICBpZiAobm9kZSkge1xyXG4gICAgICBzZXREb3dubG9hZGVkT25jZSh0cnVlKTtcclxuXHJcbiAgICAgIGh0bWxUb0ltYWdlLnRvUG5nKG5vZGUpLnRoZW4oKGJsb2IpID0+IHtcclxuICAgICAgICBGaWxlU2F2ZXIuc2F2ZUFzKGJsb2IsIFwiYmlydGhkYXktd2lzaC5wbmdcIik7XHJcbiAgICAgICAgc2V0RG93bmxvYWRpbmcoZmFsc2UpO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuICB9O1xyXG5cclxuICBjb25zdCB0aXRsZSA9IChuYW1lKSA9PiB7XHJcbiAgICBjb25zdCB3aXNoID0gXCJIYXBweSBCaXJ0aGRheSBcIiArIG5hbWUgKyBcIiFcIjtcclxuICAgIGNvbnN0IGJhc2VfbGV0dGVycyA9IFtdO1xyXG4gICAgY29uc3QgbmFtZV9sZXR0ZXJzID0gW107XHJcblxyXG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCB3aXNoLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgIGlmIChpIDwgMTUpIHtcclxuICAgICAgICBjb25zdCBsZXR0ZXIgPSB3aXNoLmNoYXJBdChpKTtcclxuICAgICAgICBiYXNlX2xldHRlcnMucHVzaChcclxuICAgICAgICAgIDxzcGFuIGtleT17aX0gc3R5bGU9e3sgXCItLWlcIjogaSArIDEgfX0+XHJcbiAgICAgICAgICAgIHtsZXR0ZXJ9XHJcbiAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBjb25zdCBsZXR0ZXIgPSB3aXNoLmNoYXJBdChpKTtcclxuICAgICAgICBuYW1lX2xldHRlcnMucHVzaChcclxuICAgICAgICAgIDxzcGFuIGtleT17aX0gc3R5bGU9e3sgXCItLWlcIjogaSArIDEgfX0gY2xhc3NOYW1lPXtzdHlsZXMuc3Bhbn0+XHJcbiAgICAgICAgICAgIHtsZXR0ZXJ9XHJcbiAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiAoXHJcbiAgICAgIDw+XHJcbiAgICAgICAge2Rvd25sb2FkaW5nID8gKFxyXG4gICAgICAgICAgPGgxXHJcbiAgICAgICAgICAgIGNsYXNzTmFtZT17c3R5bGVzLnRpdGxlSW1nfVxyXG4gICAgICAgICAgICBzdHlsZT17eyBcIi0td2lzaC1sZW5ndGhcIjogd2lzaC5sZW5ndGggfX1cclxuICAgICAgICAgID5cclxuICAgICAgICAgICAgPGRpdj57YmFzZV9sZXR0ZXJzLm1hcCgobGV0dGVyKSA9PiBsZXR0ZXIpfTwvZGl2PlxyXG4gICAgICAgICAgICA8ZGl2PntuYW1lX2xldHRlcnMubWFwKChsZXR0ZXIpID0+IGxldHRlcil9PC9kaXY+XHJcbiAgICAgICAgICA8L2gxPlxyXG4gICAgICAgICkgOiAoXHJcbiAgICAgICAgICA8aDEgY2xhc3NOYW1lPXtzdHlsZXMudGl0bGV9IHN0eWxlPXt7IFwiLS13aXNoLWxlbmd0aFwiOiB3aXNoLmxlbmd0aCB9fT5cclxuICAgICAgICAgICAgPGRpdj57YmFzZV9sZXR0ZXJzLm1hcCgobGV0dGVyKSA9PiBsZXR0ZXIpfTwvZGl2PlxyXG4gICAgICAgICAgICA8ZGl2PntuYW1lX2xldHRlcnMubWFwKChsZXR0ZXIpID0+IGxldHRlcil9PC9kaXY+XHJcbiAgICAgICAgICA8L2gxPlxyXG4gICAgICAgICl9XHJcbiAgICAgIDwvPlxyXG4gICAgKTtcclxuICB9O1xyXG5cclxuICBpZiAoZG93bmxvYWRpbmcpIHtcclxuICAgIHJldHVybiAoXHJcbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzdHlsZXMuY29udGFpbmVySW1nfSBpZD1cImltYWdlXCIgb25DbGljaz17ZG93bmxvYWRJbWFnZX0+XHJcbiAgICAgICAge2Rvd25sb2FkSW1hZ2UoKX1cclxuICAgICAgICA8bWFpbiBjbGFzc05hbWU9e3N0eWxlcy5pbWFnZX0+XHJcbiAgICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17c3R5bGVzLm1haW59Pnt0aXRsZShuYW1lICYmIG5hbWVbMF0pfTwvZGl2PlxyXG5cclxuICAgICAgICAgICAgPGRpdiBzdHlsZT17eyBoZWlnaHQ6IDQwIH19IC8+XHJcblxyXG4gICAgICAgICAgICA8cCBjbGFzc05hbWU9e3N0eWxlcy5kZXNjSW1nfT5cclxuICAgICAgICAgICAgICB7bWVzc2FnZXNbcmFuZG9tTnVtYmVyKDAsIG1lc3NhZ2VzLmxlbmd0aCldLnZhbHVlfVxyXG4gICAgICAgICAgICA8L3A+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L21haW4+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgKTtcclxuICB9XHJcblxyXG4gIHJldHVybiAoXHJcbiAgICA8ZGl2IGNsYXNzTmFtZT17c3R5bGVzLmNvbnRhaW5lcn0+XHJcbiAgICAgIDxIZWFkPlxyXG4gICAgICAgIDx0aXRsZT5IYXBweSBCaXJ0aGRheSB7bmFtZSAmJiBuYW1lWzBdfTwvdGl0bGU+XHJcbiAgICAgICAgPG1ldGEgbmFtZT1cImRlc2NyaXB0aW9uXCIgY29udGVudD1cIkdlbmVyYXRlZCBieSBjcmVhdGUgbmV4dCBhcHBcIiAvPlxyXG4gICAgICAgIDxsaW5rIHJlbD1cImljb25cIiBocmVmPVwiL2Zhdmljb24uaWNvXCIgLz5cclxuICAgICAgPC9IZWFkPlxyXG5cclxuICAgICAgPGNhbnZhcyBjbGFzc05hbWU9e3N0eWxlcy5jYW52YXMgfSBpZD1cImNhbnZhc1wiPjwvY2FudmFzPlxyXG5cclxuICAgICAgPG1haW4gY2xhc3NOYW1lPXtzdHlsZXMuYW5pbWF0ZX0+XHJcbiAgICAgICAgPGRpdj5cclxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzdHlsZXMubWFpbiArJyBoaWRkZW5faXQnfT57dGl0bGUobmFtZSAmJiBuYW1lWzBdKX08L2Rpdj5cclxuICAgICAgICAgIHsvKiA8cCBjbGFzc05hbWU9e3N0eWxlcy5kZXNjfT5cclxuICAgICAgICAgICAge21lc3NhZ2VzW3JhbmRvbU51bWJlcigwLCBtZXNzYWdlcy5sZW5ndGgpXS52YWx1ZX1cclxuICAgICAgICAgIDwvcD4gKi99XHJcbiAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgIDxFeGFtcGxlIC8+XHJcblxyXG4gICAgICAgIHsvKiA8ZGl2IGNsYXNzTmFtZT17c3R5bGVzLmJ1dHRvbkNvbnRhaW5lcn0+XHJcbiAgICAgICAgICB7aGlzdG9yeVswXSA9PSBcIi9cIiA/IDxDb3B5TGlua0J1dHRvbiAvPiA6IFwiXCJ9XHJcblxyXG4gICAgICAgICAge2hpc3RvcnlbMF0gPT0gXCIvXCIgPyAoXHJcbiAgICAgICAgICAgIDxCdXR0b25cclxuICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBzZXREb3dubG9hZGVkT25jZShmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICBzZXREb3dubG9hZGluZyh0cnVlKTtcclxuICAgICAgICAgICAgICB9fVxyXG4gICAgICAgICAgICAgIHRleHQ9XCJEb3dubG9hZCBhcyBJbWFnZVwiXHJcbiAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICApIDogKFxyXG4gICAgICAgICAgICBcIlwiXHJcbiAgICAgICAgICApfVxyXG5cclxuICAgICAgICAgIDxCdXR0b25cclxuICAgICAgICAgICAgb25DbGljaz17KCkgPT4gcm91dGVyLnB1c2goXCIvXCIpfVxyXG4gICAgICAgICAgICB0ZXh0PVwiJmxhcnI7IENyZWF0ZSBhIHdpc2hcIlxyXG4gICAgICAgICAgLz5cclxuICAgICAgICA8L2Rpdj4gKi99XHJcbiAgICAgIDwvbWFpbj5cclxuICAgICAgPGF1ZGlvIHJlZj17YXVkaW9SZWZ9IGlkPVwicGxheWVyXCIgYXV0b1BsYXk+XHJcbiAgICAgICAgPHNvdXJjZSBzcmM9XCJtZWRpYS9sZWVtb25fdHJlZS5tcDNcIiAvPlxyXG4gICAgICA8L2F1ZGlvPlxyXG4gICAgPC9kaXY+XHJcbiAgKTtcclxufTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IFdpc2g7XHJcbiIsIiBjb25zdCBtZXNzYWdlcyA9IFtcclxuICAgIHtcInZhbHVlXCI6XCJNYXkgeW91IGJlIGdpZnRlZCB3aXRoIGxpZmXigJlzIGJpZ2dlc3Qgam95cyBhbmQgbmV2ZXItZW5kaW5nIGJsaXNzLiBBZnRlciBhbGwsIHlvdSB5b3Vyc2VsZiBhcmUgYSBnaWZ0IHRvIGVhcnRoLCBzbyB5b3UgZGVzZXJ2ZSB0aGUgYmVzdCFcIn0sXHJcbiAgICB7XCJ2YWx1ZVwiOlwiQ291bnQgbm90IHRoZSBjYW5kbGVz4oCmc2VlIHRoZSBsaWdodHMgdGhleSBnaXZlLiBDb3VudCBub3QgdGhlIHllYXJzLCBidXQgdGhlIGxpZmUgeW91IGxpdmUuIFdpc2hpbmcgeW91IGEgd29uZGVyZnVsIHRpbWUgYWhlYWQhXCIgfSwgXHJcbiAgICB7XCJ2YWx1ZVwiOlwiRm9yZ2V0IHRoZSBwYXN0OyBsb29rIGZvcndhcmQgdG8gdGhlIGZ1dHVyZSwgZm9yIHRoZSBiZXN0IHRoaW5ncyBhcmUgeWV0IHRvIGNvbWUhXCJ9LFxyXG4gICAge1widmFsdWVcIjpcIllvdSBhcmUgb25seSB5b3VuZyBvbmNlLCBidXQgeW91IGNhbiBiZSBpbW1hdHVyZSBmb3IgYSBsaWZldGltZSFcIn0sXHJcbiAgICB7XCJ2YWx1ZVwiOlwiQ2hlZXJzIHRvIHlvdSBmb3IgYW5vdGhlciB0cmlwIGFyb3VuZCB0aGUgc3VuIVwifSxcclxuICAgIHtcInZhbHVlXCI6XCJXaXNoaW5nIHlvdSBhIGJlYXV0aWZ1bCBkYXkgd2l0aCBnb29kIGhlYWx0aCBhbmQgaGFwcGluZXNzIGZvcmV2ZXIuXCJ9LFxyXG4gICAge1widmFsdWVcIjpcIlRvIHF1b3RlIFNoYWtlc3BlYXJlOiDigJhQYXJ0eSB0aGluZSBhc3Mgb2ZmIeKAmVwifSxcclxuICAgIHtcInZhbHVlXCI6XCJEb27igJl0IGZvcmdldCB0byBzbWlsZSBhd2t3YXJkbHkgYXMgZXZlcnlvbmUgc2luZ3MgeW91IGhhcHB5IGJpcnRoZGF5IHRvZGF5IVwifSxcclxuICAgIHtcInZhbHVlXCI6XCJCaXJ0aGRheXMgYXJlIG5hdHVyZeKAmXMgd2F5IG9mIHRlbGxpbmcgdXMgdG8gZWF0IG1vcmUgY2FrZSFcIn0sXHJcbiAgICB7XCJ2YWx1ZVwiOlwiT24geW91ciBiaXJ0aGRheSBJ4oCZbSBnb2luZyB0byBzaGFyZSB0aGUgc2VjcmV0IHRvIHN0YXlpbmcgeW91bmc6IGxpZSBhYm91dCB5b3VyIGFnZS5cIn0sXHJcbiAgICB7XCJ2YWx1ZVwiOlwiV29yayBoYXJkLiBQbGF5IGhhcmQuIEVhdCBsb3RzIG9mIGNha2UuIFRoYXTigJlzIGEgZ29vZCBtb3R0byBmb3IgeW91ciBiaXJ0aGRheSBhbmQgZm9yIGxpZmUuXCJ9LFxyXG4gICAge1widmFsdWVcIjpcIkhvcGUgeW91IGdldCBiaXJ0aGRheSBwcmVzZW50cyBpbiB0aGUgZG96ZW5zIVwifSxcclxuICAgIHtcInZhbHVlXCI6XCJNYXkgeW91IGhhdmUgYWxsIHRoZSBsb3ZlIHlvdXIgaGVhcnQgY2FuIGhvbGQsIGFsbCB0aGUgaGFwcGluZXNzIGEgZGF5IGNhbiBicmluZywgYW5kIGFsbCB0aGUgYmxlc3NpbmdzIGEgbGlmZSBjYW4gdW5mb2xkLlwifSxcclxuICAgIHtcInZhbHVlXCI6XCJCZSBoYXBweSEgVG9kYXkgaXMgdGhlIGRheSB5b3Ugd2VyZSBicm91Z2h0IGludG8gdGhpcyB3b3JsZCB0byBiZSBhIGJsZXNzaW5nIGFuZCBpbnNwaXJhdGlvbiB0byB0aGUgcGVvcGxlIGFyb3VuZCB5b3UhIFlvdSBhcmUgYSB3b25kZXJmdWwgcGVyc29uISBNYXkgeW91IGJlIGdpdmVuIG1vcmUgYmlydGhkYXlzIHRvIGZ1bGZpbGwgYWxsIG9mIHlvdXIgZHJlYW1zIVwifSxcclxuICAgIHtcInZhbHVlXCI6XCJGb3JnZXQgYWJvdXQgdGhlIHBhc3QsIHlvdSBjYW7igJl0IGNoYW5nZSBpdC4gRm9yZ2V0IGFib3V0IHRoZSBmdXR1cmUsIHlvdSBjYW7igJl0IHByZWRpY3QgaXQuIEFuZCBmb3JnZXQgYWJvdXQgdGhlIHByZXNlbnQsIEkgZGlkbuKAmXQgZ2V0IHlvdSBvbmUuIEhhcHB5IGJpcnRoZGF5IVwifSxcclxuICAgIHtcInZhbHVlXCI6XCJBcyB5b3UgZ2V0IG9sZGVyIHRocmVlIHRoaW5ncyBoYXBwZW4uIFRoZSBmaXJzdCBpcyB5b3VyIG1lbW9yeSBnb2VzLCBhbmQgSSBjYW7igJl0IHJlbWVtYmVyIHRoZSBvdGhlciB0d28uIEhhcHB5IGJpcnRoZGF5IVwifSxcclxuICAgIHtcInZhbHVlXCI6XCJXaGVuIHRoZSBsaXR0bGUga2lkcyBhc2sgaG93IG9sZCB5b3UgYXJlIGF0IHlvdXIgcGFydHksIHlvdSBzaG91bGQgZ28gYWhlYWQgYW5kIHRlbGwgdGhlbS4gV2hpbGUgdGhleeKAmXJlIGRpc3RyYWN0ZWQgdHJ5aW5nIHRvIGNvdW50IHRoYXQgaGlnaCwgeW91IGNhbiBzdGVhbCBhIGJpdGUgb2YgdGhlaXIgY2FrZSEgSGFwcHkgYmlydGhkYXkhXCJ9LFxyXG4gICAge1widmFsdWVcIjpcIlNlbmRpbmcgeW91IHNtaWxlcyBmb3IgZXZlcnkgbW9tZW50IG9mIHlvdXIgc3BlY2lhbCBkYXnigKZIYXZlIGEgd29uZGVyZnVsIHRpbWUgYW5kIGEgdmVyeSBoYXBweSBiaXJ0aGRheSFcIn0sXHJcbiAgICB7XCJ2YWx1ZVwiOlwiU2VuZGluZyB5b3UgYSBiaXJ0aGRheSB3aXNoIHdyYXBwZWQgd2l0aCBhbGwgbXkgbG92ZS4gSGF2ZSBhIHZlcnkgaGFwcHkgYmlydGhkYXkhXCJ9LFxyXG4gICAge1widmFsdWVcIjpcIkEgc2ltcGxlIGNlbGVicmF0aW9uLCBhIGdhdGhlcmluZyBvZiBmcmllbmRzOyBoZXJlIHdpc2hpbmcgeW91IGdyZWF0IGhhcHBpbmVzcyBhbmQgYSBqb3kgdGhhdCBuZXZlciBlbmRzLiBIYXBweSBiaXJ0aGRheSFcIn0sXHJcbiAgICB7XCJ2YWx1ZVwiOlwiSG9wZSB5b3VyIGJpcnRoZGF5IGlzIGp1c3QgbGlrZSB5b3XigKZ0b3RhbGx5IGZyZWFraW5nIGF3ZXNvbWUuXCJ9LFxyXG4gICAge1widmFsdWVcIjpcIkkgaG9wZSB0aGF0IHRvZGF5LCBhdCB5b3VyIHBhcnR5LCB5b3UgZGFuY2UgYW5kIG90aGVycyBzaW5nIGFzIHlvdSBjZWxlYnJhdGUgd2l0aCBqb3kgeW91ciBiZXN0IGJpcnRoZGF5LlwifSxcclxuICAgIHtcInZhbHVlXCI6XCJJdOKAmXMgYWx3YXlzIGEgdHJlYXQgdG8gd2lzaCBoYXBweSBiaXJ0aGRheSB0byBzb21lb25lIHNvIHN3ZWV0LlwifSxcclxuICAgIHtcInZhbHVlXCI6XCJIZXkgeW91ISBJdOKAmXMgeW91ciBiaXJ0aGRheSEgQ2FrZSEgQ2FuZGxlcyEgRHJpbmtzISBQcmVzZW50cyEgTW9yZSBmb29kISBDYW4geW91IHRlbGwgSeKAmW0gZXhjaXRlZD9cIn0sXHJcbiAgICB7XCJ2YWx1ZVwiOlwiSGFwcHkgYmlydGhkYXkgdG8gbXkgZm9yZXZlciB5b3VuZyBmcmllbmQhXCJ9LFxyXG4gICAge1widmFsdWVcIjpcIkJpcnRoZGF5cyBvbmx5IGNvbWUgb25jZSBhIHllYXIsIGFuZCB5b3VyIGZyaWVuZHNoaXAgb25seSBjb21lcyBvbmNlIGEgbGlmZXRpbWUuXCJ9LFxyXG4gICAge1widmFsdWVcIjpcIllvdSBkZXNlcnZlIGFsbCB0aGUgY2FrZSwgaGFwcGluZXNzLCBhbmQgbG92ZSB0b2RheS4gSGFwcHkgYmlydGhkYXkhXCJ9LFxyXG4gICAge1widmFsdWVcIjpcIlRoYW5rIHlvdSBmb3IgYmVpbmcgdGhlIGJyaWdodGVzdCBwZXJzb24gSSBrbm93LiBIb3BlIHRvIHNlZSB5b3Ugc2hpbmUgZm9yIHllYXJzIG9uISBIYXBweSBiaXJ0aGRheSFcIn0sXHJcblxyXG5dXHJcblxyXG5leHBvcnQgZGVmYXVsdCBtZXNzYWdlcztcclxuXHJcbiIsIi8vIEV4cG9ydHNcbm1vZHVsZS5leHBvcnRzID0ge1xuXHRcImNvbnRhaW5lclwiOiBcIk5hbWVfY29udGFpbmVyX19yc1lDelwiLFxuXHRcImNvbnRhaW5lckltZ1wiOiBcIk5hbWVfY29udGFpbmVySW1nX18yRzNoYlwiLFxuXHRcImFuaW1hdGVcIjogXCJOYW1lX2FuaW1hdGVfXzE1ekphXCIsXG5cdFwiem9vbVwiOiBcIk5hbWVfem9vbV9falJTZEFcIixcblx0XCJjYW52YXNcIjogXCJOYW1lX2NhbnZhc19fMTVOWS1cIixcblx0XCJtYWluXCI6IFwiTmFtZV9tYWluX18zejdnS1wiLFxuXHRcInNwYW5cIjogXCJOYW1lX3NwYW5fXzFqR3EtXCIsXG5cdFwiZGVzY1wiOiBcIk5hbWVfZGVzY19fMzJsdTFcIixcblx0XCJkZXNjSW1nXCI6IFwiTmFtZV9kZXNjSW1nX18zbWFNMFwiLFxuXHRcInRpdGxlXCI6IFwiTmFtZV90aXRsZV9fMjB5QlhcIixcblx0XCJ3YXZ5VGV4dFwiOiBcIk5hbWVfd2F2eVRleHRfXzNUdnRBXCIsXG5cdFwidGl0bGVJbWdcIjogXCJOYW1lX3RpdGxlSW1nX18xNGVaNVwiLFxuXHRcImJ1dHRvbkNvbnRhaW5lclwiOiBcIk5hbWVfYnV0dG9uQ29udGFpbmVyX192b3FBNlwiXG59O1xuIiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiY29uZmV0dGktanNcIik7IiwibW9kdWxlLmV4cG9ydHMgPSByZXF1aXJlKFwiZmlsZS1zYXZlclwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJodG1sLXRvLWltYWdlXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcIm5leHQvaGVhZFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJuZXh0L3JvdXRlclwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdFwiKTsiLCJtb2R1bGUuZXhwb3J0cyA9IHJlcXVpcmUoXCJyZWFjdC1yZXNwb25zaXZlLWNhcm91c2VsXCIpOyIsIm1vZHVsZS5leHBvcnRzID0gcmVxdWlyZShcInJlYWN0L2pzeC1kZXYtcnVudGltZVwiKTsiXSwibmFtZXMiOlsiUmVhY3QiLCJ1c2VTdGF0ZSIsInVzZUVmZmVjdCIsInVzZVRoZW1lIiwidGhlbWVzIiwiaWQiLCJuYW1lIiwiY29sb3IiLCJjdXJyZW50VGhlbWUiLCJzZXRDdXJyZW50VGhlbWUiLCJzZXRUaGVtZSIsInJlcXVpcmVkVGhlbWUiLCJmaW5kIiwiaXRlbSIsInJvb3QiLCJkb2N1bWVudCIsImRvY3VtZW50RWxlbWVudCIsInN0eWxlIiwic2V0UHJvcGVydHkiLCJ1c2VSZWYiLCJIZWFkIiwic3R5bGVzIiwidXNlUm91dGVyIiwiQ29uZmV0dGlHZW5lcmF0b3IiLCJtZXNzYWdlcyIsImh0bWxUb0ltYWdlIiwiRmlsZVNhdmVyIiwiQ2Fyb3VzZWwiLCJFeGFtcGxlIiwid2lkdGgiLCJtYXJnaW4iLCJoZWlnaHQiLCJiYWNrZ3JvdW5kIiwiZm9udFNpemUiLCJvcGFjaXR5IiwiV2lzaCIsImhpc3RvcnkiLCJyb3V0ZXIiLCJxdWVyeSIsImRvd25sb2FkaW5nIiwic2V0RG93bmxvYWRpbmciLCJkb3dubG9hZGVkT25jZSIsInNldERvd25sb2FkZWRPbmNlIiwiYXVkaW9SZWYiLCJjb25mZXR0aVNldHRpbmdzIiwidGFyZ2V0Iiwic3RhcnRfZnJvbV9lZGdlIiwiY29uZmV0dGkiLCJyZW5kZXIiLCJjdXJyZW50IiwicGxheSIsImRvd25sb2FkSW1hZ2UiLCJyYW5kb21OdW1iZXIiLCJtaW4iLCJtYXgiLCJNYXRoIiwiZmxvb3IiLCJyYW5kb20iLCJub2RlIiwiZ2V0RWxlbWVudEJ5SWQiLCJ0b1BuZyIsInRoZW4iLCJibG9iIiwic2F2ZUFzIiwidGl0bGUiLCJ3aXNoIiwiYmFzZV9sZXR0ZXJzIiwibmFtZV9sZXR0ZXJzIiwiaSIsImxlbmd0aCIsImxldHRlciIsImNoYXJBdCIsInB1c2giLCJzcGFuIiwidGl0bGVJbWciLCJtYXAiLCJjb250YWluZXJJbWciLCJpbWFnZSIsIm1haW4iLCJkZXNjSW1nIiwidmFsdWUiLCJjb250YWluZXIiLCJjYW52YXMiLCJhbmltYXRlIl0sInNvdXJjZVJvb3QiOiIifQ==