"use strict";
self["webpackHotUpdate_N_E"]("pages/[...name]",{

/***/ "./pages/[...name].js":
/*!****************************!*\
  !*** ./pages/[...name].js ***!
  \****************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/head */ "./node_modules/next/head.js");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../styles/Name.module.css */ "./styles/Name.module.css");
/* harmony import */ var _styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/router */ "./node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var confetti_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! confetti-js */ "./node_modules/confetti-js/dist/index.es.js");
/* harmony import */ var _utils_birthdayWishes_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../utils/birthdayWishes.js */ "./utils/birthdayWishes.js");
/* harmony import */ var _hooks_useTheme__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../hooks/useTheme */ "./hooks/useTheme.js");
/* harmony import */ var html_to_image__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! html-to-image */ "./node_modules/html-to-image/es/index.js");
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! file-saver */ "./node_modules/file-saver/dist/FileSaver.min.js");
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(file_saver__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var react_responsive_carousel_lib_styles_carousel_min_css__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react-responsive-carousel/lib/styles/carousel.min.css */ "./node_modules/react-responsive-carousel/lib/styles/carousel.min.css");
/* harmony import */ var react_responsive_carousel_lib_styles_carousel_min_css__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react_responsive_carousel_lib_styles_carousel_min_css__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var react_responsive_carousel__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-responsive-carousel */ "./node_modules/react-responsive-carousel/lib/js/index.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__);
/* module decorator */ module = __webpack_require__.hmd(module);
var _jsxFileName = "D:\\SangLM3\\APP\\fx_birthday_xuanqn\\pages\\[...name].js",
    _this = undefined,
    _s = $RefreshSig$(),
    _s2 = $RefreshSig$();















var Example = function Example() {
  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
    children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
      style: {
        width: "800px",
        margin: "auto"
      },
      children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)(react_responsive_carousel__WEBPACK_IMPORTED_MODULE_9__.Carousel, {
        autoPlay: true,
        infiniteLoop: false,
        interval: 5000,
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("img", {
            src: "/h2.jpg",
            alt: "image1"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 19,
            columnNumber: 11
          }, _this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 18,
          columnNumber: 9
        }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("img", {
            src: "/h3.jpg",
            alt: "image2"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 23,
            columnNumber: 11
          }, _this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 22,
          columnNumber: 9
        }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("img", {
            src: "/h4.jpg",
            alt: "image3"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 28,
            columnNumber: 11
          }, _this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 27,
          columnNumber: 9
        }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("img", {
            src: "/h5.jpg",
            alt: "image4"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 32,
            columnNumber: 11
          }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("p", {
            style: {
              background: "#7FB77E",
              fontSize: "26px",
              opacity: "1"
            },
            className: "legend",
            children: "Lu\xF4n la\u0300 ng\u01B0\u01A1\u0300i anh ca\u0309 cu\u0309a chu\u0301ng em"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 33,
            columnNumber: 11
          }, _this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 31,
          columnNumber: 9
        }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("img", {
            src: "/h6.jpg",
            alt: "image5"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 41,
            columnNumber: 11
          }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("p", {
            style: {
              background: "#7FB77E",
              fontSize: "26px",
              opacity: "1"
            },
            className: "legend",
            children: "3 ph\xE2\u0300n y\xEAu th\u01B0\u01A1ng 7 ph\xE2\u0300n nu\xF4ng chi\xEA\u0300u chu\u0301ng em"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 42,
            columnNumber: 11
          }, _this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 40,
          columnNumber: 9
        }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("img", {
            src: "/h7.jpg",
            alt: "image6"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 50,
            columnNumber: 11
          }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("p", {
            style: {
              background: "#7FB77E",
              fontSize: "26px",
              opacity: "1"
            },
            className: "legend",
            children: "Thi thoa\u0309ng di\u0301 deadline chu\u0301ng em b\xE2\u0301t k\xEA\u0309 nga\u0300y \u0111\xEAm"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 51,
            columnNumber: 11
          }, _this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 49,
          columnNumber: 9
        }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("img", {
            src: "/h8.jpg",
            alt: "image7"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 59,
            columnNumber: 11
          }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("p", {
            style: {
              background: "#7FB77E",
              fontSize: "26px",
              opacity: "1"
            },
            className: "legend",
            children: ["Va\u0300 cu\xF4\u0301i cu\u0300ng la\u0300 \u0110a\u0303 y\xEAu th\u01B0\u01A1ng, ta\u0323o \u0111i\xEA\u0300u ki\xEA\u0323n cho chu\u0301ng em tr\u01B0\u01A1\u0309ng tha\u0300nh h\u01A1n", " "]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 60,
            columnNumber: 11
          }, _this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 58,
          columnNumber: 9
        }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("img", {
            src: "/h9.jpg",
            alt: "image8"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 69,
            columnNumber: 11
          }, _this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 68,
          columnNumber: 9
        }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("img", {
            src: "/h10.jpg",
            alt: "image9"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 73,
            columnNumber: 11
          }, _this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 72,
          columnNumber: 9
        }, _this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 17,
        columnNumber: 7
      }, _this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 16,
      columnNumber: 5
    }, _this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 14,
    columnNumber: 3
  }, _this);
};

_c = Example;

var wish_time_out = function wish_time_out() {
  _s();

  (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(function () {
    var timer = setTimeout(function () {
      setCount('Timeout called!');
    }, 1000);
    return function () {
      return clearTimeout(timer);
    };
  }, []);
  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("canvas", {
    className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().canvas),
    id: "canvas"
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 91,
    columnNumber: 5
  }, _this);
};

_s(wish_time_out, "OD7bBpZva5O2jO+Puf00hKivP7c=");

var Wish = function Wish(_ref) {
  _s2();

  var history = _ref.history;
  var router = (0,next_router__WEBPACK_IMPORTED_MODULE_2__.useRouter)();
  var name = router.query.name; // gets both name & color id in form of array [name,colorId]

  var color = name ? name[1] : 0; //extracting colorId from name

  var _useState = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)(false),
      downloading = _useState[0],
      setDownloading = _useState[1];

  var _useState2 = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)(false),
      downloadedOnce = _useState2[0],
      setDownloadedOnce = _useState2[1];

  var audioRef = (0,react__WEBPACK_IMPORTED_MODULE_0__.useRef)();

  var _useTheme = (0,_hooks_useTheme__WEBPACK_IMPORTED_MODULE_5__.default)(),
      setTheme = _useTheme.setTheme;

  (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(function () {
    // Theme Change
    setTheme(color);

    if (downloading === false) {
      // Confetti
      var confettiSettings = {
        target: "canvas",
        start_from_edge: true
      };
      var confetti = new confetti_js__WEBPACK_IMPORTED_MODULE_3__.default(confettiSettings);
      confetti.render();
    }

    audioRef.current.play();
  }, [color, downloading]);
  (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(function () {
    if (downloading === true && downloadedOnce === false) {
      downloadImage();
    }
  }, [downloading, downloadedOnce]); // function for randomly picking the message from messages array

  var randomNumber = function randomNumber(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
  };

  var downloadImage = function downloadImage() {
    if (downloadedOnce === true) return;
    var node = document.getElementById("image");

    if (node) {
      setDownloadedOnce(true);
      html_to_image__WEBPACK_IMPORTED_MODULE_6__.toPng(node).then(function (blob) {
        file_saver__WEBPACK_IMPORTED_MODULE_7___default().saveAs(blob, "birthday-wish.png");
        setDownloading(false);
      });
    }
  };

  var title = function title(name) {
    var wish = "Happy Birthday " + name + "!";
    var base_letters = [];
    var name_letters = [];

    for (var i = 0; i < wish.length; i++) {
      if (i < 15) {
        var letter = wish.charAt(i);
        base_letters.push( /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("span", {
          style: {
            "--i": i + 1
          },
          children: letter
        }, i, false, {
          fileName: _jsxFileName,
          lineNumber: 157,
          columnNumber: 11
        }, _this));
      } else {
        var _letter = wish.charAt(i);

        name_letters.push( /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("span", {
          style: {
            "--i": i + 1
          },
          className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().span),
          children: _letter
        }, i, false, {
          fileName: _jsxFileName,
          lineNumber: 164,
          columnNumber: 11
        }, _this));
      }
    }

    return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.Fragment, {
      children: downloading ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("h1", {
        className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().titleImg),
        style: {
          "--wish-length": wish.length
        },
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: base_letters.map(function (letter) {
            return letter;
          })
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 178,
          columnNumber: 13
        }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: name_letters.map(function (letter) {
            return letter;
          })
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 179,
          columnNumber: 13
        }, _this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 174,
        columnNumber: 11
      }, _this) : /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("h1", {
        className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().title),
        style: {
          "--wish-length": wish.length
        },
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: base_letters.map(function (letter) {
            return letter;
          })
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 183,
          columnNumber: 13
        }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: name_letters.map(function (letter) {
            return letter;
          })
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 184,
          columnNumber: 13
        }, _this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 182,
        columnNumber: 11
      }, _this)
    }, void 0, false);
  };

  if (downloading) {
    return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
      className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().containerImg),
      id: "image",
      onClick: downloadImage,
      children: [downloadImage(), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("main", {
        className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().image),
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
            className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().main),
            children: title(name && name[0])
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 197,
            columnNumber: 13
          }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
            style: {
              height: 40
            }
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 199,
            columnNumber: 13
          }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("p", {
            className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().descImg),
            children: _utils_birthdayWishes_js__WEBPACK_IMPORTED_MODULE_4__.default[randomNumber(0, _utils_birthdayWishes_js__WEBPACK_IMPORTED_MODULE_4__.default.length)].value
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 201,
            columnNumber: 13
          }, _this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 196,
          columnNumber: 11
        }, _this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 195,
        columnNumber: 9
      }, _this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 193,
      columnNumber: 7
    }, _this);
  }

  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
    className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().container),
    children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)((next_head__WEBPACK_IMPORTED_MODULE_1___default()), {
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("title", {
        children: ["Happy Birthday ", name && name[0]]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 213,
        columnNumber: 9
      }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("meta", {
        name: "description",
        content: "Generated by create next app"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 214,
        columnNumber: 9
      }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("link", {
        rel: "icon",
        href: "/favicon.ico"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 215,
        columnNumber: 9
      }, _this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 212,
      columnNumber: 7
    }, _this), wish_time_out, /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("main", {
      className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().animate),
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().main),
          children: title(name && name[0])
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 223,
          columnNumber: 11
        }, _this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 222,
        columnNumber: 9
      }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)(Example, {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 229,
        columnNumber: 9
      }, _this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 221,
      columnNumber: 7
    }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("audio", {
      ref: audioRef,
      id: "player",
      autoPlay: true,
      children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("source", {
        src: "media/leemon_tree.mp3"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 253,
        columnNumber: 9
      }, _this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 252,
      columnNumber: 7
    }, _this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 211,
    columnNumber: 5
  }, _this);
};

_s2(Wish, "KC1aZNv21wLD2inhQfaHb0yrDYI=", false, function () {
  return [next_router__WEBPACK_IMPORTED_MODULE_2__.useRouter, _hooks_useTheme__WEBPACK_IMPORTED_MODULE_5__.default];
});

_c2 = Wish;
/* harmony default export */ __webpack_exports__["default"] = (Wish);

var _c, _c2;

$RefreshReg$(_c, "Example");
$RefreshReg$(_c2, "Wish");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.id);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }


/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvWy4uLm5hbWVdLjM0YjY3YmMwZWU0MWUwYzkwYzQ5LmhvdC11cGRhdGUuanMiLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FBRUEsSUFBTWEsT0FBTyxHQUFHLFNBQVZBLE9BQVU7QUFBQSxzQkFDZDtBQUFBLDJCQUVFO0FBQUssV0FBSyxFQUFFO0FBQUVDLFFBQUFBLEtBQUssRUFBRSxPQUFUO0FBQWtCQyxRQUFBQSxNQUFNLEVBQUU7QUFBMUIsT0FBWjtBQUFBLDZCQUNFLCtEQUFDLCtEQUFEO0FBQVUsZ0JBQVEsRUFBRSxJQUFwQjtBQUEwQixvQkFBWSxFQUFFLEtBQXhDO0FBQStDLGdCQUFRLEVBQUUsSUFBekQ7QUFBQSxnQ0FDRTtBQUFBLGlDQUNFO0FBQUssZUFBRyxFQUFDLFNBQVQ7QUFBbUIsZUFBRyxFQUFDO0FBQXZCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQURGLGVBS0U7QUFBQSxpQ0FDRTtBQUFLLGVBQUcsRUFBQyxTQUFUO0FBQW1CLGVBQUcsRUFBQztBQUF2QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFMRixlQVVFO0FBQUEsaUNBQ0U7QUFBSyxlQUFHLEVBQUMsU0FBVDtBQUFtQixlQUFHLEVBQUM7QUFBdkI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBVkYsZUFjRTtBQUFBLGtDQUNFO0FBQUssZUFBRyxFQUFDLFNBQVQ7QUFBbUIsZUFBRyxFQUFDO0FBQXZCO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBREYsZUFFRTtBQUNFLGlCQUFLLEVBQUU7QUFBRUMsY0FBQUEsVUFBVSxFQUFFLFNBQWQ7QUFBeUJDLGNBQUFBLFFBQVEsRUFBRSxNQUFuQztBQUEyQ0MsY0FBQUEsT0FBTyxFQUFFO0FBQXBELGFBRFQ7QUFFRSxxQkFBUyxFQUFDLFFBRlo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQWRGLGVBdUJFO0FBQUEsa0NBQ0U7QUFBSyxlQUFHLEVBQUMsU0FBVDtBQUFtQixlQUFHLEVBQUM7QUFBdkI7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFERixlQUVFO0FBQ0UsaUJBQUssRUFBRTtBQUFFRixjQUFBQSxVQUFVLEVBQUUsU0FBZDtBQUF5QkMsY0FBQUEsUUFBUSxFQUFFLE1BQW5DO0FBQTJDQyxjQUFBQSxPQUFPLEVBQUU7QUFBcEQsYUFEVDtBQUVFLHFCQUFTLEVBQUMsUUFGWjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBdkJGLGVBZ0NFO0FBQUEsa0NBQ0U7QUFBSyxlQUFHLEVBQUMsU0FBVDtBQUFtQixlQUFHLEVBQUM7QUFBdkI7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFERixlQUVFO0FBQ0UsaUJBQUssRUFBRTtBQUFFRixjQUFBQSxVQUFVLEVBQUUsU0FBZDtBQUF5QkMsY0FBQUEsUUFBUSxFQUFFLE1BQW5DO0FBQTJDQyxjQUFBQSxPQUFPLEVBQUU7QUFBcEQsYUFEVDtBQUVFLHFCQUFTLEVBQUMsUUFGWjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBaENGLGVBeUNFO0FBQUEsa0NBQ0U7QUFBSyxlQUFHLEVBQUMsU0FBVDtBQUFtQixlQUFHLEVBQUM7QUFBdkI7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFERixlQUVFO0FBQ0UsaUJBQUssRUFBRTtBQUFFRixjQUFBQSxVQUFVLEVBQUUsU0FBZDtBQUF5QkMsY0FBQUEsUUFBUSxFQUFFLE1BQW5DO0FBQTJDQyxjQUFBQSxPQUFPLEVBQUU7QUFBcEQsYUFEVDtBQUVFLHFCQUFTLEVBQUMsUUFGWjtBQUFBLHNOQUthLEdBTGI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkF6Q0YsZUFtREU7QUFBQSxpQ0FDRTtBQUFLLGVBQUcsRUFBQyxTQUFUO0FBQW1CLGVBQUcsRUFBQztBQUF2QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFuREYsZUF1REU7QUFBQSxpQ0FDRTtBQUFLLGVBQUcsRUFBQyxVQUFUO0FBQW9CLGVBQUcsRUFBQztBQUF4QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkF2REY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUEsV0FEYztBQUFBLENBQWhCOztLQUFNTDs7QUFvRU4sSUFBTU0sYUFBYSxHQUFHLFNBQWhCQSxhQUFnQixHQUFNO0FBQUE7O0FBRTFCakIsRUFBQUEsZ0RBQVMsQ0FBQyxZQUFNO0FBQ2QsUUFBTWtCLEtBQUssR0FBR0MsVUFBVSxDQUFDLFlBQU07QUFDN0JDLE1BQUFBLFFBQVEsQ0FBQyxpQkFBRCxDQUFSO0FBQ0QsS0FGdUIsRUFFckIsSUFGcUIsQ0FBeEI7QUFHQSxXQUFPO0FBQUEsYUFBTUMsWUFBWSxDQUFDSCxLQUFELENBQWxCO0FBQUEsS0FBUDtBQUNELEdBTFEsRUFLTixFQUxNLENBQVQ7QUFPQSxzQkFDRTtBQUFRLGFBQVMsRUFBRWYsd0VBQW5CO0FBQWtDLE1BQUUsRUFBQztBQUFyQztBQUFBO0FBQUE7QUFBQTtBQUFBLFdBREY7QUFHRCxDQVpEOztHQUFNYzs7QUFjTixJQUFNTSxJQUFJLEdBQUcsU0FBUEEsSUFBTyxPQUFpQjtBQUFBOztBQUFBLE1BQWRDLE9BQWMsUUFBZEEsT0FBYztBQUM1QixNQUFNQyxNQUFNLEdBQUdyQixzREFBUyxFQUF4QjtBQUNBLE1BQVFzQixJQUFSLEdBQWlCRCxNQUFNLENBQUNFLEtBQXhCLENBQVFELElBQVIsQ0FGNEIsQ0FFRzs7QUFDL0IsTUFBTUUsS0FBSyxHQUFHRixJQUFJLEdBQUdBLElBQUksQ0FBQyxDQUFELENBQVAsR0FBYSxDQUEvQixDQUg0QixDQUdNOztBQUNsQyxrQkFBc0MzQiwrQ0FBUSxDQUFDLEtBQUQsQ0FBOUM7QUFBQSxNQUFPOEIsV0FBUDtBQUFBLE1BQW9CQyxjQUFwQjs7QUFDQSxtQkFBNEMvQiwrQ0FBUSxDQUFDLEtBQUQsQ0FBcEQ7QUFBQSxNQUFPZ0MsY0FBUDtBQUFBLE1BQXVCQyxpQkFBdkI7O0FBQ0EsTUFBTUMsUUFBUSxHQUFHaEMsNkNBQU0sRUFBdkI7O0FBRUEsa0JBQXFCTSx3REFBUSxFQUE3QjtBQUFBLE1BQVEyQixRQUFSLGFBQVFBLFFBQVI7O0FBRUFsQyxFQUFBQSxnREFBUyxDQUFDLFlBQU07QUFDZDtBQUNBa0MsSUFBQUEsUUFBUSxDQUFDTixLQUFELENBQVI7O0FBRUEsUUFBSUMsV0FBVyxLQUFLLEtBQXBCLEVBQTJCO0FBQ3pCO0FBQ0EsVUFBTU0sZ0JBQWdCLEdBQUc7QUFDdkJDLFFBQUFBLE1BQU0sRUFBRSxRQURlO0FBRXZCQyxRQUFBQSxlQUFlLEVBQUU7QUFGTSxPQUF6QjtBQUlBLFVBQU1DLFFBQVEsR0FBRyxJQUFJakMsZ0RBQUosQ0FBc0I4QixnQkFBdEIsQ0FBakI7QUFDQUcsTUFBQUEsUUFBUSxDQUFDQyxNQUFUO0FBQ0Q7O0FBRUROLElBQUFBLFFBQVEsQ0FBQ08sT0FBVCxDQUFpQkMsSUFBakI7QUFDRCxHQWZRLEVBZU4sQ0FBQ2IsS0FBRCxFQUFRQyxXQUFSLENBZk0sQ0FBVDtBQWlCQTdCLEVBQUFBLGdEQUFTLENBQUMsWUFBTTtBQUNkLFFBQUk2QixXQUFXLEtBQUssSUFBaEIsSUFBd0JFLGNBQWMsS0FBSyxLQUEvQyxFQUFzRDtBQUNwRFcsTUFBQUEsYUFBYTtBQUNkO0FBQ0YsR0FKUSxFQUlOLENBQUNiLFdBQUQsRUFBY0UsY0FBZCxDQUpNLENBQVQsQ0EzQjRCLENBaUM1Qjs7QUFDQSxNQUFNWSxZQUFZLEdBQUcsU0FBZkEsWUFBZSxDQUFDQyxHQUFELEVBQU1DLEdBQU4sRUFBYztBQUNqQyxXQUFPQyxJQUFJLENBQUNDLEtBQUwsQ0FBV0QsSUFBSSxDQUFDRSxNQUFMLE1BQWlCSCxHQUFHLEdBQUdELEdBQXZCLENBQVgsSUFBMENBLEdBQWpEO0FBQ0QsR0FGRDs7QUFJQSxNQUFNRixhQUFhLEdBQUcsU0FBaEJBLGFBQWdCLEdBQU07QUFDMUIsUUFBSVgsY0FBYyxLQUFLLElBQXZCLEVBQTZCO0FBRTdCLFFBQU1rQixJQUFJLEdBQUdDLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixPQUF4QixDQUFiOztBQUVBLFFBQUlGLElBQUosRUFBVTtBQUNSakIsTUFBQUEsaUJBQWlCLENBQUMsSUFBRCxDQUFqQjtBQUVBeEIsTUFBQUEsZ0RBQUEsQ0FBa0J5QyxJQUFsQixFQUF3QkksSUFBeEIsQ0FBNkIsVUFBQ0MsSUFBRCxFQUFVO0FBQ3JDN0MsUUFBQUEsd0RBQUEsQ0FBaUI2QyxJQUFqQixFQUF1QixtQkFBdkI7QUFDQXhCLFFBQUFBLGNBQWMsQ0FBQyxLQUFELENBQWQ7QUFDRCxPQUhEO0FBSUQ7QUFDRixHQWJEOztBQWVBLE1BQU0wQixLQUFLLEdBQUcsU0FBUkEsS0FBUSxDQUFDOUIsSUFBRCxFQUFVO0FBQ3RCLFFBQU0rQixJQUFJLEdBQUcsb0JBQW9CL0IsSUFBcEIsR0FBMkIsR0FBeEM7QUFDQSxRQUFNZ0MsWUFBWSxHQUFHLEVBQXJCO0FBQ0EsUUFBTUMsWUFBWSxHQUFHLEVBQXJCOztBQUVBLFNBQUssSUFBSUMsQ0FBQyxHQUFHLENBQWIsRUFBZ0JBLENBQUMsR0FBR0gsSUFBSSxDQUFDSSxNQUF6QixFQUFpQ0QsQ0FBQyxFQUFsQyxFQUFzQztBQUNwQyxVQUFJQSxDQUFDLEdBQUcsRUFBUixFQUFZO0FBQ1YsWUFBTUUsTUFBTSxHQUFHTCxJQUFJLENBQUNNLE1BQUwsQ0FBWUgsQ0FBWixDQUFmO0FBQ0FGLFFBQUFBLFlBQVksQ0FBQ00sSUFBYixlQUNFO0FBQWMsZUFBSyxFQUFFO0FBQUUsbUJBQU9KLENBQUMsR0FBRztBQUFiLFdBQXJCO0FBQUEsb0JBQ0dFO0FBREgsV0FBV0YsQ0FBWDtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQURGO0FBS0QsT0FQRCxNQU9PO0FBQ0wsWUFBTUUsT0FBTSxHQUFHTCxJQUFJLENBQUNNLE1BQUwsQ0FBWUgsQ0FBWixDQUFmOztBQUNBRCxRQUFBQSxZQUFZLENBQUNLLElBQWIsZUFDRTtBQUFjLGVBQUssRUFBRTtBQUFFLG1CQUFPSixDQUFDLEdBQUc7QUFBYixXQUFyQjtBQUF1QyxtQkFBUyxFQUFFekQsc0VBQWxEO0FBQUEsb0JBQ0cyRDtBQURILFdBQVdGLENBQVg7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFERjtBQUtEO0FBQ0Y7O0FBRUQsd0JBQ0U7QUFBQSxnQkFDRy9CLFdBQVcsZ0JBQ1Y7QUFDRSxpQkFBUyxFQUFFMUIsMEVBRGI7QUFFRSxhQUFLLEVBQUU7QUFBRSwyQkFBaUJzRCxJQUFJLENBQUNJO0FBQXhCLFNBRlQ7QUFBQSxnQ0FJRTtBQUFBLG9CQUFNSCxZQUFZLENBQUNTLEdBQWIsQ0FBaUIsVUFBQ0wsTUFBRDtBQUFBLG1CQUFZQSxNQUFaO0FBQUEsV0FBakI7QUFBTjtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUpGLGVBS0U7QUFBQSxvQkFBTUgsWUFBWSxDQUFDUSxHQUFiLENBQWlCLFVBQUNMLE1BQUQ7QUFBQSxtQkFBWUEsTUFBWjtBQUFBLFdBQWpCO0FBQU47QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFMRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUFEVSxnQkFTVjtBQUFJLGlCQUFTLEVBQUUzRCx1RUFBZjtBQUE2QixhQUFLLEVBQUU7QUFBRSwyQkFBaUJzRCxJQUFJLENBQUNJO0FBQXhCLFNBQXBDO0FBQUEsZ0NBQ0U7QUFBQSxvQkFBTUgsWUFBWSxDQUFDUyxHQUFiLENBQWlCLFVBQUNMLE1BQUQ7QUFBQSxtQkFBWUEsTUFBWjtBQUFBLFdBQWpCO0FBQU47QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFERixlQUVFO0FBQUEsb0JBQU1ILFlBQVksQ0FBQ1EsR0FBYixDQUFpQixVQUFDTCxNQUFEO0FBQUEsbUJBQVlBLE1BQVo7QUFBQSxXQUFqQjtBQUFOO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBVkoscUJBREY7QUFrQkQsR0F6Q0Q7O0FBMkNBLE1BQUlqQyxXQUFKLEVBQWlCO0FBQ2Ysd0JBQ0U7QUFBSyxlQUFTLEVBQUUxQiw4RUFBaEI7QUFBcUMsUUFBRSxFQUFDLE9BQXhDO0FBQWdELGFBQU8sRUFBRXVDLGFBQXpEO0FBQUEsaUJBQ0dBLGFBQWEsRUFEaEIsZUFFRTtBQUFNLGlCQUFTLEVBQUV2Qyx1RUFBakI7QUFBQSwrQkFDRTtBQUFBLGtDQUNFO0FBQUsscUJBQVMsRUFBRUEsc0VBQWhCO0FBQUEsc0JBQThCcUQsS0FBSyxDQUFDOUIsSUFBSSxJQUFJQSxJQUFJLENBQUMsQ0FBRCxDQUFiO0FBQW5DO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBREYsZUFHRTtBQUFLLGlCQUFLLEVBQUU7QUFBRTZDLGNBQUFBLE1BQU0sRUFBRTtBQUFWO0FBQVo7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFIRixlQUtFO0FBQUcscUJBQVMsRUFBRXBFLHlFQUFkO0FBQUEsc0JBQ0dHLDZEQUFRLENBQUNxQyxZQUFZLENBQUMsQ0FBRCxFQUFJckMsb0VBQUosQ0FBYixDQUFSLENBQTJDbUU7QUFEOUM7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFMRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGFBREY7QUFnQkQ7O0FBRUQsc0JBQ0U7QUFBSyxhQUFTLEVBQUV0RSwyRUFBaEI7QUFBQSw0QkFDRSwrREFBQyxrREFBRDtBQUFBLDhCQUNFO0FBQUEsc0NBQXVCdUIsSUFBSSxJQUFJQSxJQUFJLENBQUMsQ0FBRCxDQUFuQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUFERixlQUVFO0FBQU0sWUFBSSxFQUFDLGFBQVg7QUFBeUIsZUFBTyxFQUFDO0FBQWpDO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUFGRixlQUdFO0FBQU0sV0FBRyxFQUFDLE1BQVY7QUFBaUIsWUFBSSxFQUFDO0FBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUFIRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsYUFERixFQU9HVCxhQVBILGVBVUU7QUFBTSxlQUFTLEVBQUVkLHlFQUFqQjtBQUFBLDhCQUNFO0FBQUEsK0JBQ0U7QUFBSyxtQkFBUyxFQUFFQSxzRUFBaEI7QUFBQSxvQkFBOEJxRCxLQUFLLENBQUM5QixJQUFJLElBQUlBLElBQUksQ0FBQyxDQUFELENBQWI7QUFBbkM7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUFERixlQVFFLCtEQUFDLE9BQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQVJGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxhQVZGLGVBeUNFO0FBQU8sU0FBRyxFQUFFTyxRQUFaO0FBQXNCLFFBQUUsRUFBQyxRQUF6QjtBQUFrQyxjQUFRLE1BQTFDO0FBQUEsNkJBQ0U7QUFBUSxXQUFHLEVBQUM7QUFBWjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxhQXpDRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsV0FERjtBQStDRCxDQWxLRDs7SUFBTVY7VUFDV25CLG9EQU9NRzs7O01BUmpCZ0I7QUFvS04sK0RBQWVBLElBQWYiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvWy4uLm5hbWVdLmpzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCwgeyB1c2VTdGF0ZSwgdXNlRWZmZWN0LCB1c2VSZWYgfSBmcm9tIFwicmVhY3RcIjtcclxuaW1wb3J0IEhlYWQgZnJvbSBcIm5leHQvaGVhZFwiO1xyXG5pbXBvcnQgc3R5bGVzIGZyb20gXCIuLi9zdHlsZXMvTmFtZS5tb2R1bGUuY3NzXCI7XHJcbmltcG9ydCB7IHVzZVJvdXRlciB9IGZyb20gXCJuZXh0L3JvdXRlclwiO1xyXG5pbXBvcnQgQ29uZmV0dGlHZW5lcmF0b3IgZnJvbSBcImNvbmZldHRpLWpzXCI7XHJcbmltcG9ydCBtZXNzYWdlcyBmcm9tIFwiLi4vdXRpbHMvYmlydGhkYXlXaXNoZXMuanNcIjtcclxuaW1wb3J0IHVzZVRoZW1lIGZyb20gXCIuLi9ob29rcy91c2VUaGVtZVwiO1xyXG5pbXBvcnQgKiBhcyBodG1sVG9JbWFnZSBmcm9tIFwiaHRtbC10by1pbWFnZVwiO1xyXG5pbXBvcnQgRmlsZVNhdmVyIGZyb20gXCJmaWxlLXNhdmVyXCI7XHJcbmltcG9ydCBcInJlYWN0LXJlc3BvbnNpdmUtY2Fyb3VzZWwvbGliL3N0eWxlcy9jYXJvdXNlbC5taW4uY3NzXCI7XHJcbmltcG9ydCB7IENhcm91c2VsIH0gZnJvbSBcInJlYWN0LXJlc3BvbnNpdmUtY2Fyb3VzZWxcIjtcclxuXHJcbmNvbnN0IEV4YW1wbGUgPSAoKSA9PiAoXHJcbiAgPGRpdj5cclxuICAgIHsvKiA8aDI+Q2jDumMgYW5oIG5nw6B5IHNpbmggbmjhuq10IHRo4bqtdCB2dWkgduG6uywgbHXDtG4gbeG6oW5oIGtob+G6uywgZ+G6t3Qgbmhp4buBdSB0aMOgbmggY8O0bmcgaMahbiBu4buvYSBhbmggbmjDqSE8L2gyPiAqL31cclxuICAgIDxkaXYgc3R5bGU9e3sgd2lkdGg6IFwiODAwcHhcIiwgbWFyZ2luOiBcImF1dG9cIiB9fT5cclxuICAgICAgPENhcm91c2VsIGF1dG9QbGF5PXt0cnVlfSBpbmZpbml0ZUxvb3A9e2ZhbHNlfSBpbnRlcnZhbD17NTAwMH0+XHJcbiAgICAgICAgPGRpdj5cclxuICAgICAgICAgIDxpbWcgc3JjPVwiL2gyLmpwZ1wiIGFsdD1cImltYWdlMVwiIC8+XHJcbiAgICAgICAgICB7LyogPHAgc3R5bGU9e3tiYWNrZ3JvdW5kOlwiIzdGQjc3RVwiLCBmb250U2l6ZTpcIjI2cHhcIiwgb3BhY2l0eTpcIjFcIn19IGNsYXNzTmFtZT1cImxlZ2VuZFwiPjwvcD4gKi99XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPGRpdj5cclxuICAgICAgICAgIDxpbWcgc3JjPVwiL2gzLmpwZ1wiIGFsdD1cImltYWdlMlwiIC8+XHJcbiAgICAgICAgICB7LyogPHAgc3R5bGU9e3tiYWNrZ3JvdW5kOlwiIzdGQjc3RVwiLCBmb250U2l6ZTpcIjI2cHhcIiwgb3BhY2l0eTpcIjFcIn19IGNsYXNzTmFtZT1cImxlZ2VuZFwiPkPhuqNtIMahbiBhbmggdGjhuq10IG5oaeG7gXUgdsOsLi4uPC9wPiAqL31cclxuICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgPGRpdj5cclxuICAgICAgICAgIDxpbWcgc3JjPVwiL2g0LmpwZ1wiIGFsdD1cImltYWdlM1wiIC8+XHJcbiAgICAgICAgICB7LyogPHAgc3R5bGU9e3tiYWNrZ3JvdW5kOlwiIzdGQjc3RVwiLCBmb250U2l6ZTpcIjI2cHhcIiwgb3BhY2l0eTpcIjFcIn19IGNsYXNzTmFtZT1cImxlZ2VuZFwiPkPhuqNtIMahbiBhbmggdGjhuq10IG5oaeG7gXUgdsOsLi4uPC9wPiAqL31cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgPGltZyBzcmM9XCIvaDUuanBnXCIgYWx0PVwiaW1hZ2U0XCIgLz5cclxuICAgICAgICAgIDxwXHJcbiAgICAgICAgICAgIHN0eWxlPXt7IGJhY2tncm91bmQ6IFwiIzdGQjc3RVwiLCBmb250U2l6ZTogXCIyNnB4XCIsIG9wYWNpdHk6IFwiMVwiIH19XHJcbiAgICAgICAgICAgIGNsYXNzTmFtZT1cImxlZ2VuZFwiXHJcbiAgICAgICAgICA+XHJcbiAgICAgICAgICAgIEx1w7RuIGxhzIAgbmfGsMahzIBpIGFuaCBjYcyJIGN1zIlhIGNodcyBbmcgZW1cclxuICAgICAgICAgIDwvcD5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgPGltZyBzcmM9XCIvaDYuanBnXCIgYWx0PVwiaW1hZ2U1XCIgLz5cclxuICAgICAgICAgIDxwXHJcbiAgICAgICAgICAgIHN0eWxlPXt7IGJhY2tncm91bmQ6IFwiIzdGQjc3RVwiLCBmb250U2l6ZTogXCIyNnB4XCIsIG9wYWNpdHk6IFwiMVwiIH19XHJcbiAgICAgICAgICAgIGNsYXNzTmFtZT1cImxlZ2VuZFwiXHJcbiAgICAgICAgICA+XHJcbiAgICAgICAgICAgIDMgcGjDosyAbiB5w6p1IHRoxrDGoW5nIDcgcGjDosyAbiBudcO0bmcgY2hpw6rMgHUgY2h1zIFuZyBlbVxyXG4gICAgICAgICAgPC9wPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDxkaXY+XHJcbiAgICAgICAgICA8aW1nIHNyYz1cIi9oNy5qcGdcIiBhbHQ9XCJpbWFnZTZcIiAvPlxyXG4gICAgICAgICAgPHBcclxuICAgICAgICAgICAgc3R5bGU9e3sgYmFja2dyb3VuZDogXCIjN0ZCNzdFXCIsIGZvbnRTaXplOiBcIjI2cHhcIiwgb3BhY2l0eTogXCIxXCIgfX1cclxuICAgICAgICAgICAgY2xhc3NOYW1lPVwibGVnZW5kXCJcclxuICAgICAgICAgID5cclxuICAgICAgICAgICAgVGhpIHRob2HMiW5nIGRpzIEgZGVhZGxpbmUgY2h1zIFuZyBlbSBiw6LMgXQga8OqzIkgbmdhzIB5IMSRw6ptXHJcbiAgICAgICAgICA8L3A+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPGRpdj5cclxuICAgICAgICAgIDxpbWcgc3JjPVwiL2g4LmpwZ1wiIGFsdD1cImltYWdlN1wiIC8+XHJcbiAgICAgICAgICA8cFxyXG4gICAgICAgICAgICBzdHlsZT17eyBiYWNrZ3JvdW5kOiBcIiM3RkI3N0VcIiwgZm9udFNpemU6IFwiMjZweFwiLCBvcGFjaXR5OiBcIjFcIiB9fVxyXG4gICAgICAgICAgICBjbGFzc05hbWU9XCJsZWdlbmRcIlxyXG4gICAgICAgICAgPlxyXG4gICAgICAgICAgICBWYcyAIGN1w7TMgWkgY3XMgG5nIGxhzIAgxJBhzIMgecOqdSB0aMawxqFuZywgdGHMo28gxJFpw6rMgHUga2nDqsyjbiBjaG8gY2h1zIFuZyBlbSB0csawxqHMiW5nXHJcbiAgICAgICAgICAgIHRoYcyAbmggaMahbntcIiBcIn1cclxuICAgICAgICAgIDwvcD5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgPGltZyBzcmM9XCIvaDkuanBnXCIgYWx0PVwiaW1hZ2U4XCIgLz5cclxuICAgICAgICAgIHsvKiA8cCBzdHlsZT17e2JhY2tncm91bmQ6XCIjN0ZCNzdFXCIsIGZvbnRTaXplOlwiMjZweFwiLCBvcGFjaXR5OlwiMVwifX0gY2xhc3NOYW1lPVwibGVnZW5kXCI+VmHMgCBjdcO0zIFpIGN1zIBuZyBsYcyAICDEkGHMgyB5w6p1IHRoxrDGoW5nLCB0YcyjbyDEkWnDqsyAdSBracOqzKNuIGNobyBjaHXMgW5nIGVtIHRyxrDGocyJbmcgdGhhzIBuaCBoxqFuIDwvcD4gKi99XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPGRpdj5cclxuICAgICAgICAgIDxpbWcgc3JjPVwiL2gxMC5qcGdcIiBhbHQ9XCJpbWFnZTlcIiAvPlxyXG4gICAgICAgICAgey8qIDxwIHN0eWxlPXt7YmFja2dyb3VuZDpcIiM3RkI3N0VcIiwgZm9udFNpemU6XCIyNnB4XCIsIG9wYWNpdHk6XCIxXCJ9fSBjbGFzc05hbWU9XCJsZWdlbmRcIj5WYcyAIGN1w7TMgWkgY3XMgG5nIGxhzIAgIMSQYcyDIHnDqnUgdGjGsMahbmcsIHRhzKNvIMSRacOqzIB1IGtpw6rMo24gY2hvIGNodcyBbmcgZW0gdHLGsMahzIluZyB0aGHMgG5oIGjGoW4gPC9wPiAqL31cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgPC9DYXJvdXNlbD5cclxuICAgIDwvZGl2PlxyXG4gIDwvZGl2PlxyXG4pO1xyXG5cclxuY29uc3Qgd2lzaF90aW1lX291dCA9ICgpID0+IHtcclxuICBcclxuICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgY29uc3QgdGltZXIgPSBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgc2V0Q291bnQoJ1RpbWVvdXQgY2FsbGVkIScpO1xyXG4gICAgfSwgMTAwMCk7XHJcbiAgICByZXR1cm4gKCkgPT4gY2xlYXJUaW1lb3V0KHRpbWVyKTtcclxuICB9LCBbXSk7XHJcblxyXG4gIHJldHVybiAoXHJcbiAgICA8Y2FudmFzIGNsYXNzTmFtZT17c3R5bGVzLmNhbnZhc30gaWQ9XCJjYW52YXNcIj48L2NhbnZhcz5cclxuICApO1xyXG59O1xyXG5cclxuY29uc3QgV2lzaCA9ICh7IGhpc3RvcnkgfSkgPT4ge1xyXG4gIGNvbnN0IHJvdXRlciA9IHVzZVJvdXRlcigpO1xyXG4gIGNvbnN0IHsgbmFtZSB9ID0gcm91dGVyLnF1ZXJ5OyAvLyBnZXRzIGJvdGggbmFtZSAmIGNvbG9yIGlkIGluIGZvcm0gb2YgYXJyYXkgW25hbWUsY29sb3JJZF1cclxuICBjb25zdCBjb2xvciA9IG5hbWUgPyBuYW1lWzFdIDogMDsgLy9leHRyYWN0aW5nIGNvbG9ySWQgZnJvbSBuYW1lXHJcbiAgY29uc3QgW2Rvd25sb2FkaW5nLCBzZXREb3dubG9hZGluZ10gPSB1c2VTdGF0ZShmYWxzZSk7XHJcbiAgY29uc3QgW2Rvd25sb2FkZWRPbmNlLCBzZXREb3dubG9hZGVkT25jZV0gPSB1c2VTdGF0ZShmYWxzZSk7XHJcbiAgY29uc3QgYXVkaW9SZWYgPSB1c2VSZWYoKTtcclxuXHJcbiAgY29uc3QgeyBzZXRUaGVtZSB9ID0gdXNlVGhlbWUoKTtcclxuXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIC8vIFRoZW1lIENoYW5nZVxyXG4gICAgc2V0VGhlbWUoY29sb3IpO1xyXG5cclxuICAgIGlmIChkb3dubG9hZGluZyA9PT0gZmFsc2UpIHtcclxuICAgICAgLy8gQ29uZmV0dGlcclxuICAgICAgY29uc3QgY29uZmV0dGlTZXR0aW5ncyA9IHtcclxuICAgICAgICB0YXJnZXQ6IFwiY2FudmFzXCIsXHJcbiAgICAgICAgc3RhcnRfZnJvbV9lZGdlOiB0cnVlLFxyXG4gICAgICB9O1xyXG4gICAgICBjb25zdCBjb25mZXR0aSA9IG5ldyBDb25mZXR0aUdlbmVyYXRvcihjb25mZXR0aVNldHRpbmdzKTtcclxuICAgICAgY29uZmV0dGkucmVuZGVyKCk7XHJcbiAgICB9XHJcblxyXG4gICAgYXVkaW9SZWYuY3VycmVudC5wbGF5KCk7XHJcbiAgfSwgW2NvbG9yLCBkb3dubG9hZGluZ10pO1xyXG5cclxuICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgaWYgKGRvd25sb2FkaW5nID09PSB0cnVlICYmIGRvd25sb2FkZWRPbmNlID09PSBmYWxzZSkge1xyXG4gICAgICBkb3dubG9hZEltYWdlKCk7XHJcbiAgICB9XHJcbiAgfSwgW2Rvd25sb2FkaW5nLCBkb3dubG9hZGVkT25jZV0pO1xyXG5cclxuICAvLyBmdW5jdGlvbiBmb3IgcmFuZG9tbHkgcGlja2luZyB0aGUgbWVzc2FnZSBmcm9tIG1lc3NhZ2VzIGFycmF5XHJcbiAgY29uc3QgcmFuZG9tTnVtYmVyID0gKG1pbiwgbWF4KSA9PiB7XHJcbiAgICByZXR1cm4gTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpICogKG1heCAtIG1pbikpICsgbWluO1xyXG4gIH07XHJcblxyXG4gIGNvbnN0IGRvd25sb2FkSW1hZ2UgPSAoKSA9PiB7XHJcbiAgICBpZiAoZG93bmxvYWRlZE9uY2UgPT09IHRydWUpIHJldHVybjtcclxuXHJcbiAgICBjb25zdCBub2RlID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJpbWFnZVwiKTtcclxuXHJcbiAgICBpZiAobm9kZSkge1xyXG4gICAgICBzZXREb3dubG9hZGVkT25jZSh0cnVlKTtcclxuXHJcbiAgICAgIGh0bWxUb0ltYWdlLnRvUG5nKG5vZGUpLnRoZW4oKGJsb2IpID0+IHtcclxuICAgICAgICBGaWxlU2F2ZXIuc2F2ZUFzKGJsb2IsIFwiYmlydGhkYXktd2lzaC5wbmdcIik7XHJcbiAgICAgICAgc2V0RG93bmxvYWRpbmcoZmFsc2UpO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuICB9O1xyXG5cclxuICBjb25zdCB0aXRsZSA9IChuYW1lKSA9PiB7XHJcbiAgICBjb25zdCB3aXNoID0gXCJIYXBweSBCaXJ0aGRheSBcIiArIG5hbWUgKyBcIiFcIjtcclxuICAgIGNvbnN0IGJhc2VfbGV0dGVycyA9IFtdO1xyXG4gICAgY29uc3QgbmFtZV9sZXR0ZXJzID0gW107XHJcblxyXG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCB3aXNoLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgIGlmIChpIDwgMTUpIHtcclxuICAgICAgICBjb25zdCBsZXR0ZXIgPSB3aXNoLmNoYXJBdChpKTtcclxuICAgICAgICBiYXNlX2xldHRlcnMucHVzaChcclxuICAgICAgICAgIDxzcGFuIGtleT17aX0gc3R5bGU9e3sgXCItLWlcIjogaSArIDEgfX0+XHJcbiAgICAgICAgICAgIHtsZXR0ZXJ9XHJcbiAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBjb25zdCBsZXR0ZXIgPSB3aXNoLmNoYXJBdChpKTtcclxuICAgICAgICBuYW1lX2xldHRlcnMucHVzaChcclxuICAgICAgICAgIDxzcGFuIGtleT17aX0gc3R5bGU9e3sgXCItLWlcIjogaSArIDEgfX0gY2xhc3NOYW1lPXtzdHlsZXMuc3Bhbn0+XHJcbiAgICAgICAgICAgIHtsZXR0ZXJ9XHJcbiAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiAoXHJcbiAgICAgIDw+XHJcbiAgICAgICAge2Rvd25sb2FkaW5nID8gKFxyXG4gICAgICAgICAgPGgxXHJcbiAgICAgICAgICAgIGNsYXNzTmFtZT17c3R5bGVzLnRpdGxlSW1nfVxyXG4gICAgICAgICAgICBzdHlsZT17eyBcIi0td2lzaC1sZW5ndGhcIjogd2lzaC5sZW5ndGggfX1cclxuICAgICAgICAgID5cclxuICAgICAgICAgICAgPGRpdj57YmFzZV9sZXR0ZXJzLm1hcCgobGV0dGVyKSA9PiBsZXR0ZXIpfTwvZGl2PlxyXG4gICAgICAgICAgICA8ZGl2PntuYW1lX2xldHRlcnMubWFwKChsZXR0ZXIpID0+IGxldHRlcil9PC9kaXY+XHJcbiAgICAgICAgICA8L2gxPlxyXG4gICAgICAgICkgOiAoXHJcbiAgICAgICAgICA8aDEgY2xhc3NOYW1lPXtzdHlsZXMudGl0bGV9IHN0eWxlPXt7IFwiLS13aXNoLWxlbmd0aFwiOiB3aXNoLmxlbmd0aCB9fT5cclxuICAgICAgICAgICAgPGRpdj57YmFzZV9sZXR0ZXJzLm1hcCgobGV0dGVyKSA9PiBsZXR0ZXIpfTwvZGl2PlxyXG4gICAgICAgICAgICA8ZGl2PntuYW1lX2xldHRlcnMubWFwKChsZXR0ZXIpID0+IGxldHRlcil9PC9kaXY+XHJcbiAgICAgICAgICA8L2gxPlxyXG4gICAgICAgICl9XHJcbiAgICAgIDwvPlxyXG4gICAgKTtcclxuICB9O1xyXG5cclxuICBpZiAoZG93bmxvYWRpbmcpIHtcclxuICAgIHJldHVybiAoXHJcbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzdHlsZXMuY29udGFpbmVySW1nfSBpZD1cImltYWdlXCIgb25DbGljaz17ZG93bmxvYWRJbWFnZX0+XHJcbiAgICAgICAge2Rvd25sb2FkSW1hZ2UoKX1cclxuICAgICAgICA8bWFpbiBjbGFzc05hbWU9e3N0eWxlcy5pbWFnZX0+XHJcbiAgICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17c3R5bGVzLm1haW59Pnt0aXRsZShuYW1lICYmIG5hbWVbMF0pfTwvZGl2PlxyXG5cclxuICAgICAgICAgICAgPGRpdiBzdHlsZT17eyBoZWlnaHQ6IDQwIH19IC8+XHJcblxyXG4gICAgICAgICAgICA8cCBjbGFzc05hbWU9e3N0eWxlcy5kZXNjSW1nfT5cclxuICAgICAgICAgICAgICB7bWVzc2FnZXNbcmFuZG9tTnVtYmVyKDAsIG1lc3NhZ2VzLmxlbmd0aCldLnZhbHVlfVxyXG4gICAgICAgICAgICA8L3A+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L21haW4+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgKTtcclxuICB9XHJcblxyXG4gIHJldHVybiAoXHJcbiAgICA8ZGl2IGNsYXNzTmFtZT17c3R5bGVzLmNvbnRhaW5lcn0+XHJcbiAgICAgIDxIZWFkPlxyXG4gICAgICAgIDx0aXRsZT5IYXBweSBCaXJ0aGRheSB7bmFtZSAmJiBuYW1lWzBdfTwvdGl0bGU+XHJcbiAgICAgICAgPG1ldGEgbmFtZT1cImRlc2NyaXB0aW9uXCIgY29udGVudD1cIkdlbmVyYXRlZCBieSBjcmVhdGUgbmV4dCBhcHBcIiAvPlxyXG4gICAgICAgIDxsaW5rIHJlbD1cImljb25cIiBocmVmPVwiL2Zhdmljb24uaWNvXCIgLz5cclxuICAgICAgPC9IZWFkPlxyXG5cclxuICAgICAge3dpc2hfdGltZV9vdXR9XHJcbiAgICAgIFxyXG5cclxuICAgICAgPG1haW4gY2xhc3NOYW1lPXtzdHlsZXMuYW5pbWF0ZX0+XHJcbiAgICAgICAgPGRpdj5cclxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzdHlsZXMubWFpbn0+e3RpdGxlKG5hbWUgJiYgbmFtZVswXSl9PC9kaXY+XHJcbiAgICAgICAgICB7LyogPHAgY2xhc3NOYW1lPXtzdHlsZXMuZGVzY30+XHJcbiAgICAgICAgICAgIHttZXNzYWdlc1tyYW5kb21OdW1iZXIoMCwgbWVzc2FnZXMubGVuZ3RoKV0udmFsdWV9XHJcbiAgICAgICAgICA8L3A+ICovfVxyXG4gICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICA8RXhhbXBsZSAvPlxyXG5cclxuICAgICAgICB7LyogPGRpdiBjbGFzc05hbWU9e3N0eWxlcy5idXR0b25Db250YWluZXJ9PlxyXG4gICAgICAgICAge2hpc3RvcnlbMF0gPT0gXCIvXCIgPyA8Q29weUxpbmtCdXR0b24gLz4gOiBcIlwifVxyXG5cclxuICAgICAgICAgIHtoaXN0b3J5WzBdID09IFwiL1wiID8gKFxyXG4gICAgICAgICAgICA8QnV0dG9uXHJcbiAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgc2V0RG93bmxvYWRlZE9uY2UoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgc2V0RG93bmxvYWRpbmcodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgfX1cclxuICAgICAgICAgICAgICB0ZXh0PVwiRG93bmxvYWQgYXMgSW1hZ2VcIlxyXG4gICAgICAgICAgICAvPlxyXG4gICAgICAgICAgKSA6IChcclxuICAgICAgICAgICAgXCJcIlxyXG4gICAgICAgICAgKX1cclxuXHJcbiAgICAgICAgICA8QnV0dG9uXHJcbiAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHJvdXRlci5wdXNoKFwiL1wiKX1cclxuICAgICAgICAgICAgdGV4dD1cIiZsYXJyOyBDcmVhdGUgYSB3aXNoXCJcclxuICAgICAgICAgIC8+XHJcbiAgICAgICAgPC9kaXY+ICovfVxyXG4gICAgICA8L21haW4+XHJcbiAgICAgIDxhdWRpbyByZWY9e2F1ZGlvUmVmfSBpZD1cInBsYXllclwiIGF1dG9QbGF5PlxyXG4gICAgICAgIDxzb3VyY2Ugc3JjPVwibWVkaWEvbGVlbW9uX3RyZWUubXAzXCIgLz5cclxuICAgICAgPC9hdWRpbz5cclxuICAgIDwvZGl2PlxyXG4gICk7XHJcbn07XHJcblxyXG5leHBvcnQgZGVmYXVsdCBXaXNoO1xyXG4iXSwibmFtZXMiOlsiUmVhY3QiLCJ1c2VTdGF0ZSIsInVzZUVmZmVjdCIsInVzZVJlZiIsIkhlYWQiLCJzdHlsZXMiLCJ1c2VSb3V0ZXIiLCJDb25mZXR0aUdlbmVyYXRvciIsIm1lc3NhZ2VzIiwidXNlVGhlbWUiLCJodG1sVG9JbWFnZSIsIkZpbGVTYXZlciIsIkNhcm91c2VsIiwiRXhhbXBsZSIsIndpZHRoIiwibWFyZ2luIiwiYmFja2dyb3VuZCIsImZvbnRTaXplIiwib3BhY2l0eSIsIndpc2hfdGltZV9vdXQiLCJ0aW1lciIsInNldFRpbWVvdXQiLCJzZXRDb3VudCIsImNsZWFyVGltZW91dCIsImNhbnZhcyIsIldpc2giLCJoaXN0b3J5Iiwicm91dGVyIiwibmFtZSIsInF1ZXJ5IiwiY29sb3IiLCJkb3dubG9hZGluZyIsInNldERvd25sb2FkaW5nIiwiZG93bmxvYWRlZE9uY2UiLCJzZXREb3dubG9hZGVkT25jZSIsImF1ZGlvUmVmIiwic2V0VGhlbWUiLCJjb25mZXR0aVNldHRpbmdzIiwidGFyZ2V0Iiwic3RhcnRfZnJvbV9lZGdlIiwiY29uZmV0dGkiLCJyZW5kZXIiLCJjdXJyZW50IiwicGxheSIsImRvd25sb2FkSW1hZ2UiLCJyYW5kb21OdW1iZXIiLCJtaW4iLCJtYXgiLCJNYXRoIiwiZmxvb3IiLCJyYW5kb20iLCJub2RlIiwiZG9jdW1lbnQiLCJnZXRFbGVtZW50QnlJZCIsInRvUG5nIiwidGhlbiIsImJsb2IiLCJzYXZlQXMiLCJ0aXRsZSIsIndpc2giLCJiYXNlX2xldHRlcnMiLCJuYW1lX2xldHRlcnMiLCJpIiwibGVuZ3RoIiwibGV0dGVyIiwiY2hhckF0IiwicHVzaCIsInNwYW4iLCJ0aXRsZUltZyIsIm1hcCIsImNvbnRhaW5lckltZyIsImltYWdlIiwibWFpbiIsImhlaWdodCIsImRlc2NJbWciLCJ2YWx1ZSIsImNvbnRhaW5lciIsImFuaW1hdGUiXSwic291cmNlUm9vdCI6IiJ9