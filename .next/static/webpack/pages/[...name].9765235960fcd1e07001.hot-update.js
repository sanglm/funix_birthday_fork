"use strict";
self["webpackHotUpdate_N_E"]("pages/[...name]",{

/***/ "./pages/[...name].js":
/*!****************************!*\
  !*** ./pages/[...name].js ***!
  \****************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/head */ "./node_modules/next/head.js");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../styles/Name.module.css */ "./styles/Name.module.css");
/* harmony import */ var _styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/router */ "./node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var confetti_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! confetti-js */ "./node_modules/confetti-js/dist/index.es.js");
/* harmony import */ var _utils_birthdayWishes_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../utils/birthdayWishes.js */ "./utils/birthdayWishes.js");
/* harmony import */ var _hooks_useTheme__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../hooks/useTheme */ "./hooks/useTheme.js");
/* harmony import */ var html_to_image__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! html-to-image */ "./node_modules/html-to-image/es/index.js");
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! file-saver */ "./node_modules/file-saver/dist/FileSaver.min.js");
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(file_saver__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var react_responsive_carousel_lib_styles_carousel_min_css__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react-responsive-carousel/lib/styles/carousel.min.css */ "./node_modules/react-responsive-carousel/lib/styles/carousel.min.css");
/* harmony import */ var react_responsive_carousel_lib_styles_carousel_min_css__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react_responsive_carousel_lib_styles_carousel_min_css__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var react_responsive_carousel__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-responsive-carousel */ "./node_modules/react-responsive-carousel/lib/js/index.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__);
/* module decorator */ module = __webpack_require__.hmd(module);
var _jsxFileName = "D:\\SangLM3\\APP\\fx_birthday_xuanqn\\pages\\[...name].js",
    _this = undefined,
    _s = $RefreshSig$();















var Example = function Example() {
  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
    children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
      style: {
        width: "800px",
        margin: "auto",
        height: "800px"
      },
      children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)(react_responsive_carousel__WEBPACK_IMPORTED_MODULE_9__.Carousel, {
        autoPlay: true,
        infiniteLoop: false,
        interval: 5000,
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("img", {
            src: "/h2.jpg",
            alt: "image1"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 19,
            columnNumber: 11
          }, _this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 18,
          columnNumber: 9
        }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("img", {
            src: "/h3.jpg",
            alt: "image2"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 23,
            columnNumber: 11
          }, _this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 22,
          columnNumber: 9
        }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("img", {
            src: "/h4.jpg",
            alt: "image3"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 28,
            columnNumber: 11
          }, _this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 27,
          columnNumber: 9
        }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("img", {
            src: "/h5.jpg",
            alt: "image4"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 32,
            columnNumber: 11
          }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("p", {
            style: {
              background: "#7FB77E",
              fontSize: "26px",
              opacity: "1"
            },
            className: "legend",
            children: "Lu\xF4n la\u0300 ng\u01B0\u01A1\u0300i anh ca\u0309 cu\u0309a chu\u0301ng em"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 33,
            columnNumber: 11
          }, _this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 31,
          columnNumber: 9
        }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("img", {
            src: "/h6.jpg",
            alt: "image5"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 41,
            columnNumber: 11
          }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("p", {
            style: {
              background: "#7FB77E",
              fontSize: "26px",
              opacity: "1"
            },
            className: "legend",
            children: "3 ph\xE2\u0300n y\xEAu th\u01B0\u01A1ng 7 ph\xE2\u0300n nu\xF4ng chi\xEA\u0300u chu\u0301ng em"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 42,
            columnNumber: 11
          }, _this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 40,
          columnNumber: 9
        }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("img", {
            src: "/h7.jpg",
            alt: "image6"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 50,
            columnNumber: 11
          }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("p", {
            style: {
              background: "#7FB77E",
              fontSize: "26px",
              opacity: "1"
            },
            className: "legend",
            children: "Thi thoa\u0309ng di\u0301 deadline chu\u0301ng em b\xE2\u0301t k\xEA\u0309 nga\u0300y \u0111\xEAm"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 51,
            columnNumber: 11
          }, _this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 49,
          columnNumber: 9
        }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("img", {
            src: "/h8.jpg",
            alt: "image7"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 59,
            columnNumber: 11
          }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("p", {
            style: {
              background: "#7FB77E",
              fontSize: "26px",
              opacity: "1"
            },
            className: "legend",
            children: ["Va\u0300 cu\xF4\u0301i cu\u0300ng la\u0300 \u0110a\u0303 y\xEAu th\u01B0\u01A1ng, ta\u0323o \u0111i\xEA\u0300u ki\xEA\u0323n cho chu\u0301ng em tr\u01B0\u01A1\u0309ng tha\u0300nh h\u01A1n", " "]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 60,
            columnNumber: 11
          }, _this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 58,
          columnNumber: 9
        }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("img", {
            src: "/h9.jpg",
            alt: "image8"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 69,
            columnNumber: 11
          }, _this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 68,
          columnNumber: 9
        }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("img", {
            src: "/h10.jpg",
            alt: "image9"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 73,
            columnNumber: 11
          }, _this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 72,
          columnNumber: 9
        }, _this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 17,
        columnNumber: 7
      }, _this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 16,
      columnNumber: 5
    }, _this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 14,
    columnNumber: 3
  }, _this);
};

_c = Example;

var Wish = function Wish(_ref) {
  _s();

  var history = _ref.history;
  var router = (0,next_router__WEBPACK_IMPORTED_MODULE_2__.useRouter)();
  var name = router.query.name; // gets both name & color id in form of array [name,colorId]

  var color = name ? name[1] : 0; //extracting colorId from name

  var _useState = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)(false),
      downloading = _useState[0],
      setDownloading = _useState[1];

  var _useState2 = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)(false),
      downloadedOnce = _useState2[0],
      setDownloadedOnce = _useState2[1];

  var audioRef = (0,react__WEBPACK_IMPORTED_MODULE_0__.useRef)();

  var _useTheme = (0,_hooks_useTheme__WEBPACK_IMPORTED_MODULE_5__.default)(),
      setTheme = _useTheme.setTheme;

  (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(function () {
    // Theme Change
    setTheme(color);

    if (downloading === false) {
      // Confetti
      var confettiSettings = {
        target: "canvas",
        start_from_edge: true
      };
      var confetti = new confetti_js__WEBPACK_IMPORTED_MODULE_3__.default(confettiSettings);
      confetti.render();
    }

    audioRef.current.play();
  }, [color, downloading]);
  (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(function () {
    if (downloading === true && downloadedOnce === false) {
      downloadImage();
    }
  }, [downloading, downloadedOnce]); // function for randomly picking the message from messages array

  var randomNumber = function randomNumber(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
  };

  var downloadImage = function downloadImage() {
    if (downloadedOnce === true) return;
    var node = document.getElementById("image");

    if (node) {
      setDownloadedOnce(true);
      html_to_image__WEBPACK_IMPORTED_MODULE_6__.toPng(node).then(function (blob) {
        file_saver__WEBPACK_IMPORTED_MODULE_7___default().saveAs(blob, "birthday-wish.png");
        setDownloading(false);
      });
    }
  };

  var title = function title(name) {
    var wish = "Happy Birthday " + name + "!";
    var base_letters = [];
    var name_letters = [];

    for (var i = 0; i < wish.length; i++) {
      if (i < 15) {
        var letter = wish.charAt(i);
        base_letters.push( /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("span", {
          style: {
            "--i": i + 1
          },
          children: letter
        }, i, false, {
          fileName: _jsxFileName,
          lineNumber: 145,
          columnNumber: 11
        }, _this));
      } else {
        var _letter = wish.charAt(i);

        name_letters.push( /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("span", {
          style: {
            "--i": i + 1
          },
          className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().span),
          children: _letter
        }, i, false, {
          fileName: _jsxFileName,
          lineNumber: 152,
          columnNumber: 11
        }, _this));
      }
    }

    return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.Fragment, {
      children: downloading ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("h1", {
        className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().titleImg),
        style: {
          "--wish-length": wish.length
        },
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: base_letters.map(function (letter) {
            return letter;
          })
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 166,
          columnNumber: 13
        }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: name_letters.map(function (letter) {
            return letter;
          })
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 167,
          columnNumber: 13
        }, _this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 162,
        columnNumber: 11
      }, _this) : /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("h1", {
        className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().title),
        style: {
          "--wish-length": wish.length
        },
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: base_letters.map(function (letter) {
            return letter;
          })
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 171,
          columnNumber: 13
        }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: name_letters.map(function (letter) {
            return letter;
          })
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 172,
          columnNumber: 13
        }, _this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 170,
        columnNumber: 11
      }, _this)
    }, void 0, false);
  };

  if (downloading) {
    return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
      className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().containerImg),
      id: "image",
      onClick: downloadImage,
      children: [downloadImage(), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("main", {
        className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().image),
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
            className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().main),
            children: title(name && name[0])
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 185,
            columnNumber: 13
          }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
            style: {
              height: 40
            }
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 187,
            columnNumber: 13
          }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("p", {
            className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().descImg),
            children: _utils_birthdayWishes_js__WEBPACK_IMPORTED_MODULE_4__.default[randomNumber(0, _utils_birthdayWishes_js__WEBPACK_IMPORTED_MODULE_4__.default.length)].value
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 189,
            columnNumber: 13
          }, _this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 184,
          columnNumber: 11
        }, _this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 183,
        columnNumber: 9
      }, _this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 181,
      columnNumber: 7
    }, _this);
  }

  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
    className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().container),
    children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)((next_head__WEBPACK_IMPORTED_MODULE_1___default()), {
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("title", {
        children: ["Happy Birthday ", name && name[0]]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 201,
        columnNumber: 9
      }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("meta", {
        name: "description",
        content: "Generated by create next app"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 202,
        columnNumber: 9
      }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("link", {
        rel: "icon",
        href: "/favicon.ico"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 203,
        columnNumber: 9
      }, _this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 200,
      columnNumber: 7
    }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("canvas", {
      className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().canvas),
      id: "canvas"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 206,
      columnNumber: 7
    }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("main", {
      className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().animate),
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().main) + ' hidden_it',
          children: title(name && name[0])
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 210,
          columnNumber: 11
        }, _this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 209,
        columnNumber: 9
      }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)(Example, {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 216,
        columnNumber: 9
      }, _this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 208,
      columnNumber: 7
    }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("audio", {
      ref: audioRef,
      id: "player",
      autoPlay: true,
      children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("source", {
        src: "media/leemon_tree.mp3"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 240,
        columnNumber: 9
      }, _this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 239,
      columnNumber: 7
    }, _this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 199,
    columnNumber: 5
  }, _this);
};

_s(Wish, "KC1aZNv21wLD2inhQfaHb0yrDYI=", false, function () {
  return [next_router__WEBPACK_IMPORTED_MODULE_2__.useRouter, _hooks_useTheme__WEBPACK_IMPORTED_MODULE_5__.default];
});

_c2 = Wish;
/* harmony default export */ __webpack_exports__["default"] = (Wish);

var _c, _c2;

$RefreshReg$(_c, "Example");
$RefreshReg$(_c2, "Wish");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.id);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }


/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvWy4uLm5hbWVdLjk3NjUyMzU5NjBmY2QxZTA3MDAxLmhvdC11cGRhdGUuanMiLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFFQSxJQUFNYSxPQUFPLEdBQUcsU0FBVkEsT0FBVTtBQUFBLHNCQUNkO0FBQUEsMkJBRUU7QUFBSyxXQUFLLEVBQUU7QUFBRUMsUUFBQUEsS0FBSyxFQUFFLE9BQVQ7QUFBa0JDLFFBQUFBLE1BQU0sRUFBRSxNQUExQjtBQUFrQ0MsUUFBQUEsTUFBTSxFQUFFO0FBQTFDLE9BQVo7QUFBQSw2QkFDRSwrREFBQywrREFBRDtBQUFVLGdCQUFRLEVBQUUsSUFBcEI7QUFBMEIsb0JBQVksRUFBRSxLQUF4QztBQUErQyxnQkFBUSxFQUFFLElBQXpEO0FBQUEsZ0NBQ0U7QUFBQSxpQ0FDRTtBQUFLLGVBQUcsRUFBQyxTQUFUO0FBQW1CLGVBQUcsRUFBQztBQUF2QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFERixlQUtFO0FBQUEsaUNBQ0U7QUFBSyxlQUFHLEVBQUMsU0FBVDtBQUFtQixlQUFHLEVBQUM7QUFBdkI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBTEYsZUFVRTtBQUFBLGlDQUNFO0FBQUssZUFBRyxFQUFDLFNBQVQ7QUFBbUIsZUFBRyxFQUFDO0FBQXZCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQVZGLGVBY0U7QUFBQSxrQ0FDRTtBQUFLLGVBQUcsRUFBQyxTQUFUO0FBQW1CLGVBQUcsRUFBQztBQUF2QjtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQURGLGVBRUU7QUFDRSxpQkFBSyxFQUFFO0FBQUVDLGNBQUFBLFVBQVUsRUFBRSxTQUFkO0FBQXlCQyxjQUFBQSxRQUFRLEVBQUUsTUFBbkM7QUFBMkNDLGNBQUFBLE9BQU8sRUFBRTtBQUFwRCxhQURUO0FBRUUscUJBQVMsRUFBQyxRQUZaO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFkRixlQXVCRTtBQUFBLGtDQUNFO0FBQUssZUFBRyxFQUFDLFNBQVQ7QUFBbUIsZUFBRyxFQUFDO0FBQXZCO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBREYsZUFFRTtBQUNFLGlCQUFLLEVBQUU7QUFBRUYsY0FBQUEsVUFBVSxFQUFFLFNBQWQ7QUFBeUJDLGNBQUFBLFFBQVEsRUFBRSxNQUFuQztBQUEyQ0MsY0FBQUEsT0FBTyxFQUFFO0FBQXBELGFBRFQ7QUFFRSxxQkFBUyxFQUFDLFFBRlo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQXZCRixlQWdDRTtBQUFBLGtDQUNFO0FBQUssZUFBRyxFQUFDLFNBQVQ7QUFBbUIsZUFBRyxFQUFDO0FBQXZCO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBREYsZUFFRTtBQUNFLGlCQUFLLEVBQUU7QUFBRUYsY0FBQUEsVUFBVSxFQUFFLFNBQWQ7QUFBeUJDLGNBQUFBLFFBQVEsRUFBRSxNQUFuQztBQUEyQ0MsY0FBQUEsT0FBTyxFQUFFO0FBQXBELGFBRFQ7QUFFRSxxQkFBUyxFQUFDLFFBRlo7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQWhDRixlQXlDRTtBQUFBLGtDQUNFO0FBQUssZUFBRyxFQUFDLFNBQVQ7QUFBbUIsZUFBRyxFQUFDO0FBQXZCO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBREYsZUFFRTtBQUNFLGlCQUFLLEVBQUU7QUFBRUYsY0FBQUEsVUFBVSxFQUFFLFNBQWQ7QUFBeUJDLGNBQUFBLFFBQVEsRUFBRSxNQUFuQztBQUEyQ0MsY0FBQUEsT0FBTyxFQUFFO0FBQXBELGFBRFQ7QUFFRSxxQkFBUyxFQUFDLFFBRlo7QUFBQSxzTkFLYSxHQUxiO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBekNGLGVBbURFO0FBQUEsaUNBQ0U7QUFBSyxlQUFHLEVBQUMsU0FBVDtBQUFtQixlQUFHLEVBQUM7QUFBdkI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBbkRGLGVBdURFO0FBQUEsaUNBQ0U7QUFBSyxlQUFHLEVBQUMsVUFBVDtBQUFvQixlQUFHLEVBQUM7QUFBeEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBdkRGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFGRjtBQUFBO0FBQUE7QUFBQTtBQUFBLFdBRGM7QUFBQSxDQUFoQjs7S0FBTU47O0FBc0VOLElBQU1PLElBQUksR0FBRyxTQUFQQSxJQUFPLE9BQWlCO0FBQUE7O0FBQUEsTUFBZEMsT0FBYyxRQUFkQSxPQUFjO0FBQzVCLE1BQU1DLE1BQU0sR0FBR2hCLHNEQUFTLEVBQXhCO0FBQ0EsTUFBUWlCLElBQVIsR0FBaUJELE1BQU0sQ0FBQ0UsS0FBeEIsQ0FBUUQsSUFBUixDQUY0QixDQUVHOztBQUMvQixNQUFNRSxLQUFLLEdBQUdGLElBQUksR0FBR0EsSUFBSSxDQUFDLENBQUQsQ0FBUCxHQUFhLENBQS9CLENBSDRCLENBR007O0FBQ2xDLGtCQUFzQ3RCLCtDQUFRLENBQUMsS0FBRCxDQUE5QztBQUFBLE1BQU95QixXQUFQO0FBQUEsTUFBb0JDLGNBQXBCOztBQUNBLG1CQUE0QzFCLCtDQUFRLENBQUMsS0FBRCxDQUFwRDtBQUFBLE1BQU8yQixjQUFQO0FBQUEsTUFBdUJDLGlCQUF2Qjs7QUFDQSxNQUFNQyxRQUFRLEdBQUczQiw2Q0FBTSxFQUF2Qjs7QUFFQSxrQkFBcUJNLHdEQUFRLEVBQTdCO0FBQUEsTUFBUXNCLFFBQVIsYUFBUUEsUUFBUjs7QUFFQTdCLEVBQUFBLGdEQUFTLENBQUMsWUFBTTtBQUNkO0FBQ0E2QixJQUFBQSxRQUFRLENBQUNOLEtBQUQsQ0FBUjs7QUFFQSxRQUFJQyxXQUFXLEtBQUssS0FBcEIsRUFBMkI7QUFDekI7QUFDQSxVQUFNTSxnQkFBZ0IsR0FBRztBQUN2QkMsUUFBQUEsTUFBTSxFQUFFLFFBRGU7QUFFdkJDLFFBQUFBLGVBQWUsRUFBRTtBQUZNLE9BQXpCO0FBSUEsVUFBTUMsUUFBUSxHQUFHLElBQUk1QixnREFBSixDQUFzQnlCLGdCQUF0QixDQUFqQjtBQUNBRyxNQUFBQSxRQUFRLENBQUNDLE1BQVQ7QUFDRDs7QUFFRE4sSUFBQUEsUUFBUSxDQUFDTyxPQUFULENBQWlCQyxJQUFqQjtBQUNELEdBZlEsRUFlTixDQUFDYixLQUFELEVBQVFDLFdBQVIsQ0FmTSxDQUFUO0FBaUJBeEIsRUFBQUEsZ0RBQVMsQ0FBQyxZQUFNO0FBQ2QsUUFBSXdCLFdBQVcsS0FBSyxJQUFoQixJQUF3QkUsY0FBYyxLQUFLLEtBQS9DLEVBQXNEO0FBQ3BEVyxNQUFBQSxhQUFhO0FBQ2Q7QUFDRixHQUpRLEVBSU4sQ0FBQ2IsV0FBRCxFQUFjRSxjQUFkLENBSk0sQ0FBVCxDQTNCNEIsQ0FpQzVCOztBQUNBLE1BQU1ZLFlBQVksR0FBRyxTQUFmQSxZQUFlLENBQUNDLEdBQUQsRUFBTUMsR0FBTixFQUFjO0FBQ2pDLFdBQU9DLElBQUksQ0FBQ0MsS0FBTCxDQUFXRCxJQUFJLENBQUNFLE1BQUwsTUFBaUJILEdBQUcsR0FBR0QsR0FBdkIsQ0FBWCxJQUEwQ0EsR0FBakQ7QUFDRCxHQUZEOztBQUlBLE1BQU1GLGFBQWEsR0FBRyxTQUFoQkEsYUFBZ0IsR0FBTTtBQUMxQixRQUFJWCxjQUFjLEtBQUssSUFBdkIsRUFBNkI7QUFFN0IsUUFBTWtCLElBQUksR0FBR0MsUUFBUSxDQUFDQyxjQUFULENBQXdCLE9BQXhCLENBQWI7O0FBRUEsUUFBSUYsSUFBSixFQUFVO0FBQ1JqQixNQUFBQSxpQkFBaUIsQ0FBQyxJQUFELENBQWpCO0FBRUFuQixNQUFBQSxnREFBQSxDQUFrQm9DLElBQWxCLEVBQXdCSSxJQUF4QixDQUE2QixVQUFDQyxJQUFELEVBQVU7QUFDckN4QyxRQUFBQSx3REFBQSxDQUFpQndDLElBQWpCLEVBQXVCLG1CQUF2QjtBQUNBeEIsUUFBQUEsY0FBYyxDQUFDLEtBQUQsQ0FBZDtBQUNELE9BSEQ7QUFJRDtBQUNGLEdBYkQ7O0FBZUEsTUFBTTBCLEtBQUssR0FBRyxTQUFSQSxLQUFRLENBQUM5QixJQUFELEVBQVU7QUFDdEIsUUFBTStCLElBQUksR0FBRyxvQkFBb0IvQixJQUFwQixHQUEyQixHQUF4QztBQUNBLFFBQU1nQyxZQUFZLEdBQUcsRUFBckI7QUFDQSxRQUFNQyxZQUFZLEdBQUcsRUFBckI7O0FBRUEsU0FBSyxJQUFJQyxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHSCxJQUFJLENBQUNJLE1BQXpCLEVBQWlDRCxDQUFDLEVBQWxDLEVBQXNDO0FBQ3BDLFVBQUlBLENBQUMsR0FBRyxFQUFSLEVBQVk7QUFDVixZQUFNRSxNQUFNLEdBQUdMLElBQUksQ0FBQ00sTUFBTCxDQUFZSCxDQUFaLENBQWY7QUFDQUYsUUFBQUEsWUFBWSxDQUFDTSxJQUFiLGVBQ0U7QUFBYyxlQUFLLEVBQUU7QUFBRSxtQkFBT0osQ0FBQyxHQUFHO0FBQWIsV0FBckI7QUFBQSxvQkFDR0U7QUFESCxXQUFXRixDQUFYO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBREY7QUFLRCxPQVBELE1BT087QUFDTCxZQUFNRSxPQUFNLEdBQUdMLElBQUksQ0FBQ00sTUFBTCxDQUFZSCxDQUFaLENBQWY7O0FBQ0FELFFBQUFBLFlBQVksQ0FBQ0ssSUFBYixlQUNFO0FBQWMsZUFBSyxFQUFFO0FBQUUsbUJBQU9KLENBQUMsR0FBRztBQUFiLFdBQXJCO0FBQXVDLG1CQUFTLEVBQUVwRCxzRUFBbEQ7QUFBQSxvQkFDR3NEO0FBREgsV0FBV0YsQ0FBWDtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQURGO0FBS0Q7QUFDRjs7QUFFRCx3QkFDRTtBQUFBLGdCQUNHL0IsV0FBVyxnQkFDVjtBQUNFLGlCQUFTLEVBQUVyQiwwRUFEYjtBQUVFLGFBQUssRUFBRTtBQUFFLDJCQUFpQmlELElBQUksQ0FBQ0k7QUFBeEIsU0FGVDtBQUFBLGdDQUlFO0FBQUEsb0JBQU1ILFlBQVksQ0FBQ1MsR0FBYixDQUFpQixVQUFDTCxNQUFEO0FBQUEsbUJBQVlBLE1BQVo7QUFBQSxXQUFqQjtBQUFOO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBSkYsZUFLRTtBQUFBLG9CQUFNSCxZQUFZLENBQUNRLEdBQWIsQ0FBaUIsVUFBQ0wsTUFBRDtBQUFBLG1CQUFZQSxNQUFaO0FBQUEsV0FBakI7QUFBTjtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUxGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQURVLGdCQVNWO0FBQUksaUJBQVMsRUFBRXRELHVFQUFmO0FBQTZCLGFBQUssRUFBRTtBQUFFLDJCQUFpQmlELElBQUksQ0FBQ0k7QUFBeEIsU0FBcEM7QUFBQSxnQ0FDRTtBQUFBLG9CQUFNSCxZQUFZLENBQUNTLEdBQWIsQ0FBaUIsVUFBQ0wsTUFBRDtBQUFBLG1CQUFZQSxNQUFaO0FBQUEsV0FBakI7QUFBTjtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQURGLGVBRUU7QUFBQSxvQkFBTUgsWUFBWSxDQUFDUSxHQUFiLENBQWlCLFVBQUNMLE1BQUQ7QUFBQSxtQkFBWUEsTUFBWjtBQUFBLFdBQWpCO0FBQU47QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFWSixxQkFERjtBQWtCRCxHQXpDRDs7QUEyQ0EsTUFBSWpDLFdBQUosRUFBaUI7QUFDZix3QkFDRTtBQUFLLGVBQVMsRUFBRXJCLDhFQUFoQjtBQUFxQyxRQUFFLEVBQUMsT0FBeEM7QUFBZ0QsYUFBTyxFQUFFa0MsYUFBekQ7QUFBQSxpQkFDR0EsYUFBYSxFQURoQixlQUVFO0FBQU0saUJBQVMsRUFBRWxDLHVFQUFqQjtBQUFBLCtCQUNFO0FBQUEsa0NBQ0U7QUFBSyxxQkFBUyxFQUFFQSxzRUFBaEI7QUFBQSxzQkFBOEJnRCxLQUFLLENBQUM5QixJQUFJLElBQUlBLElBQUksQ0FBQyxDQUFELENBQWI7QUFBbkM7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFERixlQUdFO0FBQUssaUJBQUssRUFBRTtBQUFFUCxjQUFBQSxNQUFNLEVBQUU7QUFBVjtBQUFaO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBSEYsZUFLRTtBQUFHLHFCQUFTLEVBQUVYLHlFQUFkO0FBQUEsc0JBQ0dHLDZEQUFRLENBQUNnQyxZQUFZLENBQUMsQ0FBRCxFQUFJaEMsb0VBQUosQ0FBYixDQUFSLENBQTJDNkQ7QUFEOUM7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFMRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGFBREY7QUFnQkQ7O0FBRUQsc0JBQ0U7QUFBSyxhQUFTLEVBQUVoRSwyRUFBaEI7QUFBQSw0QkFDRSwrREFBQyxrREFBRDtBQUFBLDhCQUNFO0FBQUEsc0NBQXVCa0IsSUFBSSxJQUFJQSxJQUFJLENBQUMsQ0FBRCxDQUFuQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUFERixlQUVFO0FBQU0sWUFBSSxFQUFDLGFBQVg7QUFBeUIsZUFBTyxFQUFDO0FBQWpDO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUFGRixlQUdFO0FBQU0sV0FBRyxFQUFDLE1BQVY7QUFBaUIsWUFBSSxFQUFDO0FBQXRCO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUFIRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsYUFERixlQU9FO0FBQVEsZUFBUyxFQUFFbEIsd0VBQW5CO0FBQW1DLFFBQUUsRUFBQztBQUF0QztBQUFBO0FBQUE7QUFBQTtBQUFBLGFBUEYsZUFTRTtBQUFNLGVBQVMsRUFBRUEseUVBQWpCO0FBQUEsOEJBQ0U7QUFBQSwrQkFDRTtBQUFLLG1CQUFTLEVBQUVBLHNFQUFBLEdBQWEsWUFBN0I7QUFBQSxvQkFBNENnRCxLQUFLLENBQUM5QixJQUFJLElBQUlBLElBQUksQ0FBQyxDQUFELENBQWI7QUFBakQ7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUFERixlQVFFLCtEQUFDLE9BQUQ7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQVJGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxhQVRGLGVBd0NFO0FBQU8sU0FBRyxFQUFFTyxRQUFaO0FBQXNCLFFBQUUsRUFBQyxRQUF6QjtBQUFrQyxjQUFRLE1BQTFDO0FBQUEsNkJBQ0U7QUFBUSxXQUFHLEVBQUM7QUFBWjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxhQXhDRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsV0FERjtBQThDRCxDQWpLRDs7R0FBTVY7VUFDV2Qsb0RBT01HOzs7TUFSakJXO0FBbUtOLCtEQUFlQSxJQUFmIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vX05fRS8uL3BhZ2VzL1suLi5uYW1lXS5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QsIHsgdXNlU3RhdGUsIHVzZUVmZmVjdCwgdXNlUmVmIH0gZnJvbSBcInJlYWN0XCI7XHJcbmltcG9ydCBIZWFkIGZyb20gXCJuZXh0L2hlYWRcIjtcclxuaW1wb3J0IHN0eWxlcyBmcm9tIFwiLi4vc3R5bGVzL05hbWUubW9kdWxlLmNzc1wiO1xyXG5pbXBvcnQgeyB1c2VSb3V0ZXIgfSBmcm9tIFwibmV4dC9yb3V0ZXJcIjtcclxuaW1wb3J0IENvbmZldHRpR2VuZXJhdG9yIGZyb20gXCJjb25mZXR0aS1qc1wiO1xyXG5pbXBvcnQgbWVzc2FnZXMgZnJvbSBcIi4uL3V0aWxzL2JpcnRoZGF5V2lzaGVzLmpzXCI7XHJcbmltcG9ydCB1c2VUaGVtZSBmcm9tIFwiLi4vaG9va3MvdXNlVGhlbWVcIjtcclxuaW1wb3J0ICogYXMgaHRtbFRvSW1hZ2UgZnJvbSBcImh0bWwtdG8taW1hZ2VcIjtcclxuaW1wb3J0IEZpbGVTYXZlciBmcm9tIFwiZmlsZS1zYXZlclwiO1xyXG5pbXBvcnQgXCJyZWFjdC1yZXNwb25zaXZlLWNhcm91c2VsL2xpYi9zdHlsZXMvY2Fyb3VzZWwubWluLmNzc1wiO1xyXG5pbXBvcnQgeyBDYXJvdXNlbCB9IGZyb20gXCJyZWFjdC1yZXNwb25zaXZlLWNhcm91c2VsXCI7XHJcblxyXG5jb25zdCBFeGFtcGxlID0gKCkgPT4gKFxyXG4gIDxkaXY+XHJcbiAgICB7LyogPGgyPkNow7pjIGFuaCBuZ8OgeSBzaW5oIG5o4bqtdCB0aOG6rXQgdnVpIHbhurssIGx1w7RuIG3huqFuaCBraG/hurssIGfhurd0IG5oaeG7gXUgdGjDoG5oIGPDtG5nIGjGoW4gbuG7r2EgYW5oIG5ow6khPC9oMj4gKi99XHJcbiAgICA8ZGl2IHN0eWxlPXt7IHdpZHRoOiBcIjgwMHB4XCIsIG1hcmdpbjogXCJhdXRvXCIsIGhlaWdodDogXCI4MDBweFwifX0+XHJcbiAgICAgIDxDYXJvdXNlbCBhdXRvUGxheT17dHJ1ZX0gaW5maW5pdGVMb29wPXtmYWxzZX0gaW50ZXJ2YWw9ezUwMDB9PlxyXG4gICAgICAgIDxkaXY+XHJcbiAgICAgICAgICA8aW1nIHNyYz1cIi9oMi5qcGdcIiBhbHQ9XCJpbWFnZTFcIiAvPlxyXG4gICAgICAgICAgey8qIDxwIHN0eWxlPXt7YmFja2dyb3VuZDpcIiM3RkI3N0VcIiwgZm9udFNpemU6XCIyNnB4XCIsIG9wYWNpdHk6XCIxXCJ9fSBjbGFzc05hbWU9XCJsZWdlbmRcIj48L3A+ICovfVxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDxkaXY+XHJcbiAgICAgICAgICA8aW1nIHNyYz1cIi9oMy5qcGdcIiBhbHQ9XCJpbWFnZTJcIiAvPlxyXG4gICAgICAgICAgey8qIDxwIHN0eWxlPXt7YmFja2dyb3VuZDpcIiM3RkI3N0VcIiwgZm9udFNpemU6XCIyNnB4XCIsIG9wYWNpdHk6XCIxXCJ9fSBjbGFzc05hbWU9XCJsZWdlbmRcIj5D4bqjbSDGoW4gYW5oIHRo4bqtdCBuaGnhu4F1IHbDrC4uLjwvcD4gKi99XHJcbiAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgIDxkaXY+XHJcbiAgICAgICAgICA8aW1nIHNyYz1cIi9oNC5qcGdcIiBhbHQ9XCJpbWFnZTNcIiAvPlxyXG4gICAgICAgICAgey8qIDxwIHN0eWxlPXt7YmFja2dyb3VuZDpcIiM3RkI3N0VcIiwgZm9udFNpemU6XCIyNnB4XCIsIG9wYWNpdHk6XCIxXCJ9fSBjbGFzc05hbWU9XCJsZWdlbmRcIj5D4bqjbSDGoW4gYW5oIHRo4bqtdCBuaGnhu4F1IHbDrC4uLjwvcD4gKi99XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPGRpdj5cclxuICAgICAgICAgIDxpbWcgc3JjPVwiL2g1LmpwZ1wiIGFsdD1cImltYWdlNFwiIC8+XHJcbiAgICAgICAgICA8cFxyXG4gICAgICAgICAgICBzdHlsZT17eyBiYWNrZ3JvdW5kOiBcIiM3RkI3N0VcIiwgZm9udFNpemU6IFwiMjZweFwiLCBvcGFjaXR5OiBcIjFcIiB9fVxyXG4gICAgICAgICAgICBjbGFzc05hbWU9XCJsZWdlbmRcIlxyXG4gICAgICAgICAgPlxyXG4gICAgICAgICAgICBMdcO0biBsYcyAIG5nxrDGocyAaSBhbmggY2HMiSBjdcyJYSBjaHXMgW5nIGVtXHJcbiAgICAgICAgICA8L3A+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPGRpdj5cclxuICAgICAgICAgIDxpbWcgc3JjPVwiL2g2LmpwZ1wiIGFsdD1cImltYWdlNVwiIC8+XHJcbiAgICAgICAgICA8cFxyXG4gICAgICAgICAgICBzdHlsZT17eyBiYWNrZ3JvdW5kOiBcIiM3RkI3N0VcIiwgZm9udFNpemU6IFwiMjZweFwiLCBvcGFjaXR5OiBcIjFcIiB9fVxyXG4gICAgICAgICAgICBjbGFzc05hbWU9XCJsZWdlbmRcIlxyXG4gICAgICAgICAgPlxyXG4gICAgICAgICAgICAzIHBow6LMgG4gecOqdSB0aMawxqFuZyA3IHBow6LMgG4gbnXDtG5nIGNoacOqzIB1IGNodcyBbmcgZW1cclxuICAgICAgICAgIDwvcD5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgPGltZyBzcmM9XCIvaDcuanBnXCIgYWx0PVwiaW1hZ2U2XCIgLz5cclxuICAgICAgICAgIDxwXHJcbiAgICAgICAgICAgIHN0eWxlPXt7IGJhY2tncm91bmQ6IFwiIzdGQjc3RVwiLCBmb250U2l6ZTogXCIyNnB4XCIsIG9wYWNpdHk6IFwiMVwiIH19XHJcbiAgICAgICAgICAgIGNsYXNzTmFtZT1cImxlZ2VuZFwiXHJcbiAgICAgICAgICA+XHJcbiAgICAgICAgICAgIFRoaSB0aG9hzIluZyBkacyBIGRlYWRsaW5lIGNodcyBbmcgZW0gYsOizIF0IGvDqsyJIG5nYcyAeSDEkcOqbVxyXG4gICAgICAgICAgPC9wPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDxkaXY+XHJcbiAgICAgICAgICA8aW1nIHNyYz1cIi9oOC5qcGdcIiBhbHQ9XCJpbWFnZTdcIiAvPlxyXG4gICAgICAgICAgPHBcclxuICAgICAgICAgICAgc3R5bGU9e3sgYmFja2dyb3VuZDogXCIjN0ZCNzdFXCIsIGZvbnRTaXplOiBcIjI2cHhcIiwgb3BhY2l0eTogXCIxXCIgfX1cclxuICAgICAgICAgICAgY2xhc3NOYW1lPVwibGVnZW5kXCJcclxuICAgICAgICAgID5cclxuICAgICAgICAgICAgVmHMgCBjdcO0zIFpIGN1zIBuZyBsYcyAIMSQYcyDIHnDqnUgdGjGsMahbmcsIHRhzKNvIMSRacOqzIB1IGtpw6rMo24gY2hvIGNodcyBbmcgZW0gdHLGsMahzIluZ1xyXG4gICAgICAgICAgICB0aGHMgG5oIGjGoW57XCIgXCJ9XHJcbiAgICAgICAgICA8L3A+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPGRpdj5cclxuICAgICAgICAgIDxpbWcgc3JjPVwiL2g5LmpwZ1wiIGFsdD1cImltYWdlOFwiIC8+XHJcbiAgICAgICAgICB7LyogPHAgc3R5bGU9e3tiYWNrZ3JvdW5kOlwiIzdGQjc3RVwiLCBmb250U2l6ZTpcIjI2cHhcIiwgb3BhY2l0eTpcIjFcIn19IGNsYXNzTmFtZT1cImxlZ2VuZFwiPlZhzIAgY3XDtMyBaSBjdcyAbmcgbGHMgCAgxJBhzIMgecOqdSB0aMawxqFuZywgdGHMo28gxJFpw6rMgHUga2nDqsyjbiBjaG8gY2h1zIFuZyBlbSB0csawxqHMiW5nIHRoYcyAbmggaMahbiA8L3A+ICovfVxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDxkaXY+XHJcbiAgICAgICAgICA8aW1nIHNyYz1cIi9oMTAuanBnXCIgYWx0PVwiaW1hZ2U5XCIgLz5cclxuICAgICAgICAgIHsvKiA8cCBzdHlsZT17e2JhY2tncm91bmQ6XCIjN0ZCNzdFXCIsIGZvbnRTaXplOlwiMjZweFwiLCBvcGFjaXR5OlwiMVwifX0gY2xhc3NOYW1lPVwibGVnZW5kXCI+VmHMgCBjdcO0zIFpIGN1zIBuZyBsYcyAICDEkGHMgyB5w6p1IHRoxrDGoW5nLCB0YcyjbyDEkWnDqsyAdSBracOqzKNuIGNobyBjaHXMgW5nIGVtIHRyxrDGocyJbmcgdGhhzIBuaCBoxqFuIDwvcD4gKi99XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgIDwvQ2Fyb3VzZWw+XHJcbiAgICA8L2Rpdj5cclxuICA8L2Rpdj5cclxuKTtcclxuXHJcblxyXG5cclxuY29uc3QgV2lzaCA9ICh7IGhpc3RvcnkgfSkgPT4ge1xyXG4gIGNvbnN0IHJvdXRlciA9IHVzZVJvdXRlcigpO1xyXG4gIGNvbnN0IHsgbmFtZSB9ID0gcm91dGVyLnF1ZXJ5OyAvLyBnZXRzIGJvdGggbmFtZSAmIGNvbG9yIGlkIGluIGZvcm0gb2YgYXJyYXkgW25hbWUsY29sb3JJZF1cclxuICBjb25zdCBjb2xvciA9IG5hbWUgPyBuYW1lWzFdIDogMDsgLy9leHRyYWN0aW5nIGNvbG9ySWQgZnJvbSBuYW1lXHJcbiAgY29uc3QgW2Rvd25sb2FkaW5nLCBzZXREb3dubG9hZGluZ10gPSB1c2VTdGF0ZShmYWxzZSk7XHJcbiAgY29uc3QgW2Rvd25sb2FkZWRPbmNlLCBzZXREb3dubG9hZGVkT25jZV0gPSB1c2VTdGF0ZShmYWxzZSk7XHJcbiAgY29uc3QgYXVkaW9SZWYgPSB1c2VSZWYoKTtcclxuXHJcbiAgY29uc3QgeyBzZXRUaGVtZSB9ID0gdXNlVGhlbWUoKTtcclxuXHJcbiAgdXNlRWZmZWN0KCgpID0+IHtcclxuICAgIC8vIFRoZW1lIENoYW5nZVxyXG4gICAgc2V0VGhlbWUoY29sb3IpO1xyXG5cclxuICAgIGlmIChkb3dubG9hZGluZyA9PT0gZmFsc2UpIHtcclxuICAgICAgLy8gQ29uZmV0dGlcclxuICAgICAgY29uc3QgY29uZmV0dGlTZXR0aW5ncyA9IHtcclxuICAgICAgICB0YXJnZXQ6IFwiY2FudmFzXCIsXHJcbiAgICAgICAgc3RhcnRfZnJvbV9lZGdlOiB0cnVlLFxyXG4gICAgICB9O1xyXG4gICAgICBjb25zdCBjb25mZXR0aSA9IG5ldyBDb25mZXR0aUdlbmVyYXRvcihjb25mZXR0aVNldHRpbmdzKTtcclxuICAgICAgY29uZmV0dGkucmVuZGVyKCk7XHJcbiAgICB9XHJcblxyXG4gICAgYXVkaW9SZWYuY3VycmVudC5wbGF5KCk7XHJcbiAgfSwgW2NvbG9yLCBkb3dubG9hZGluZ10pO1xyXG5cclxuICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgaWYgKGRvd25sb2FkaW5nID09PSB0cnVlICYmIGRvd25sb2FkZWRPbmNlID09PSBmYWxzZSkge1xyXG4gICAgICBkb3dubG9hZEltYWdlKCk7XHJcbiAgICB9XHJcbiAgfSwgW2Rvd25sb2FkaW5nLCBkb3dubG9hZGVkT25jZV0pO1xyXG5cclxuICAvLyBmdW5jdGlvbiBmb3IgcmFuZG9tbHkgcGlja2luZyB0aGUgbWVzc2FnZSBmcm9tIG1lc3NhZ2VzIGFycmF5XHJcbiAgY29uc3QgcmFuZG9tTnVtYmVyID0gKG1pbiwgbWF4KSA9PiB7XHJcbiAgICByZXR1cm4gTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpICogKG1heCAtIG1pbikpICsgbWluO1xyXG4gIH07XHJcblxyXG4gIGNvbnN0IGRvd25sb2FkSW1hZ2UgPSAoKSA9PiB7XHJcbiAgICBpZiAoZG93bmxvYWRlZE9uY2UgPT09IHRydWUpIHJldHVybjtcclxuXHJcbiAgICBjb25zdCBub2RlID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJpbWFnZVwiKTtcclxuXHJcbiAgICBpZiAobm9kZSkge1xyXG4gICAgICBzZXREb3dubG9hZGVkT25jZSh0cnVlKTtcclxuXHJcbiAgICAgIGh0bWxUb0ltYWdlLnRvUG5nKG5vZGUpLnRoZW4oKGJsb2IpID0+IHtcclxuICAgICAgICBGaWxlU2F2ZXIuc2F2ZUFzKGJsb2IsIFwiYmlydGhkYXktd2lzaC5wbmdcIik7XHJcbiAgICAgICAgc2V0RG93bmxvYWRpbmcoZmFsc2UpO1xyXG4gICAgICB9KTtcclxuICAgIH1cclxuICB9O1xyXG5cclxuICBjb25zdCB0aXRsZSA9IChuYW1lKSA9PiB7XHJcbiAgICBjb25zdCB3aXNoID0gXCJIYXBweSBCaXJ0aGRheSBcIiArIG5hbWUgKyBcIiFcIjtcclxuICAgIGNvbnN0IGJhc2VfbGV0dGVycyA9IFtdO1xyXG4gICAgY29uc3QgbmFtZV9sZXR0ZXJzID0gW107XHJcblxyXG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCB3aXNoLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgIGlmIChpIDwgMTUpIHtcclxuICAgICAgICBjb25zdCBsZXR0ZXIgPSB3aXNoLmNoYXJBdChpKTtcclxuICAgICAgICBiYXNlX2xldHRlcnMucHVzaChcclxuICAgICAgICAgIDxzcGFuIGtleT17aX0gc3R5bGU9e3sgXCItLWlcIjogaSArIDEgfX0+XHJcbiAgICAgICAgICAgIHtsZXR0ZXJ9XHJcbiAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgKTtcclxuICAgICAgfSBlbHNlIHtcclxuICAgICAgICBjb25zdCBsZXR0ZXIgPSB3aXNoLmNoYXJBdChpKTtcclxuICAgICAgICBuYW1lX2xldHRlcnMucHVzaChcclxuICAgICAgICAgIDxzcGFuIGtleT17aX0gc3R5bGU9e3sgXCItLWlcIjogaSArIDEgfX0gY2xhc3NOYW1lPXtzdHlsZXMuc3Bhbn0+XHJcbiAgICAgICAgICAgIHtsZXR0ZXJ9XHJcbiAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgKTtcclxuICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHJldHVybiAoXHJcbiAgICAgIDw+XHJcbiAgICAgICAge2Rvd25sb2FkaW5nID8gKFxyXG4gICAgICAgICAgPGgxXHJcbiAgICAgICAgICAgIGNsYXNzTmFtZT17c3R5bGVzLnRpdGxlSW1nfVxyXG4gICAgICAgICAgICBzdHlsZT17eyBcIi0td2lzaC1sZW5ndGhcIjogd2lzaC5sZW5ndGggfX1cclxuICAgICAgICAgID5cclxuICAgICAgICAgICAgPGRpdj57YmFzZV9sZXR0ZXJzLm1hcCgobGV0dGVyKSA9PiBsZXR0ZXIpfTwvZGl2PlxyXG4gICAgICAgICAgICA8ZGl2PntuYW1lX2xldHRlcnMubWFwKChsZXR0ZXIpID0+IGxldHRlcil9PC9kaXY+XHJcbiAgICAgICAgICA8L2gxPlxyXG4gICAgICAgICkgOiAoXHJcbiAgICAgICAgICA8aDEgY2xhc3NOYW1lPXtzdHlsZXMudGl0bGV9IHN0eWxlPXt7IFwiLS13aXNoLWxlbmd0aFwiOiB3aXNoLmxlbmd0aCB9fT5cclxuICAgICAgICAgICAgPGRpdj57YmFzZV9sZXR0ZXJzLm1hcCgobGV0dGVyKSA9PiBsZXR0ZXIpfTwvZGl2PlxyXG4gICAgICAgICAgICA8ZGl2PntuYW1lX2xldHRlcnMubWFwKChsZXR0ZXIpID0+IGxldHRlcil9PC9kaXY+XHJcbiAgICAgICAgICA8L2gxPlxyXG4gICAgICAgICl9XHJcbiAgICAgIDwvPlxyXG4gICAgKTtcclxuICB9O1xyXG5cclxuICBpZiAoZG93bmxvYWRpbmcpIHtcclxuICAgIHJldHVybiAoXHJcbiAgICAgIDxkaXYgY2xhc3NOYW1lPXtzdHlsZXMuY29udGFpbmVySW1nfSBpZD1cImltYWdlXCIgb25DbGljaz17ZG93bmxvYWRJbWFnZX0+XHJcbiAgICAgICAge2Rvd25sb2FkSW1hZ2UoKX1cclxuICAgICAgICA8bWFpbiBjbGFzc05hbWU9e3N0eWxlcy5pbWFnZX0+XHJcbiAgICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT17c3R5bGVzLm1haW59Pnt0aXRsZShuYW1lICYmIG5hbWVbMF0pfTwvZGl2PlxyXG5cclxuICAgICAgICAgICAgPGRpdiBzdHlsZT17eyBoZWlnaHQ6IDQwIH19IC8+XHJcblxyXG4gICAgICAgICAgICA8cCBjbGFzc05hbWU9e3N0eWxlcy5kZXNjSW1nfT5cclxuICAgICAgICAgICAgICB7bWVzc2FnZXNbcmFuZG9tTnVtYmVyKDAsIG1lc3NhZ2VzLmxlbmd0aCldLnZhbHVlfVxyXG4gICAgICAgICAgICA8L3A+XHJcbiAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L21haW4+XHJcbiAgICAgIDwvZGl2PlxyXG4gICAgKTtcclxuICB9XHJcblxyXG4gIHJldHVybiAoXHJcbiAgICA8ZGl2IGNsYXNzTmFtZT17c3R5bGVzLmNvbnRhaW5lcn0+XHJcbiAgICAgIDxIZWFkPlxyXG4gICAgICAgIDx0aXRsZT5IYXBweSBCaXJ0aGRheSB7bmFtZSAmJiBuYW1lWzBdfTwvdGl0bGU+XHJcbiAgICAgICAgPG1ldGEgbmFtZT1cImRlc2NyaXB0aW9uXCIgY29udGVudD1cIkdlbmVyYXRlZCBieSBjcmVhdGUgbmV4dCBhcHBcIiAvPlxyXG4gICAgICAgIDxsaW5rIHJlbD1cImljb25cIiBocmVmPVwiL2Zhdmljb24uaWNvXCIgLz5cclxuICAgICAgPC9IZWFkPlxyXG5cclxuICAgICAgPGNhbnZhcyBjbGFzc05hbWU9e3N0eWxlcy5jYW52YXMgfSBpZD1cImNhbnZhc1wiPjwvY2FudmFzPlxyXG5cclxuICAgICAgPG1haW4gY2xhc3NOYW1lPXtzdHlsZXMuYW5pbWF0ZX0+XHJcbiAgICAgICAgPGRpdj5cclxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzdHlsZXMubWFpbiArJyBoaWRkZW5faXQnfT57dGl0bGUobmFtZSAmJiBuYW1lWzBdKX08L2Rpdj5cclxuICAgICAgICAgIHsvKiA8cCBjbGFzc05hbWU9e3N0eWxlcy5kZXNjfT5cclxuICAgICAgICAgICAge21lc3NhZ2VzW3JhbmRvbU51bWJlcigwLCBtZXNzYWdlcy5sZW5ndGgpXS52YWx1ZX1cclxuICAgICAgICAgIDwvcD4gKi99XHJcbiAgICAgICAgPC9kaXY+XHJcblxyXG4gICAgICAgIDxFeGFtcGxlIC8+XHJcblxyXG4gICAgICAgIHsvKiA8ZGl2IGNsYXNzTmFtZT17c3R5bGVzLmJ1dHRvbkNvbnRhaW5lcn0+XHJcbiAgICAgICAgICB7aGlzdG9yeVswXSA9PSBcIi9cIiA/IDxDb3B5TGlua0J1dHRvbiAvPiA6IFwiXCJ9XHJcblxyXG4gICAgICAgICAge2hpc3RvcnlbMF0gPT0gXCIvXCIgPyAoXHJcbiAgICAgICAgICAgIDxCdXR0b25cclxuICAgICAgICAgICAgICBvbkNsaWNrPXsoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBzZXREb3dubG9hZGVkT25jZShmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICBzZXREb3dubG9hZGluZyh0cnVlKTtcclxuICAgICAgICAgICAgICB9fVxyXG4gICAgICAgICAgICAgIHRleHQ9XCJEb3dubG9hZCBhcyBJbWFnZVwiXHJcbiAgICAgICAgICAgIC8+XHJcbiAgICAgICAgICApIDogKFxyXG4gICAgICAgICAgICBcIlwiXHJcbiAgICAgICAgICApfVxyXG5cclxuICAgICAgICAgIDxCdXR0b25cclxuICAgICAgICAgICAgb25DbGljaz17KCkgPT4gcm91dGVyLnB1c2goXCIvXCIpfVxyXG4gICAgICAgICAgICB0ZXh0PVwiJmxhcnI7IENyZWF0ZSBhIHdpc2hcIlxyXG4gICAgICAgICAgLz5cclxuICAgICAgICA8L2Rpdj4gKi99XHJcbiAgICAgIDwvbWFpbj5cclxuICAgICAgPGF1ZGlvIHJlZj17YXVkaW9SZWZ9IGlkPVwicGxheWVyXCIgYXV0b1BsYXk+XHJcbiAgICAgICAgPHNvdXJjZSBzcmM9XCJtZWRpYS9sZWVtb25fdHJlZS5tcDNcIiAvPlxyXG4gICAgICA8L2F1ZGlvPlxyXG4gICAgPC9kaXY+XHJcbiAgKTtcclxufTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IFdpc2g7XHJcbiJdLCJuYW1lcyI6WyJSZWFjdCIsInVzZVN0YXRlIiwidXNlRWZmZWN0IiwidXNlUmVmIiwiSGVhZCIsInN0eWxlcyIsInVzZVJvdXRlciIsIkNvbmZldHRpR2VuZXJhdG9yIiwibWVzc2FnZXMiLCJ1c2VUaGVtZSIsImh0bWxUb0ltYWdlIiwiRmlsZVNhdmVyIiwiQ2Fyb3VzZWwiLCJFeGFtcGxlIiwid2lkdGgiLCJtYXJnaW4iLCJoZWlnaHQiLCJiYWNrZ3JvdW5kIiwiZm9udFNpemUiLCJvcGFjaXR5IiwiV2lzaCIsImhpc3RvcnkiLCJyb3V0ZXIiLCJuYW1lIiwicXVlcnkiLCJjb2xvciIsImRvd25sb2FkaW5nIiwic2V0RG93bmxvYWRpbmciLCJkb3dubG9hZGVkT25jZSIsInNldERvd25sb2FkZWRPbmNlIiwiYXVkaW9SZWYiLCJzZXRUaGVtZSIsImNvbmZldHRpU2V0dGluZ3MiLCJ0YXJnZXQiLCJzdGFydF9mcm9tX2VkZ2UiLCJjb25mZXR0aSIsInJlbmRlciIsImN1cnJlbnQiLCJwbGF5IiwiZG93bmxvYWRJbWFnZSIsInJhbmRvbU51bWJlciIsIm1pbiIsIm1heCIsIk1hdGgiLCJmbG9vciIsInJhbmRvbSIsIm5vZGUiLCJkb2N1bWVudCIsImdldEVsZW1lbnRCeUlkIiwidG9QbmciLCJ0aGVuIiwiYmxvYiIsInNhdmVBcyIsInRpdGxlIiwid2lzaCIsImJhc2VfbGV0dGVycyIsIm5hbWVfbGV0dGVycyIsImkiLCJsZW5ndGgiLCJsZXR0ZXIiLCJjaGFyQXQiLCJwdXNoIiwic3BhbiIsInRpdGxlSW1nIiwibWFwIiwiY29udGFpbmVySW1nIiwiaW1hZ2UiLCJtYWluIiwiZGVzY0ltZyIsInZhbHVlIiwiY29udGFpbmVyIiwiY2FudmFzIiwiYW5pbWF0ZSJdLCJzb3VyY2VSb290IjoiIn0=