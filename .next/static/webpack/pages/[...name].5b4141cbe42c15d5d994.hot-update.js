"use strict";
self["webpackHotUpdate_N_E"]("pages/[...name]",{

/***/ "./pages/[...name].js":
/*!****************************!*\
  !*** ./pages/[...name].js ***!
  \****************************/
/***/ (function(module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/head */ "./node_modules/next/head.js");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../styles/Name.module.css */ "./styles/Name.module.css");
/* harmony import */ var _styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default = /*#__PURE__*/__webpack_require__.n(_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/router */ "./node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var confetti_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! confetti-js */ "./node_modules/confetti-js/dist/index.es.js");
/* harmony import */ var _utils_birthdayWishes_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../utils/birthdayWishes.js */ "./utils/birthdayWishes.js");
/* harmony import */ var _hooks_useTheme__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../hooks/useTheme */ "./hooks/useTheme.js");
/* harmony import */ var html_to_image__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! html-to-image */ "./node_modules/html-to-image/es/index.js");
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! file-saver */ "./node_modules/file-saver/dist/FileSaver.min.js");
/* harmony import */ var file_saver__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(file_saver__WEBPACK_IMPORTED_MODULE_7__);
/* harmony import */ var react_responsive_carousel_lib_styles_carousel_min_css__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! react-responsive-carousel/lib/styles/carousel.min.css */ "./node_modules/react-responsive-carousel/lib/styles/carousel.min.css");
/* harmony import */ var react_responsive_carousel_lib_styles_carousel_min_css__WEBPACK_IMPORTED_MODULE_8___default = /*#__PURE__*/__webpack_require__.n(react_responsive_carousel_lib_styles_carousel_min_css__WEBPACK_IMPORTED_MODULE_8__);
/* harmony import */ var react_responsive_carousel__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! react-responsive-carousel */ "./node_modules/react-responsive-carousel/lib/js/index.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! react/jsx-dev-runtime */ "./node_modules/react/jsx-dev-runtime.js");
/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__);
/* module decorator */ module = __webpack_require__.hmd(module);
var _jsxFileName = "D:\\SangLM3\\APP\\fx_birthday_xuanqn\\pages\\[...name].js",
    _this = undefined,
    _s = $RefreshSig$();















var Example = function Example() {
  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
    children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
      style: {
        width: "800px",
        margin: "auto"
      },
      children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)(react_responsive_carousel__WEBPACK_IMPORTED_MODULE_9__.Carousel, {
        autoPlay: true,
        infiniteLoop: false,
        interval: 5000,
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("img", {
            src: "/h2.jpg",
            alt: "image1"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 19,
            columnNumber: 11
          }, _this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 18,
          columnNumber: 9
        }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("img", {
            src: "/h3.jpg",
            alt: "image2"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 23,
            columnNumber: 11
          }, _this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 22,
          columnNumber: 9
        }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("img", {
            src: "/h4.jpg",
            alt: "image3"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 28,
            columnNumber: 11
          }, _this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 27,
          columnNumber: 9
        }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("img", {
            src: "/h5.jpg",
            alt: "image4"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 32,
            columnNumber: 11
          }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("p", {
            style: {
              background: "#7FB77E",
              fontSize: "26px",
              opacity: "1"
            },
            className: "legend",
            children: "Lu\xF4n la\u0300 ng\u01B0\u01A1\u0300i anh ca\u0309 cu\u0309a chu\u0301ng em"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 33,
            columnNumber: 11
          }, _this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 31,
          columnNumber: 9
        }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("img", {
            src: "/h6.jpg",
            alt: "image5"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 41,
            columnNumber: 11
          }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("p", {
            style: {
              background: "#7FB77E",
              fontSize: "26px",
              opacity: "1"
            },
            className: "legend",
            children: "3 ph\xE2\u0300n y\xEAu th\u01B0\u01A1ng 7 ph\xE2\u0300n nu\xF4ng chi\xEA\u0300u chu\u0301ng em"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 42,
            columnNumber: 11
          }, _this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 40,
          columnNumber: 9
        }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("img", {
            src: "/h7.jpg",
            alt: "image6"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 50,
            columnNumber: 11
          }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("p", {
            style: {
              background: "#7FB77E",
              fontSize: "26px",
              opacity: "1"
            },
            className: "legend",
            children: "Thi thoa\u0309ng di\u0301 deadline chu\u0301ng em b\xE2\u0301t k\xEA\u0309 nga\u0300y \u0111\xEAm"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 51,
            columnNumber: 11
          }, _this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 49,
          columnNumber: 9
        }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("img", {
            src: "/h8.jpg",
            alt: "image7"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 59,
            columnNumber: 11
          }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("p", {
            style: {
              background: "#7FB77E",
              fontSize: "26px",
              opacity: "1"
            },
            className: "legend",
            children: ["Va\u0300 cu\xF4\u0301i cu\u0300ng la\u0300 \u0110a\u0303 y\xEAu th\u01B0\u01A1ng, ta\u0323o \u0111i\xEA\u0300u ki\xEA\u0323n cho chu\u0301ng em tr\u01B0\u01A1\u0309ng tha\u0300nh h\u01A1n", " "]
          }, void 0, true, {
            fileName: _jsxFileName,
            lineNumber: 60,
            columnNumber: 11
          }, _this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 58,
          columnNumber: 9
        }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("img", {
            src: "/h9.jpg",
            alt: "image8"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 69,
            columnNumber: 11
          }, _this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 68,
          columnNumber: 9
        }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("img", {
            src: "/h10.jpg",
            alt: "image9"
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 73,
            columnNumber: 11
          }, _this)
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 72,
          columnNumber: 9
        }, _this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 17,
        columnNumber: 7
      }, _this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 16,
      columnNumber: 5
    }, _this)
  }, void 0, false, {
    fileName: _jsxFileName,
    lineNumber: 14,
    columnNumber: 3
  }, _this);
};

_c = Example;

var Wish = function Wish(_ref) {
  _s();

  var history = _ref.history;
  var router = (0,next_router__WEBPACK_IMPORTED_MODULE_2__.useRouter)();
  var name = router.query.name; // gets both name & color id in form of array [name,colorId]

  var color = name ? name[1] : 0; //extracting colorId from name

  var _useState = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)(false),
      downloading = _useState[0],
      setDownloading = _useState[1];

  var _useState2 = (0,react__WEBPACK_IMPORTED_MODULE_0__.useState)(false),
      downloadedOnce = _useState2[0],
      setDownloadedOnce = _useState2[1];

  var audioRef = (0,react__WEBPACK_IMPORTED_MODULE_0__.useRef)();

  var _useTheme = (0,_hooks_useTheme__WEBPACK_IMPORTED_MODULE_5__.default)(),
      setTheme = _useTheme.setTheme;

  (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(function () {
    // Theme Change
    setTheme(color);

    if (downloading === false) {
      // Confetti
      var confettiSettings = {
        target: "canvas",
        start_from_edge: true
      };
      var confetti = new confetti_js__WEBPACK_IMPORTED_MODULE_3__.default(confettiSettings);
      confetti.render();
    }

    audioRef.current.play();
  }, [color, downloading]);
  (0,react__WEBPACK_IMPORTED_MODULE_0__.useEffect)(function () {
    if (downloading === true && downloadedOnce === false) {
      downloadImage();
    }
  }, [downloading, downloadedOnce]); // function for randomly picking the message from messages array

  var randomNumber = function randomNumber(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
  };

  var downloadImage = function downloadImage() {
    if (downloadedOnce === true) return;
    var node = document.getElementById("image");

    if (node) {
      setDownloadedOnce(true);
      html_to_image__WEBPACK_IMPORTED_MODULE_6__.toPng(node).then(function (blob) {
        file_saver__WEBPACK_IMPORTED_MODULE_7___default().saveAs(blob, "birthday-wish.png");
        setDownloading(false);
      });
    }
  };

  var title = function title(name) {
    var wish = "Happy Birthday " + name + "!";
    var base_letters = [];
    var name_letters = [];

    for (var i = 0; i < wish.length; i++) {
      if (i < 15) {
        var letter = wish.charAt(i);
        base_letters.push( /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("span", {
          style: {
            "--i": i + 1
          },
          children: letter
        }, i, false, {
          fileName: _jsxFileName,
          lineNumber: 145,
          columnNumber: 11
        }, _this));
      } else {
        var _letter = wish.charAt(i);

        name_letters.push( /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("span", {
          style: {
            "--i": i + 1
          },
          className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().span),
          children: _letter
        }, i, false, {
          fileName: _jsxFileName,
          lineNumber: 152,
          columnNumber: 11
        }, _this));
      }
    }

    return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.Fragment, {
      children: downloading ? /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("h1", {
        className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().titleImg),
        style: {
          "--wish-length": wish.length
        },
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: base_letters.map(function (letter) {
            return letter;
          })
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 166,
          columnNumber: 13
        }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: name_letters.map(function (letter) {
            return letter;
          })
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 167,
          columnNumber: 13
        }, _this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 162,
        columnNumber: 11
      }, _this) : /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("h1", {
        className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().title),
        style: {
          "--wish-length": wish.length
        },
        children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: base_letters.map(function (letter) {
            return letter;
          })
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 171,
          columnNumber: 13
        }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: name_letters.map(function (letter) {
            return letter;
          })
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 172,
          columnNumber: 13
        }, _this)]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 170,
        columnNumber: 11
      }, _this)
    }, void 0, false);
  };

  if (downloading) {
    return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
      className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().containerImg),
      id: "image",
      onClick: downloadImage,
      children: [downloadImage(), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("main", {
        className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().image),
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
            className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().main),
            children: title(name && name[0])
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 185,
            columnNumber: 13
          }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
            style: {
              height: 40
            }
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 187,
            columnNumber: 13
          }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("p", {
            className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().descImg),
            children: _utils_birthdayWishes_js__WEBPACK_IMPORTED_MODULE_4__.default[randomNumber(0, _utils_birthdayWishes_js__WEBPACK_IMPORTED_MODULE_4__.default.length)].value
          }, void 0, false, {
            fileName: _jsxFileName,
            lineNumber: 189,
            columnNumber: 13
          }, _this)]
        }, void 0, true, {
          fileName: _jsxFileName,
          lineNumber: 184,
          columnNumber: 11
        }, _this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 183,
        columnNumber: 9
      }, _this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 181,
      columnNumber: 7
    }, _this);
  }

  return /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
    className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().container),
    children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)((next_head__WEBPACK_IMPORTED_MODULE_1___default()), {
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("title", {
        children: ["Happy Birthday ", name && name[0]]
      }, void 0, true, {
        fileName: _jsxFileName,
        lineNumber: 201,
        columnNumber: 9
      }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("meta", {
        name: "description",
        content: "Generated by create next app"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 202,
        columnNumber: 9
      }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("link", {
        rel: "icon",
        href: "/favicon.ico"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 203,
        columnNumber: 9
      }, _this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 200,
      columnNumber: 7
    }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("canvas", {
      className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().canvas) + ' hidden_it',
      id: "canvas"
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 206,
      columnNumber: 7
    }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("main", {
      className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().animate),
      children: [/*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
        children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("div", {
          className: (_styles_Name_module_css__WEBPACK_IMPORTED_MODULE_11___default().main),
          children: title(name && name[0])
        }, void 0, false, {
          fileName: _jsxFileName,
          lineNumber: 210,
          columnNumber: 11
        }, _this)
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 209,
        columnNumber: 9
      }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)(Example, {}, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 216,
        columnNumber: 9
      }, _this)]
    }, void 0, true, {
      fileName: _jsxFileName,
      lineNumber: 208,
      columnNumber: 7
    }, _this), /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("audio", {
      ref: audioRef,
      id: "player",
      autoPlay: true,
      children: /*#__PURE__*/(0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_10__.jsxDEV)("source", {
        src: "media/leemon_tree.mp3"
      }, void 0, false, {
        fileName: _jsxFileName,
        lineNumber: 240,
        columnNumber: 9
      }, _this)
    }, void 0, false, {
      fileName: _jsxFileName,
      lineNumber: 239,
      columnNumber: 7
    }, _this)]
  }, void 0, true, {
    fileName: _jsxFileName,
    lineNumber: 199,
    columnNumber: 5
  }, _this);
};

_s(Wish, "KC1aZNv21wLD2inhQfaHb0yrDYI=", false, function () {
  return [next_router__WEBPACK_IMPORTED_MODULE_2__.useRouter, _hooks_useTheme__WEBPACK_IMPORTED_MODULE_5__.default];
});

_c2 = Wish;
/* harmony default export */ __webpack_exports__["default"] = (Wish);

var _c, _c2;

$RefreshReg$(_c, "Example");
$RefreshReg$(_c2, "Wish");

;
    var _a, _b;
    // Legacy CSS implementations will `eval` browser code in a Node.js context
    // to extract CSS. For backwards compatibility, we need to check we're in a
    // browser context before continuing.
    if (typeof self !== 'undefined' &&
        // AMP / No-JS mode does not inject these helpers:
        '$RefreshHelpers$' in self) {
        var currentExports = module.__proto__.exports;
        var prevExports = (_b = (_a = module.hot.data) === null || _a === void 0 ? void 0 : _a.prevExports) !== null && _b !== void 0 ? _b : null;
        // This cannot happen in MainTemplate because the exports mismatch between
        // templating and execution.
        self.$RefreshHelpers$.registerExportsForReactRefresh(currentExports, module.id);
        // A module can be accepted automatically based on its exports, e.g. when
        // it is a Refresh Boundary.
        if (self.$RefreshHelpers$.isReactRefreshBoundary(currentExports)) {
            // Save the previous exports on update so we can compare the boundary
            // signatures.
            module.hot.dispose(function (data) {
                data.prevExports = currentExports;
            });
            // Unconditionally accept an update to this module, we'll check if it's
            // still a Refresh Boundary later.
            module.hot.accept();
            // This field is set when the previous version of this module was a
            // Refresh Boundary, letting us know we need to check for invalidation or
            // enqueue an update.
            if (prevExports !== null) {
                // A boundary can become ineligible if its exports are incompatible
                // with the previous exports.
                //
                // For example, if you add/remove/change exports, we'll want to
                // re-execute the importing modules, and force those components to
                // re-render. Similarly, if you convert a class component to a
                // function, we want to invalidate the boundary.
                if (self.$RefreshHelpers$.shouldInvalidateReactRefreshBoundary(prevExports, currentExports)) {
                    module.hot.invalidate();
                }
                else {
                    self.$RefreshHelpers$.scheduleUpdate();
                }
            }
        }
        else {
            // Since we just executed the code for the module, it's possible that the
            // new exports made it ineligible for being a boundary.
            // We only care about the case when we were _previously_ a boundary,
            // because we already accepted this update (accidental side effect).
            var isNoLongerABoundary = prevExports !== null;
            if (isNoLongerABoundary) {
                module.hot.invalidate();
            }
        }
    }


/***/ })

});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RhdGljL3dlYnBhY2svcGFnZXMvWy4uLm5hbWVdLjViNDE0MWNiZTQyYzE1ZDVkOTk0LmhvdC11cGRhdGUuanMiLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUFFQSxJQUFNYSxPQUFPLEdBQUcsU0FBVkEsT0FBVTtBQUFBLHNCQUNkO0FBQUEsMkJBRUU7QUFBSyxXQUFLLEVBQUU7QUFBRUMsUUFBQUEsS0FBSyxFQUFFLE9BQVQ7QUFBa0JDLFFBQUFBLE1BQU0sRUFBRTtBQUExQixPQUFaO0FBQUEsNkJBQ0UsK0RBQUMsK0RBQUQ7QUFBVSxnQkFBUSxFQUFFLElBQXBCO0FBQTBCLG9CQUFZLEVBQUUsS0FBeEM7QUFBK0MsZ0JBQVEsRUFBRSxJQUF6RDtBQUFBLGdDQUNFO0FBQUEsaUNBQ0U7QUFBSyxlQUFHLEVBQUMsU0FBVDtBQUFtQixlQUFHLEVBQUM7QUFBdkI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBREYsZUFLRTtBQUFBLGlDQUNFO0FBQUssZUFBRyxFQUFDLFNBQVQ7QUFBbUIsZUFBRyxFQUFDO0FBQXZCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUxGLGVBVUU7QUFBQSxpQ0FDRTtBQUFLLGVBQUcsRUFBQyxTQUFUO0FBQW1CLGVBQUcsRUFBQztBQUF2QjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFWRixlQWNFO0FBQUEsa0NBQ0U7QUFBSyxlQUFHLEVBQUMsU0FBVDtBQUFtQixlQUFHLEVBQUM7QUFBdkI7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFERixlQUVFO0FBQ0UsaUJBQUssRUFBRTtBQUFFQyxjQUFBQSxVQUFVLEVBQUUsU0FBZDtBQUF5QkMsY0FBQUEsUUFBUSxFQUFFLE1BQW5DO0FBQTJDQyxjQUFBQSxPQUFPLEVBQUU7QUFBcEQsYUFEVDtBQUVFLHFCQUFTLEVBQUMsUUFGWjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBZEYsZUF1QkU7QUFBQSxrQ0FDRTtBQUFLLGVBQUcsRUFBQyxTQUFUO0FBQW1CLGVBQUcsRUFBQztBQUF2QjtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQURGLGVBRUU7QUFDRSxpQkFBSyxFQUFFO0FBQUVGLGNBQUFBLFVBQVUsRUFBRSxTQUFkO0FBQXlCQyxjQUFBQSxRQUFRLEVBQUUsTUFBbkM7QUFBMkNDLGNBQUFBLE9BQU8sRUFBRTtBQUFwRCxhQURUO0FBRUUscUJBQVMsRUFBQyxRQUZaO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkF2QkYsZUFnQ0U7QUFBQSxrQ0FDRTtBQUFLLGVBQUcsRUFBQyxTQUFUO0FBQW1CLGVBQUcsRUFBQztBQUF2QjtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQURGLGVBRUU7QUFDRSxpQkFBSyxFQUFFO0FBQUVGLGNBQUFBLFVBQVUsRUFBRSxTQUFkO0FBQXlCQyxjQUFBQSxRQUFRLEVBQUUsTUFBbkM7QUFBMkNDLGNBQUFBLE9BQU8sRUFBRTtBQUFwRCxhQURUO0FBRUUscUJBQVMsRUFBQyxRQUZaO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQUZGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFoQ0YsZUF5Q0U7QUFBQSxrQ0FDRTtBQUFLLGVBQUcsRUFBQyxTQUFUO0FBQW1CLGVBQUcsRUFBQztBQUF2QjtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQURGLGVBRUU7QUFDRSxpQkFBSyxFQUFFO0FBQUVGLGNBQUFBLFVBQVUsRUFBRSxTQUFkO0FBQXlCQyxjQUFBQSxRQUFRLEVBQUUsTUFBbkM7QUFBMkNDLGNBQUFBLE9BQU8sRUFBRTtBQUFwRCxhQURUO0FBRUUscUJBQVMsRUFBQyxRQUZaO0FBQUEsc05BS2EsR0FMYjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsbUJBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQXpDRixlQW1ERTtBQUFBLGlDQUNFO0FBQUssZUFBRyxFQUFDLFNBQVQ7QUFBbUIsZUFBRyxFQUFDO0FBQXZCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQW5ERixlQXVERTtBQUFBLGlDQUNFO0FBQUssZUFBRyxFQUFDLFVBQVQ7QUFBb0IsZUFBRyxFQUFDO0FBQXhCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQXZERjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBRkY7QUFBQTtBQUFBO0FBQUE7QUFBQSxXQURjO0FBQUEsQ0FBaEI7O0tBQU1MOztBQXNFTixJQUFNTSxJQUFJLEdBQUcsU0FBUEEsSUFBTyxPQUFpQjtBQUFBOztBQUFBLE1BQWRDLE9BQWMsUUFBZEEsT0FBYztBQUM1QixNQUFNQyxNQUFNLEdBQUdmLHNEQUFTLEVBQXhCO0FBQ0EsTUFBUWdCLElBQVIsR0FBaUJELE1BQU0sQ0FBQ0UsS0FBeEIsQ0FBUUQsSUFBUixDQUY0QixDQUVHOztBQUMvQixNQUFNRSxLQUFLLEdBQUdGLElBQUksR0FBR0EsSUFBSSxDQUFDLENBQUQsQ0FBUCxHQUFhLENBQS9CLENBSDRCLENBR007O0FBQ2xDLGtCQUFzQ3JCLCtDQUFRLENBQUMsS0FBRCxDQUE5QztBQUFBLE1BQU93QixXQUFQO0FBQUEsTUFBb0JDLGNBQXBCOztBQUNBLG1CQUE0Q3pCLCtDQUFRLENBQUMsS0FBRCxDQUFwRDtBQUFBLE1BQU8wQixjQUFQO0FBQUEsTUFBdUJDLGlCQUF2Qjs7QUFDQSxNQUFNQyxRQUFRLEdBQUcxQiw2Q0FBTSxFQUF2Qjs7QUFFQSxrQkFBcUJNLHdEQUFRLEVBQTdCO0FBQUEsTUFBUXFCLFFBQVIsYUFBUUEsUUFBUjs7QUFFQTVCLEVBQUFBLGdEQUFTLENBQUMsWUFBTTtBQUNkO0FBQ0E0QixJQUFBQSxRQUFRLENBQUNOLEtBQUQsQ0FBUjs7QUFFQSxRQUFJQyxXQUFXLEtBQUssS0FBcEIsRUFBMkI7QUFDekI7QUFDQSxVQUFNTSxnQkFBZ0IsR0FBRztBQUN2QkMsUUFBQUEsTUFBTSxFQUFFLFFBRGU7QUFFdkJDLFFBQUFBLGVBQWUsRUFBRTtBQUZNLE9BQXpCO0FBSUEsVUFBTUMsUUFBUSxHQUFHLElBQUkzQixnREFBSixDQUFzQndCLGdCQUF0QixDQUFqQjtBQUNBRyxNQUFBQSxRQUFRLENBQUNDLE1BQVQ7QUFDRDs7QUFFRE4sSUFBQUEsUUFBUSxDQUFDTyxPQUFULENBQWlCQyxJQUFqQjtBQUNELEdBZlEsRUFlTixDQUFDYixLQUFELEVBQVFDLFdBQVIsQ0FmTSxDQUFUO0FBaUJBdkIsRUFBQUEsZ0RBQVMsQ0FBQyxZQUFNO0FBQ2QsUUFBSXVCLFdBQVcsS0FBSyxJQUFoQixJQUF3QkUsY0FBYyxLQUFLLEtBQS9DLEVBQXNEO0FBQ3BEVyxNQUFBQSxhQUFhO0FBQ2Q7QUFDRixHQUpRLEVBSU4sQ0FBQ2IsV0FBRCxFQUFjRSxjQUFkLENBSk0sQ0FBVCxDQTNCNEIsQ0FpQzVCOztBQUNBLE1BQU1ZLFlBQVksR0FBRyxTQUFmQSxZQUFlLENBQUNDLEdBQUQsRUFBTUMsR0FBTixFQUFjO0FBQ2pDLFdBQU9DLElBQUksQ0FBQ0MsS0FBTCxDQUFXRCxJQUFJLENBQUNFLE1BQUwsTUFBaUJILEdBQUcsR0FBR0QsR0FBdkIsQ0FBWCxJQUEwQ0EsR0FBakQ7QUFDRCxHQUZEOztBQUlBLE1BQU1GLGFBQWEsR0FBRyxTQUFoQkEsYUFBZ0IsR0FBTTtBQUMxQixRQUFJWCxjQUFjLEtBQUssSUFBdkIsRUFBNkI7QUFFN0IsUUFBTWtCLElBQUksR0FBR0MsUUFBUSxDQUFDQyxjQUFULENBQXdCLE9BQXhCLENBQWI7O0FBRUEsUUFBSUYsSUFBSixFQUFVO0FBQ1JqQixNQUFBQSxpQkFBaUIsQ0FBQyxJQUFELENBQWpCO0FBRUFsQixNQUFBQSxnREFBQSxDQUFrQm1DLElBQWxCLEVBQXdCSSxJQUF4QixDQUE2QixVQUFDQyxJQUFELEVBQVU7QUFDckN2QyxRQUFBQSx3REFBQSxDQUFpQnVDLElBQWpCLEVBQXVCLG1CQUF2QjtBQUNBeEIsUUFBQUEsY0FBYyxDQUFDLEtBQUQsQ0FBZDtBQUNELE9BSEQ7QUFJRDtBQUNGLEdBYkQ7O0FBZUEsTUFBTTBCLEtBQUssR0FBRyxTQUFSQSxLQUFRLENBQUM5QixJQUFELEVBQVU7QUFDdEIsUUFBTStCLElBQUksR0FBRyxvQkFBb0IvQixJQUFwQixHQUEyQixHQUF4QztBQUNBLFFBQU1nQyxZQUFZLEdBQUcsRUFBckI7QUFDQSxRQUFNQyxZQUFZLEdBQUcsRUFBckI7O0FBRUEsU0FBSyxJQUFJQyxDQUFDLEdBQUcsQ0FBYixFQUFnQkEsQ0FBQyxHQUFHSCxJQUFJLENBQUNJLE1BQXpCLEVBQWlDRCxDQUFDLEVBQWxDLEVBQXNDO0FBQ3BDLFVBQUlBLENBQUMsR0FBRyxFQUFSLEVBQVk7QUFDVixZQUFNRSxNQUFNLEdBQUdMLElBQUksQ0FBQ00sTUFBTCxDQUFZSCxDQUFaLENBQWY7QUFDQUYsUUFBQUEsWUFBWSxDQUFDTSxJQUFiLGVBQ0U7QUFBYyxlQUFLLEVBQUU7QUFBRSxtQkFBT0osQ0FBQyxHQUFHO0FBQWIsV0FBckI7QUFBQSxvQkFDR0U7QUFESCxXQUFXRixDQUFYO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBREY7QUFLRCxPQVBELE1BT087QUFDTCxZQUFNRSxPQUFNLEdBQUdMLElBQUksQ0FBQ00sTUFBTCxDQUFZSCxDQUFaLENBQWY7O0FBQ0FELFFBQUFBLFlBQVksQ0FBQ0ssSUFBYixlQUNFO0FBQWMsZUFBSyxFQUFFO0FBQUUsbUJBQU9KLENBQUMsR0FBRztBQUFiLFdBQXJCO0FBQXVDLG1CQUFTLEVBQUVuRCxzRUFBbEQ7QUFBQSxvQkFDR3FEO0FBREgsV0FBV0YsQ0FBWDtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQURGO0FBS0Q7QUFDRjs7QUFFRCx3QkFDRTtBQUFBLGdCQUNHL0IsV0FBVyxnQkFDVjtBQUNFLGlCQUFTLEVBQUVwQiwwRUFEYjtBQUVFLGFBQUssRUFBRTtBQUFFLDJCQUFpQmdELElBQUksQ0FBQ0k7QUFBeEIsU0FGVDtBQUFBLGdDQUlFO0FBQUEsb0JBQU1ILFlBQVksQ0FBQ1MsR0FBYixDQUFpQixVQUFDTCxNQUFEO0FBQUEsbUJBQVlBLE1BQVo7QUFBQSxXQUFqQjtBQUFOO0FBQUE7QUFBQTtBQUFBO0FBQUEsaUJBSkYsZUFLRTtBQUFBLG9CQUFNSCxZQUFZLENBQUNRLEdBQWIsQ0FBaUIsVUFBQ0wsTUFBRDtBQUFBLG1CQUFZQSxNQUFaO0FBQUEsV0FBakI7QUFBTjtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQUxGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQURVLGdCQVNWO0FBQUksaUJBQVMsRUFBRXJELHVFQUFmO0FBQTZCLGFBQUssRUFBRTtBQUFFLDJCQUFpQmdELElBQUksQ0FBQ0k7QUFBeEIsU0FBcEM7QUFBQSxnQ0FDRTtBQUFBLG9CQUFNSCxZQUFZLENBQUNTLEdBQWIsQ0FBaUIsVUFBQ0wsTUFBRDtBQUFBLG1CQUFZQSxNQUFaO0FBQUEsV0FBakI7QUFBTjtBQUFBO0FBQUE7QUFBQTtBQUFBLGlCQURGLGVBRUU7QUFBQSxvQkFBTUgsWUFBWSxDQUFDUSxHQUFiLENBQWlCLFVBQUNMLE1BQUQ7QUFBQSxtQkFBWUEsTUFBWjtBQUFBLFdBQWpCO0FBQU47QUFBQTtBQUFBO0FBQUE7QUFBQSxpQkFGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFWSixxQkFERjtBQWtCRCxHQXpDRDs7QUEyQ0EsTUFBSWpDLFdBQUosRUFBaUI7QUFDZix3QkFDRTtBQUFLLGVBQVMsRUFBRXBCLDhFQUFoQjtBQUFxQyxRQUFFLEVBQUMsT0FBeEM7QUFBZ0QsYUFBTyxFQUFFaUMsYUFBekQ7QUFBQSxpQkFDR0EsYUFBYSxFQURoQixlQUVFO0FBQU0saUJBQVMsRUFBRWpDLHVFQUFqQjtBQUFBLCtCQUNFO0FBQUEsa0NBQ0U7QUFBSyxxQkFBUyxFQUFFQSxzRUFBaEI7QUFBQSxzQkFBOEIrQyxLQUFLLENBQUM5QixJQUFJLElBQUlBLElBQUksQ0FBQyxDQUFELENBQWI7QUFBbkM7QUFBQTtBQUFBO0FBQUE7QUFBQSxtQkFERixlQUdFO0FBQUssaUJBQUssRUFBRTtBQUFFNkMsY0FBQUEsTUFBTSxFQUFFO0FBQVY7QUFBWjtBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQUhGLGVBS0U7QUFBRyxxQkFBUyxFQUFFOUQseUVBQWQ7QUFBQSxzQkFDR0csNkRBQVEsQ0FBQytCLFlBQVksQ0FBQyxDQUFELEVBQUkvQixvRUFBSixDQUFiLENBQVIsQ0FBMkM2RDtBQUQ5QztBQUFBO0FBQUE7QUFBQTtBQUFBLG1CQUxGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQURGO0FBQUE7QUFBQTtBQUFBO0FBQUEsZUFGRjtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUEsYUFERjtBQWdCRDs7QUFFRCxzQkFDRTtBQUFLLGFBQVMsRUFBRWhFLDJFQUFoQjtBQUFBLDRCQUNFLCtEQUFDLGtEQUFEO0FBQUEsOEJBQ0U7QUFBQSxzQ0FBdUJpQixJQUFJLElBQUlBLElBQUksQ0FBQyxDQUFELENBQW5DO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQURGLGVBRUU7QUFBTSxZQUFJLEVBQUMsYUFBWDtBQUF5QixlQUFPLEVBQUM7QUFBakM7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQUZGLGVBR0U7QUFBTSxXQUFHLEVBQUMsTUFBVjtBQUFpQixZQUFJLEVBQUM7QUFBdEI7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQUhGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxhQURGLGVBT0U7QUFBUSxlQUFTLEVBQUVqQix3RUFBQSxHQUFlLFlBQWxDO0FBQWdELFFBQUUsRUFBQztBQUFuRDtBQUFBO0FBQUE7QUFBQTtBQUFBLGFBUEYsZUFTRTtBQUFNLGVBQVMsRUFBRUEseUVBQWpCO0FBQUEsOEJBQ0U7QUFBQSwrQkFDRTtBQUFLLG1CQUFTLEVBQUVBLHNFQUFoQjtBQUFBLG9CQUE4QitDLEtBQUssQ0FBQzlCLElBQUksSUFBSUEsSUFBSSxDQUFDLENBQUQsQ0FBYjtBQUFuQztBQUFBO0FBQUE7QUFBQTtBQUFBO0FBREY7QUFBQTtBQUFBO0FBQUE7QUFBQSxlQURGLGVBUUUsK0RBQUMsT0FBRDtBQUFBO0FBQUE7QUFBQTtBQUFBLGVBUkY7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBLGFBVEYsZUF3Q0U7QUFBTyxTQUFHLEVBQUVPLFFBQVo7QUFBc0IsUUFBRSxFQUFDLFFBQXpCO0FBQWtDLGNBQVEsTUFBMUM7QUFBQSw2QkFDRTtBQUFRLFdBQUcsRUFBQztBQUFaO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFERjtBQUFBO0FBQUE7QUFBQTtBQUFBLGFBeENGO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxXQURGO0FBOENELENBaktEOztHQUFNVjtVQUNXYixvREFPTUc7OztNQVJqQlU7QUFtS04sK0RBQWVBLElBQWYiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly9fTl9FLy4vcGFnZXMvWy4uLm5hbWVdLmpzIl0sInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCwgeyB1c2VTdGF0ZSwgdXNlRWZmZWN0LCB1c2VSZWYgfSBmcm9tIFwicmVhY3RcIjtcclxuaW1wb3J0IEhlYWQgZnJvbSBcIm5leHQvaGVhZFwiO1xyXG5pbXBvcnQgc3R5bGVzIGZyb20gXCIuLi9zdHlsZXMvTmFtZS5tb2R1bGUuY3NzXCI7XHJcbmltcG9ydCB7IHVzZVJvdXRlciB9IGZyb20gXCJuZXh0L3JvdXRlclwiO1xyXG5pbXBvcnQgQ29uZmV0dGlHZW5lcmF0b3IgZnJvbSBcImNvbmZldHRpLWpzXCI7XHJcbmltcG9ydCBtZXNzYWdlcyBmcm9tIFwiLi4vdXRpbHMvYmlydGhkYXlXaXNoZXMuanNcIjtcclxuaW1wb3J0IHVzZVRoZW1lIGZyb20gXCIuLi9ob29rcy91c2VUaGVtZVwiO1xyXG5pbXBvcnQgKiBhcyBodG1sVG9JbWFnZSBmcm9tIFwiaHRtbC10by1pbWFnZVwiO1xyXG5pbXBvcnQgRmlsZVNhdmVyIGZyb20gXCJmaWxlLXNhdmVyXCI7XHJcbmltcG9ydCBcInJlYWN0LXJlc3BvbnNpdmUtY2Fyb3VzZWwvbGliL3N0eWxlcy9jYXJvdXNlbC5taW4uY3NzXCI7XHJcbmltcG9ydCB7IENhcm91c2VsIH0gZnJvbSBcInJlYWN0LXJlc3BvbnNpdmUtY2Fyb3VzZWxcIjtcclxuXHJcbmNvbnN0IEV4YW1wbGUgPSAoKSA9PiAoXHJcbiAgPGRpdj5cclxuICAgIHsvKiA8aDI+Q2jDumMgYW5oIG5nw6B5IHNpbmggbmjhuq10IHRo4bqtdCB2dWkgduG6uywgbHXDtG4gbeG6oW5oIGtob+G6uywgZ+G6t3Qgbmhp4buBdSB0aMOgbmggY8O0bmcgaMahbiBu4buvYSBhbmggbmjDqSE8L2gyPiAqL31cclxuICAgIDxkaXYgc3R5bGU9e3sgd2lkdGg6IFwiODAwcHhcIiwgbWFyZ2luOiBcImF1dG9cIiB9fT5cclxuICAgICAgPENhcm91c2VsIGF1dG9QbGF5PXt0cnVlfSBpbmZpbml0ZUxvb3A9e2ZhbHNlfSBpbnRlcnZhbD17NTAwMH0+XHJcbiAgICAgICAgPGRpdj5cclxuICAgICAgICAgIDxpbWcgc3JjPVwiL2gyLmpwZ1wiIGFsdD1cImltYWdlMVwiIC8+XHJcbiAgICAgICAgICB7LyogPHAgc3R5bGU9e3tiYWNrZ3JvdW5kOlwiIzdGQjc3RVwiLCBmb250U2l6ZTpcIjI2cHhcIiwgb3BhY2l0eTpcIjFcIn19IGNsYXNzTmFtZT1cImxlZ2VuZFwiPjwvcD4gKi99XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPGRpdj5cclxuICAgICAgICAgIDxpbWcgc3JjPVwiL2gzLmpwZ1wiIGFsdD1cImltYWdlMlwiIC8+XHJcbiAgICAgICAgICB7LyogPHAgc3R5bGU9e3tiYWNrZ3JvdW5kOlwiIzdGQjc3RVwiLCBmb250U2l6ZTpcIjI2cHhcIiwgb3BhY2l0eTpcIjFcIn19IGNsYXNzTmFtZT1cImxlZ2VuZFwiPkPhuqNtIMahbiBhbmggdGjhuq10IG5oaeG7gXUgdsOsLi4uPC9wPiAqL31cclxuICAgICAgICA8L2Rpdj5cclxuXHJcbiAgICAgICAgPGRpdj5cclxuICAgICAgICAgIDxpbWcgc3JjPVwiL2g0LmpwZ1wiIGFsdD1cImltYWdlM1wiIC8+XHJcbiAgICAgICAgICB7LyogPHAgc3R5bGU9e3tiYWNrZ3JvdW5kOlwiIzdGQjc3RVwiLCBmb250U2l6ZTpcIjI2cHhcIiwgb3BhY2l0eTpcIjFcIn19IGNsYXNzTmFtZT1cImxlZ2VuZFwiPkPhuqNtIMahbiBhbmggdGjhuq10IG5oaeG7gXUgdsOsLi4uPC9wPiAqL31cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgPGltZyBzcmM9XCIvaDUuanBnXCIgYWx0PVwiaW1hZ2U0XCIgLz5cclxuICAgICAgICAgIDxwXHJcbiAgICAgICAgICAgIHN0eWxlPXt7IGJhY2tncm91bmQ6IFwiIzdGQjc3RVwiLCBmb250U2l6ZTogXCIyNnB4XCIsIG9wYWNpdHk6IFwiMVwiIH19XHJcbiAgICAgICAgICAgIGNsYXNzTmFtZT1cImxlZ2VuZFwiXHJcbiAgICAgICAgICA+XHJcbiAgICAgICAgICAgIEx1w7RuIGxhzIAgbmfGsMahzIBpIGFuaCBjYcyJIGN1zIlhIGNodcyBbmcgZW1cclxuICAgICAgICAgIDwvcD5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgPGltZyBzcmM9XCIvaDYuanBnXCIgYWx0PVwiaW1hZ2U1XCIgLz5cclxuICAgICAgICAgIDxwXHJcbiAgICAgICAgICAgIHN0eWxlPXt7IGJhY2tncm91bmQ6IFwiIzdGQjc3RVwiLCBmb250U2l6ZTogXCIyNnB4XCIsIG9wYWNpdHk6IFwiMVwiIH19XHJcbiAgICAgICAgICAgIGNsYXNzTmFtZT1cImxlZ2VuZFwiXHJcbiAgICAgICAgICA+XHJcbiAgICAgICAgICAgIDMgcGjDosyAbiB5w6p1IHRoxrDGoW5nIDcgcGjDosyAbiBudcO0bmcgY2hpw6rMgHUgY2h1zIFuZyBlbVxyXG4gICAgICAgICAgPC9wPlxyXG4gICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDxkaXY+XHJcbiAgICAgICAgICA8aW1nIHNyYz1cIi9oNy5qcGdcIiBhbHQ9XCJpbWFnZTZcIiAvPlxyXG4gICAgICAgICAgPHBcclxuICAgICAgICAgICAgc3R5bGU9e3sgYmFja2dyb3VuZDogXCIjN0ZCNzdFXCIsIGZvbnRTaXplOiBcIjI2cHhcIiwgb3BhY2l0eTogXCIxXCIgfX1cclxuICAgICAgICAgICAgY2xhc3NOYW1lPVwibGVnZW5kXCJcclxuICAgICAgICAgID5cclxuICAgICAgICAgICAgVGhpIHRob2HMiW5nIGRpzIEgZGVhZGxpbmUgY2h1zIFuZyBlbSBiw6LMgXQga8OqzIkgbmdhzIB5IMSRw6ptXHJcbiAgICAgICAgICA8L3A+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPGRpdj5cclxuICAgICAgICAgIDxpbWcgc3JjPVwiL2g4LmpwZ1wiIGFsdD1cImltYWdlN1wiIC8+XHJcbiAgICAgICAgICA8cFxyXG4gICAgICAgICAgICBzdHlsZT17eyBiYWNrZ3JvdW5kOiBcIiM3RkI3N0VcIiwgZm9udFNpemU6IFwiMjZweFwiLCBvcGFjaXR5OiBcIjFcIiB9fVxyXG4gICAgICAgICAgICBjbGFzc05hbWU9XCJsZWdlbmRcIlxyXG4gICAgICAgICAgPlxyXG4gICAgICAgICAgICBWYcyAIGN1w7TMgWkgY3XMgG5nIGxhzIAgxJBhzIMgecOqdSB0aMawxqFuZywgdGHMo28gxJFpw6rMgHUga2nDqsyjbiBjaG8gY2h1zIFuZyBlbSB0csawxqHMiW5nXHJcbiAgICAgICAgICAgIHRoYcyAbmggaMahbntcIiBcIn1cclxuICAgICAgICAgIDwvcD5cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8ZGl2PlxyXG4gICAgICAgICAgPGltZyBzcmM9XCIvaDkuanBnXCIgYWx0PVwiaW1hZ2U4XCIgLz5cclxuICAgICAgICAgIHsvKiA8cCBzdHlsZT17e2JhY2tncm91bmQ6XCIjN0ZCNzdFXCIsIGZvbnRTaXplOlwiMjZweFwiLCBvcGFjaXR5OlwiMVwifX0gY2xhc3NOYW1lPVwibGVnZW5kXCI+VmHMgCBjdcO0zIFpIGN1zIBuZyBsYcyAICDEkGHMgyB5w6p1IHRoxrDGoW5nLCB0YcyjbyDEkWnDqsyAdSBracOqzKNuIGNobyBjaHXMgW5nIGVtIHRyxrDGocyJbmcgdGhhzIBuaCBoxqFuIDwvcD4gKi99XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPGRpdj5cclxuICAgICAgICAgIDxpbWcgc3JjPVwiL2gxMC5qcGdcIiBhbHQ9XCJpbWFnZTlcIiAvPlxyXG4gICAgICAgICAgey8qIDxwIHN0eWxlPXt7YmFja2dyb3VuZDpcIiM3RkI3N0VcIiwgZm9udFNpemU6XCIyNnB4XCIsIG9wYWNpdHk6XCIxXCJ9fSBjbGFzc05hbWU9XCJsZWdlbmRcIj5WYcyAIGN1w7TMgWkgY3XMgG5nIGxhzIAgIMSQYcyDIHnDqnUgdGjGsMahbmcsIHRhzKNvIMSRacOqzIB1IGtpw6rMo24gY2hvIGNodcyBbmcgZW0gdHLGsMahzIluZyB0aGHMgG5oIGjGoW4gPC9wPiAqL31cclxuICAgICAgICA8L2Rpdj5cclxuICAgICAgPC9DYXJvdXNlbD5cclxuICAgIDwvZGl2PlxyXG4gIDwvZGl2PlxyXG4pO1xyXG5cclxuXHJcblxyXG5jb25zdCBXaXNoID0gKHsgaGlzdG9yeSB9KSA9PiB7XHJcbiAgY29uc3Qgcm91dGVyID0gdXNlUm91dGVyKCk7XHJcbiAgY29uc3QgeyBuYW1lIH0gPSByb3V0ZXIucXVlcnk7IC8vIGdldHMgYm90aCBuYW1lICYgY29sb3IgaWQgaW4gZm9ybSBvZiBhcnJheSBbbmFtZSxjb2xvcklkXVxyXG4gIGNvbnN0IGNvbG9yID0gbmFtZSA/IG5hbWVbMV0gOiAwOyAvL2V4dHJhY3RpbmcgY29sb3JJZCBmcm9tIG5hbWVcclxuICBjb25zdCBbZG93bmxvYWRpbmcsIHNldERvd25sb2FkaW5nXSA9IHVzZVN0YXRlKGZhbHNlKTtcclxuICBjb25zdCBbZG93bmxvYWRlZE9uY2UsIHNldERvd25sb2FkZWRPbmNlXSA9IHVzZVN0YXRlKGZhbHNlKTtcclxuICBjb25zdCBhdWRpb1JlZiA9IHVzZVJlZigpO1xyXG5cclxuICBjb25zdCB7IHNldFRoZW1lIH0gPSB1c2VUaGVtZSgpO1xyXG5cclxuICB1c2VFZmZlY3QoKCkgPT4ge1xyXG4gICAgLy8gVGhlbWUgQ2hhbmdlXHJcbiAgICBzZXRUaGVtZShjb2xvcik7XHJcblxyXG4gICAgaWYgKGRvd25sb2FkaW5nID09PSBmYWxzZSkge1xyXG4gICAgICAvLyBDb25mZXR0aVxyXG4gICAgICBjb25zdCBjb25mZXR0aVNldHRpbmdzID0ge1xyXG4gICAgICAgIHRhcmdldDogXCJjYW52YXNcIixcclxuICAgICAgICBzdGFydF9mcm9tX2VkZ2U6IHRydWUsXHJcbiAgICAgIH07XHJcbiAgICAgIGNvbnN0IGNvbmZldHRpID0gbmV3IENvbmZldHRpR2VuZXJhdG9yKGNvbmZldHRpU2V0dGluZ3MpO1xyXG4gICAgICBjb25mZXR0aS5yZW5kZXIoKTtcclxuICAgIH1cclxuXHJcbiAgICBhdWRpb1JlZi5jdXJyZW50LnBsYXkoKTtcclxuICB9LCBbY29sb3IsIGRvd25sb2FkaW5nXSk7XHJcblxyXG4gIHVzZUVmZmVjdCgoKSA9PiB7XHJcbiAgICBpZiAoZG93bmxvYWRpbmcgPT09IHRydWUgJiYgZG93bmxvYWRlZE9uY2UgPT09IGZhbHNlKSB7XHJcbiAgICAgIGRvd25sb2FkSW1hZ2UoKTtcclxuICAgIH1cclxuICB9LCBbZG93bmxvYWRpbmcsIGRvd25sb2FkZWRPbmNlXSk7XHJcblxyXG4gIC8vIGZ1bmN0aW9uIGZvciByYW5kb21seSBwaWNraW5nIHRoZSBtZXNzYWdlIGZyb20gbWVzc2FnZXMgYXJyYXlcclxuICBjb25zdCByYW5kb21OdW1iZXIgPSAobWluLCBtYXgpID0+IHtcclxuICAgIHJldHVybiBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiAobWF4IC0gbWluKSkgKyBtaW47XHJcbiAgfTtcclxuXHJcbiAgY29uc3QgZG93bmxvYWRJbWFnZSA9ICgpID0+IHtcclxuICAgIGlmIChkb3dubG9hZGVkT25jZSA9PT0gdHJ1ZSkgcmV0dXJuO1xyXG5cclxuICAgIGNvbnN0IG5vZGUgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImltYWdlXCIpO1xyXG5cclxuICAgIGlmIChub2RlKSB7XHJcbiAgICAgIHNldERvd25sb2FkZWRPbmNlKHRydWUpO1xyXG5cclxuICAgICAgaHRtbFRvSW1hZ2UudG9Qbmcobm9kZSkudGhlbigoYmxvYikgPT4ge1xyXG4gICAgICAgIEZpbGVTYXZlci5zYXZlQXMoYmxvYiwgXCJiaXJ0aGRheS13aXNoLnBuZ1wiKTtcclxuICAgICAgICBzZXREb3dubG9hZGluZyhmYWxzZSk7XHJcbiAgICAgIH0pO1xyXG4gICAgfVxyXG4gIH07XHJcblxyXG4gIGNvbnN0IHRpdGxlID0gKG5hbWUpID0+IHtcclxuICAgIGNvbnN0IHdpc2ggPSBcIkhhcHB5IEJpcnRoZGF5IFwiICsgbmFtZSArIFwiIVwiO1xyXG4gICAgY29uc3QgYmFzZV9sZXR0ZXJzID0gW107XHJcbiAgICBjb25zdCBuYW1lX2xldHRlcnMgPSBbXTtcclxuXHJcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHdpc2gubGVuZ3RoOyBpKyspIHtcclxuICAgICAgaWYgKGkgPCAxNSkge1xyXG4gICAgICAgIGNvbnN0IGxldHRlciA9IHdpc2guY2hhckF0KGkpO1xyXG4gICAgICAgIGJhc2VfbGV0dGVycy5wdXNoKFxyXG4gICAgICAgICAgPHNwYW4ga2V5PXtpfSBzdHlsZT17eyBcIi0taVwiOiBpICsgMSB9fT5cclxuICAgICAgICAgICAge2xldHRlcn1cclxuICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICApO1xyXG4gICAgICB9IGVsc2Uge1xyXG4gICAgICAgIGNvbnN0IGxldHRlciA9IHdpc2guY2hhckF0KGkpO1xyXG4gICAgICAgIG5hbWVfbGV0dGVycy5wdXNoKFxyXG4gICAgICAgICAgPHNwYW4ga2V5PXtpfSBzdHlsZT17eyBcIi0taVwiOiBpICsgMSB9fSBjbGFzc05hbWU9e3N0eWxlcy5zcGFufT5cclxuICAgICAgICAgICAge2xldHRlcn1cclxuICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICApO1xyXG4gICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcmV0dXJuIChcclxuICAgICAgPD5cclxuICAgICAgICB7ZG93bmxvYWRpbmcgPyAoXHJcbiAgICAgICAgICA8aDFcclxuICAgICAgICAgICAgY2xhc3NOYW1lPXtzdHlsZXMudGl0bGVJbWd9XHJcbiAgICAgICAgICAgIHN0eWxlPXt7IFwiLS13aXNoLWxlbmd0aFwiOiB3aXNoLmxlbmd0aCB9fVxyXG4gICAgICAgICAgPlxyXG4gICAgICAgICAgICA8ZGl2PntiYXNlX2xldHRlcnMubWFwKChsZXR0ZXIpID0+IGxldHRlcil9PC9kaXY+XHJcbiAgICAgICAgICAgIDxkaXY+e25hbWVfbGV0dGVycy5tYXAoKGxldHRlcikgPT4gbGV0dGVyKX08L2Rpdj5cclxuICAgICAgICAgIDwvaDE+XHJcbiAgICAgICAgKSA6IChcclxuICAgICAgICAgIDxoMSBjbGFzc05hbWU9e3N0eWxlcy50aXRsZX0gc3R5bGU9e3sgXCItLXdpc2gtbGVuZ3RoXCI6IHdpc2gubGVuZ3RoIH19PlxyXG4gICAgICAgICAgICA8ZGl2PntiYXNlX2xldHRlcnMubWFwKChsZXR0ZXIpID0+IGxldHRlcil9PC9kaXY+XHJcbiAgICAgICAgICAgIDxkaXY+e25hbWVfbGV0dGVycy5tYXAoKGxldHRlcikgPT4gbGV0dGVyKX08L2Rpdj5cclxuICAgICAgICAgIDwvaDE+XHJcbiAgICAgICAgKX1cclxuICAgICAgPC8+XHJcbiAgICApO1xyXG4gIH07XHJcblxyXG4gIGlmIChkb3dubG9hZGluZykge1xyXG4gICAgcmV0dXJuIChcclxuICAgICAgPGRpdiBjbGFzc05hbWU9e3N0eWxlcy5jb250YWluZXJJbWd9IGlkPVwiaW1hZ2VcIiBvbkNsaWNrPXtkb3dubG9hZEltYWdlfT5cclxuICAgICAgICB7ZG93bmxvYWRJbWFnZSgpfVxyXG4gICAgICAgIDxtYWluIGNsYXNzTmFtZT17c3R5bGVzLmltYWdlfT5cclxuICAgICAgICAgIDxkaXY+XHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzdHlsZXMubWFpbn0+e3RpdGxlKG5hbWUgJiYgbmFtZVswXSl9PC9kaXY+XHJcblxyXG4gICAgICAgICAgICA8ZGl2IHN0eWxlPXt7IGhlaWdodDogNDAgfX0gLz5cclxuXHJcbiAgICAgICAgICAgIDxwIGNsYXNzTmFtZT17c3R5bGVzLmRlc2NJbWd9PlxyXG4gICAgICAgICAgICAgIHttZXNzYWdlc1tyYW5kb21OdW1iZXIoMCwgbWVzc2FnZXMubGVuZ3RoKV0udmFsdWV9XHJcbiAgICAgICAgICAgIDwvcD5cclxuICAgICAgICAgIDwvZGl2PlxyXG4gICAgICAgIDwvbWFpbj5cclxuICAgICAgPC9kaXY+XHJcbiAgICApO1xyXG4gIH1cclxuXHJcbiAgcmV0dXJuIChcclxuICAgIDxkaXYgY2xhc3NOYW1lPXtzdHlsZXMuY29udGFpbmVyfT5cclxuICAgICAgPEhlYWQ+XHJcbiAgICAgICAgPHRpdGxlPkhhcHB5IEJpcnRoZGF5IHtuYW1lICYmIG5hbWVbMF19PC90aXRsZT5cclxuICAgICAgICA8bWV0YSBuYW1lPVwiZGVzY3JpcHRpb25cIiBjb250ZW50PVwiR2VuZXJhdGVkIGJ5IGNyZWF0ZSBuZXh0IGFwcFwiIC8+XHJcbiAgICAgICAgPGxpbmsgcmVsPVwiaWNvblwiIGhyZWY9XCIvZmF2aWNvbi5pY29cIiAvPlxyXG4gICAgICA8L0hlYWQ+XHJcblxyXG4gICAgICA8Y2FudmFzIGNsYXNzTmFtZT17c3R5bGVzLmNhbnZhcyArJyBoaWRkZW5faXQnfSBpZD1cImNhbnZhc1wiPjwvY2FudmFzPlxyXG5cclxuICAgICAgPG1haW4gY2xhc3NOYW1lPXtzdHlsZXMuYW5pbWF0ZX0+XHJcbiAgICAgICAgPGRpdj5cclxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPXtzdHlsZXMubWFpbn0+e3RpdGxlKG5hbWUgJiYgbmFtZVswXSl9PC9kaXY+XHJcbiAgICAgICAgICB7LyogPHAgY2xhc3NOYW1lPXtzdHlsZXMuZGVzY30+XHJcbiAgICAgICAgICAgIHttZXNzYWdlc1tyYW5kb21OdW1iZXIoMCwgbWVzc2FnZXMubGVuZ3RoKV0udmFsdWV9XHJcbiAgICAgICAgICA8L3A+ICovfVxyXG4gICAgICAgIDwvZGl2PlxyXG5cclxuICAgICAgICA8RXhhbXBsZSAvPlxyXG5cclxuICAgICAgICB7LyogPGRpdiBjbGFzc05hbWU9e3N0eWxlcy5idXR0b25Db250YWluZXJ9PlxyXG4gICAgICAgICAge2hpc3RvcnlbMF0gPT0gXCIvXCIgPyA8Q29weUxpbmtCdXR0b24gLz4gOiBcIlwifVxyXG5cclxuICAgICAgICAgIHtoaXN0b3J5WzBdID09IFwiL1wiID8gKFxyXG4gICAgICAgICAgICA8QnV0dG9uXHJcbiAgICAgICAgICAgICAgb25DbGljaz17KCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgc2V0RG93bmxvYWRlZE9uY2UoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgc2V0RG93bmxvYWRpbmcodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgfX1cclxuICAgICAgICAgICAgICB0ZXh0PVwiRG93bmxvYWQgYXMgSW1hZ2VcIlxyXG4gICAgICAgICAgICAvPlxyXG4gICAgICAgICAgKSA6IChcclxuICAgICAgICAgICAgXCJcIlxyXG4gICAgICAgICAgKX1cclxuXHJcbiAgICAgICAgICA8QnV0dG9uXHJcbiAgICAgICAgICAgIG9uQ2xpY2s9eygpID0+IHJvdXRlci5wdXNoKFwiL1wiKX1cclxuICAgICAgICAgICAgdGV4dD1cIiZsYXJyOyBDcmVhdGUgYSB3aXNoXCJcclxuICAgICAgICAgIC8+XHJcbiAgICAgICAgPC9kaXY+ICovfVxyXG4gICAgICA8L21haW4+XHJcbiAgICAgIDxhdWRpbyByZWY9e2F1ZGlvUmVmfSBpZD1cInBsYXllclwiIGF1dG9QbGF5PlxyXG4gICAgICAgIDxzb3VyY2Ugc3JjPVwibWVkaWEvbGVlbW9uX3RyZWUubXAzXCIgLz5cclxuICAgICAgPC9hdWRpbz5cclxuICAgIDwvZGl2PlxyXG4gICk7XHJcbn07XHJcblxyXG5leHBvcnQgZGVmYXVsdCBXaXNoO1xyXG4iXSwibmFtZXMiOlsiUmVhY3QiLCJ1c2VTdGF0ZSIsInVzZUVmZmVjdCIsInVzZVJlZiIsIkhlYWQiLCJzdHlsZXMiLCJ1c2VSb3V0ZXIiLCJDb25mZXR0aUdlbmVyYXRvciIsIm1lc3NhZ2VzIiwidXNlVGhlbWUiLCJodG1sVG9JbWFnZSIsIkZpbGVTYXZlciIsIkNhcm91c2VsIiwiRXhhbXBsZSIsIndpZHRoIiwibWFyZ2luIiwiYmFja2dyb3VuZCIsImZvbnRTaXplIiwib3BhY2l0eSIsIldpc2giLCJoaXN0b3J5Iiwicm91dGVyIiwibmFtZSIsInF1ZXJ5IiwiY29sb3IiLCJkb3dubG9hZGluZyIsInNldERvd25sb2FkaW5nIiwiZG93bmxvYWRlZE9uY2UiLCJzZXREb3dubG9hZGVkT25jZSIsImF1ZGlvUmVmIiwic2V0VGhlbWUiLCJjb25mZXR0aVNldHRpbmdzIiwidGFyZ2V0Iiwic3RhcnRfZnJvbV9lZGdlIiwiY29uZmV0dGkiLCJyZW5kZXIiLCJjdXJyZW50IiwicGxheSIsImRvd25sb2FkSW1hZ2UiLCJyYW5kb21OdW1iZXIiLCJtaW4iLCJtYXgiLCJNYXRoIiwiZmxvb3IiLCJyYW5kb20iLCJub2RlIiwiZG9jdW1lbnQiLCJnZXRFbGVtZW50QnlJZCIsInRvUG5nIiwidGhlbiIsImJsb2IiLCJzYXZlQXMiLCJ0aXRsZSIsIndpc2giLCJiYXNlX2xldHRlcnMiLCJuYW1lX2xldHRlcnMiLCJpIiwibGVuZ3RoIiwibGV0dGVyIiwiY2hhckF0IiwicHVzaCIsInNwYW4iLCJ0aXRsZUltZyIsIm1hcCIsImNvbnRhaW5lckltZyIsImltYWdlIiwibWFpbiIsImhlaWdodCIsImRlc2NJbWciLCJ2YWx1ZSIsImNvbnRhaW5lciIsImNhbnZhcyIsImFuaW1hdGUiXSwic291cmNlUm9vdCI6IiJ9